
%if 0

lDebug DI command messages

Copyright (C) 2008-2023 E. C. Masloch

Usage of the works is permitted provided that this
instrument is retained with the works, so that any entity
that uses the works is notified of this instrument.

DISCLAIMER: THE WORKS ARE WITHOUT WARRANTY.

%endif

.di_error:	counted " Error!"
.di_hidden:	counted "hidden "
.di_iisp:	counted " (IISP)"
.di_nonstd_iisp:counted " (nonstandard IISP)"
.di_uninst_iisp:counted " (uninstalled IISP)"
.di_freedos_reloc:
		counted " (FD kernel reloc)"
.di_jmpfar:	counted " (far jmp imm)"
.di_jmpfarindirect:
		counted " (far jmp indirect)"
.di_testhook:	counted " (test hook)"
.di_toomany:	counted " (too many chained handlers)"
.di_empty:	counted " empty MCB name"
.di_system_mcb:	counted " system MCB"
.di_system_upper:
		counted " system in UMA"
.di_system_low:	counted " system in LMA"
.di_hma:	counted " high memory area"
.di_multiplex.1:counted " [mpx:"
.di_multiplex.2:counted "h list:"
.di_multiplex.3:counted "h]"

%if _PM
gatewrong:	equ $
		asciz "gate not accessible",13,10
%endif
