#! /bin/bash

# Public Domain

if [[ -z "$RELEASENUMBER" ]]
then
  echo "Release number must be set!"
  exit 1
fi

if [[ -f ovr.sh ]] || [[ -f eld/ovr.sh ]]
then
  echo "Override files must not exist!"
  exit 1
fi

if ! hg up release$RELEASENUMBER
then
  echo "Failed to check out release$RELEASENUMBER!"
  exit 1
fi

echo 'INICOMP_METHOD="lz4 lzd exodecr apl lzsa2"' > ./ovr.sh
echo 'INICOMP_WINNER=lzsa2' >> ./ovr.sh
echo 'use_build_decomp_test=1' >> ./ovr.sh
echo 'INICOMP_PROGRESS=1' >> ./ovr.sh
echo 'use_build_xld=0' > eld/ovr.sh
echo 'use_build_eld=1' >> eld/ovr.sh
echo 'use_build_eldcomp=0' >> eld/ovr.sh

./make -D_VERSION="' release $RELEASENUMBER (',__DATE__,')'"
./makinst.sh
cdexec eld ./mak.sh
cdexec ../doc ./mak.sh

rm -f ../tmp/{*.big,*.mac,*.bin,packlib.*}
for dir in sa2 apl exo lz lz4
do
  rm -f ../tmp/$dir/{p*,t*,*.lst,debug*}
done
rm -f ../lst/{*.lst,*.xls}
