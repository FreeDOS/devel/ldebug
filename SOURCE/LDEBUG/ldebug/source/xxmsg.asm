
%if 0

lDebug X commands messages

Copyright (C) 1995-2003 Paul Vojta
Copyright (C) 2008-2023 E. C. Masloch

Usage of the works is permitted provided that this
instrument is retained with the works, so that any entity
that uses the works is notified of this instrument.

DISCLAIMER: THE WORKS ARE WITHOUT WARRANTY.

%endif

;emmname:	db "EMMXXXX0"
emsnot:		asciz "EMS not installed",13,10
emserr1:	asciz "EMS internal error",13,10
emserr3:	asciz "Handle not found",13,10
emserr5:	asciz "No free handles",13,10
emserr7:	asciz "Total pages exceeded",13,10
emserr8:	asciz "Free pages exceeded",13,10
emserr9:	asciz "Parameter error",13,10
emserra:	asciz "Logical page out of range",13,10
emserrb:	asciz "Physical page out of range",13,10
	align 2, db 0
emserrs:	dw_emserr emserr1,emserr1,0,emserr3,0,emserr5,0,emserr7
		dw_emserr emserr8,emserr9,emserra,emserrb
emserrx:	asciz "EMS error "
xaans:		db "Handle created = "
xaans1:		asciz "____",13,10
xdans:		db "Handle "
xdans1:		asciz "____ deallocated",13,10
xrans:		asciz "Handle reallocated",13,10
xmans:		db "Logical page "
xmans1:		db "____ mapped to physical page "
xmans2:		asciz "__",13,10
xsstr1:		db "Handle "
xsstr1a:	db "____ has "
xsstr1b:	asciz "____ pages allocated",13,10
xsstr2:		db "phys. page "
xsstr2a:	db "__ = segment "
xsstr2b:	asciz "____  "
xsstr3:		db "____ of a total "
xsstr3a:	asciz "____ EMS "
xsstr4:		asciz "es have been allocated",13,10
xsstrpg:	asciz "pag"
xsstrhd:	asciz "handl"
xsnopgs:	asciz "no mappable pages",13,10,13,10
