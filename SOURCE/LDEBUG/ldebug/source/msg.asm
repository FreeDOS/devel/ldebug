%if 0

lDebug messages

Copyright (C) 1995-2003 Paul Vojta
Copyright (C) 2008-2024 E. C. Masloch

Usage of the works is permitted provided that this
instrument is retained with the works, so that any entity
that uses the works is notified of this instrument.

DISCLAIMER: THE WORKS ARE WITHOUT WARRANTY.

%endif

%if _MESSAGESEGMENT
 %define usemessagesegment usesection MESSAGESEGMENT
%else
 %define usemessagesegment usesection lDEBUG_DATA_ENTRY
%endif

%define MESSAGE_INLINE 1

%if _HELP_COMPRESSED
 %assign NONCOMPRESSEDFLAG 1
%else
 %assign NONCOMPRESSEDFLAG 0
%endif

%if _HELP_EXTERNAL && _HELP_COMPRESSED
 %imacro helppage 1-2
  %defstr %%basename %1
  %strcat %%includename %%basename, ".hhs"
	align 2, db 0
%00:
	dw %%end - %%start
%%start:
	incbin %%includename
%%end:
 %endmacro
%elif _HELP_EXTERNAL
 %imacro helppage 1-2 0
  %defstr %%basename %1
  %strcat %%includename %%basename, ".txt"
%00:
	incbin %%includename
  %ifn %2
	asciz
  %endif
 %endmacro
%else
 %imacro helppage 1-2 0
  %defstr %%basename %1
  %strcat %%includename "help/", %%basename, ".asm"
%00:
  %include %%includename
  %ifn %2
	asciz
  %endif
 %endmacro
%endif


%if _LINK
	usemessagesegment
%include "linkinfo.asm"
%endif


	usesection lDEBUG_DATA_ENTRY

msg:

	align 2, db 0
.help_array:
	dw .build_nameversion + NONCOMPRESSEDFLAG
	dw .help_header + NONCOMPRESSEDFLAG
	dw .help_contents
.help_array_amount: equ ($ - .help_array) / 2

	usemessagesegment
	align 2, db 0
.help_header:
	asciz " help screen",13,10
.help_contents: helppage main

%if _EXTHELP
	usesection lDEBUG_DATA_ENTRY
.source:asciz "SOURCE"

	usemessagesegment
.help_source: helppage source
%endif

	usesection lDEBUG_DATA_ENTRY
.re:
	asciz "RE"

	usemessagesegment
.help_re: helppage re

	usesection lDEBUG_DATA_ENTRY
.run:
	asciz "RUN"

	usemessagesegment
.help_run: helppage run

	usesection lDEBUG_DATA_ENTRY

	align 2, db 0
.build_array:
	dw .build_nameversion + NONCOMPRESSEDFLAG
	dw .build_linebreak + NONCOMPRESSEDFLAG
.build_version_amount: equ ($ - .build_array) / 2
	dw .build_ldebug + NONCOMPRESSEDFLAG
	dw .build_lmacros + NONCOMPRESSEDFLAG
%if _SYMBOLIC
	dw .build_symsnip + NONCOMPRESSEDFLAG
%endif
%if _BOOTLDR
	dw .build_scanptab + NONCOMPRESSEDFLAG
%endif
	dw .build_inicomp + NONCOMPRESSEDFLAG
%if (_BOOTLDR && _CHECKSUM) || _CHECKSUM_NEW
	dw .build_inicheck + NONCOMPRESSEDFLAG
%endif
%if _BOOTLDR
	dw .build_ldosboot + NONCOMPRESSEDFLAG
%endif
.build_short_amount: equ ($ - .build_array) / 2
%if _EXTHELP
	dw .build_long
%endif
.build_long_amount: equ ($ - .build_array) / 2

%if _OPTIONS && _EXTHELP
	align 2, db 0
.options_array:
	dw .options_1
	dw .options_2
	dw .options_3
	dw .options_4
	dw .options_5
	dw .options_6
.options_array_option_amount: equ ($ - .options_array) / 2
	dw .flags_1
	dw .asmoptions_1
.options_array_amount: equ ($ - .options_array) / 2

.options_scan:
	db "123456"
.options_scan_amount: equ ($ - .options_scan)
%if .options_array_option_amount != .options_scan_amount
 %error Array size mismatch
%endif

.string_options:
	asciz "OPTIONS"

	usemessagesegment
.options_pages: helppage options
%endif

	usesection lDEBUG_DATA_ENTRY
.string_build:
	asciz "BUILD"
.string_version:
	asciz "VERSION"

	usemessagesegment
	align 2, db 0
.build_nameversion:
	asciz _PROGNAME,_VERSION

	align 2, db 0
.build_linebreak:
	asciz 13,10
	_fill_at_least 80, 0, .build_nameversion

	align 2, db 0
.build_ldebug:
%ifnidn _REVISIONID,""
	db "Source Control Revision ID: ",_REVISIONID,13,10
%endif
	asciz
	_fill_at_least 64, 0, .build_ldebug

	align 2, db 0
.build_lmacros:
	fill_at_least 64, 0, asciz _REVISIONID_LMACROS

%if _SYMBOLIC
	align 2, db 0
.build_symsnip:
	fill_at_least 64, 0, asciz _REVISIONID_SYMSNIP
%endif

%if _BOOTLDR
	align 2, db 0
.build_scanptab:
	fill_at_least 64, 0, asciz _REVISIONID_SCANPTAB
%endif

	align 2, db 0
.build_inicomp:
	fill_at_least 64, 0, asciz _REVISIONID_INICOMP

%if (_BOOTLDR && _CHECKSUM) || _CHECKSUM_NEW
	align 2, db 0
.build_inicheck:
	fill_at_least 64, 0, asciz _REVISIONID_INICHECK
%endif

%if _BOOTLDR
	align 2, db 0
.build_ldosboot:
	fill_at_least 64, 0, asciz _REVISIONID_LDOSBOOT
%endif

%if _EXTHELP
.build_long: helppage build
%endif
%if ! _EXTHELP || _HELP_COMPRESSED
 %if _BOOTLDR
	asciz
	; This message is used by mak.sh to detect that we
	;  are building with boot load support.
	db 13,10,"Boot loader",13,10
	asciz
 %endif
%endif

%if _EXTHELP
	usemessagesegment
.license: helppage license

	usemessagesegment
.reghelp: helppage regs

	usemessagesegment
.flaghelp: helppage flags

 %if _COND
	usemessagesegment
.condhelp: helppage cond
 %endif

 %if _EXPRESSIONS
	usemessagesegment
.expressionhelp: helppage expr
 %endif

 %if _OPTIONS
	usemessagesegment
.options_1: helppage options1

	usemessagesegment
.options_2: helppage options2

	usemessagesegment
.options_3: helppage options3

	usemessagesegment
.options_4: helppage options4, 1
	; terminator is in .options_5

	usemessagesegment
	align 2, db 0
.options_5:
%if _HELP_COMPRESSED
	dw 2		; length of compressed data (excluding itself)
	db 10, 4	; parameters expected by depack
%else
	asciz
%endif

	usemessagesegment
.options_6: helppage options6

	usemessagesegment
.flags_1: helppage optionsi

	usemessagesegment
.asmoptions_1: helppage optionsa
 %endif

 %if _VARIABLES || _OPTIONS || _PSPVARIABLES
	usemessagesegment
.varhelp: helppage vars
 %endif
%endif

	usesection lDEBUG_DATA_ENTRY
.readonly:	asciz "This lDebug variable cannot be written to. See ?V.",13,10
.readonly_mem:	asciz "This memory variable cannot be written to.",13,10
%if _MMXSUPP
.internal_error_no_mmx:
		asciz "Internal error, MMX variables not supported.",13,10
%endif
%if _PM
.readonly_verifysegm:
		db "Memory using selector "
.readonly_verifysegm.selector:
		asciz "---- is inaccessible for writing.",13,10
%endif
.more:		db "[more]"
 .more_size equ $-.more
.more_over:	db 13
		times .more_size db 32
		db 13		; to overwrite previous prompt
 .more_over_size equ $-.more_over
.ctrlc:		db "^C",13,10
 .ctrlc_size equ $-.ctrlc
 		asciz
.freedos_ctrlc_workaround:
		asciz " (Old FreeDOS kernel Ctrl-C work around happened)",13,10
.not_while_indos:
		asciz "Command not supported while in InDOS mode.",13,10
.rv_mode.before:	asciz "Current mode: "
%if _PM
.rv_mode_dpmi_16:	asciz "DPMI 16-bit CS",13,10
.rv_mode_dpmi_32:	asciz "DPMI 32-bit CS",13,10
%endif
.rv_mode_r86m:		asciz "Real 86 Mode",13,10
.rv_mode_v86m:		asciz "Virtual 86 Mode",13,10

.regs386:	asciz "386 registers are "
.regs386_off:db "not "
.regs386_on:	asciz "displayed",13,10

%if _EMS
	usemessagesegment
.xhelp: helppage ems
%endif

%if _PM
	usesection lDEBUG_DATA_ENTRY
.desc:	asciz "DESC"
	usemessagesegment
.deschelp: helppage desc
%endif

%if !_BOOTLDR_DISCARD_HELP && _MESSAGESEGMENT
messagesegment_truncated_size equ messagesegment_size
	endarea messagesegment_truncated, 1
messagesegment_nobits_truncated_size equ messagesegment_nobits_size
	endarea messagesegment_nobits_truncated, 1
%endif
%if _EXTHELP
 %if _BOOTLDR
	usemessagesegment
 %if _BOOTLDR_DISCARD_HELP
	align 2, db 0
messagesegment_truncated_size equ fromparas(paras($ - messagesegment_start + fromwords(imsg.boothelp_replacement_size_w)))
	endarea messagesegment_truncated, 1
 %endif
.boothelp: helppage boot
 %if _BOOTLDR_DISCARD_HELP

truncated_message_last equ .boothelp + fromwords(imsg.boothelp_replacement_size_w)
	absolute truncated_message_last
%if _HELP_COMPRESSED
	alignb 16
truncated_hshrink_message_buffer: equ $
		resb hshrink_message_buffer_size
%endif

	alignb 16
messagesegment_nobits_truncated_size equ $ - $$
	endarea messagesegment_nobits_truncated, 1
 %endif
 %endif
%endif

	usesection lDEBUG_DATA_ENTRY
%if _TSR
.pspnotfound:	asciz "Cannot go resident, child PSP not found.",13,10
.psphooked:	asciz "Cannot go resident, child PSP parent return address hooked.",13,10
.nowtsr1:	asciz "Patched PSP at "
.nowtsr2:	asciz ", now resident.",13,10
.alreadytsr:	asciz "Already resident.",13,10
.tsr_cannot_terminate_attached:
		asciz "Cannot terminate attached while resident.",13,10
.tsr_cannot_ll:
		asciz "Cannot load process while resident. Use R DCO7 CLR= ",_4digitshex(opt7_no_tsr_ll)," to override.",13,10
%endif
%if _ATTACH
.notyettsr:	asciz "Not yet resident. Cannot attach.",13,10
.invalidpsp:	asciz "Invalid PSP specified. Cannot attach.",13,10
.selfownedpsp:	asciz "Self-owned PSP specified. Cannot attach.",13,10
.nowattached:	asciz "Patched PSP, now attached.",13,10
.attach:	asciz "ATTACH"
%endif
%if _PM && (_TSR || _BOOTLDR)
.cannotpmquit:	asciz "Cannot quit, still in protected mode.",13,10
%endif
%if _PM
.cannotpmload:	asciz "Process loading aborted: Still in protected mode.",13,10
%endif
%if _BOOTLDR
.nobootsupp:	asciz "Command not supported in boot loaded mode.",13,10
.boot_quit_fail:asciz "Shutdown not supported.",13,10
.bootfail:	asciz "Boot failure: "
.bootfail_read:	db "Reading sector failed (error "
.bootfail_read_errorcode:	asciz "__h).",13,10
.bootfail_sig:	asciz "Boot sector signature missing (is not AA55h).",13,10
.bootfail_sig_parttable:	ascii "Partition table signature missing"
				asciz " (is not AA55h).",13,10
.bootfail_code:	asciz "Boot sector code invalid (is 0000h).",13,10
.bootfail_secsizediffer:
		asciz "BPB BpS differs from actual sector size.",13,10
.bootfail_stack_underflow:
		asciz "Boot stack underflowed.",13,10
.bootfail_check_mismatch:
		db "Check mismatch, expected "
.bootfail_check_mismatch.check_value:
		db "____h at offset "
.bootfail_check_mismatch.check_offset:
		db "____h but has "
.bootfail_check_mismatch.check_got:
		asciz "____h.",13,10
.boot_out_of_memory_error:	asciz "Out of memory.", 13,10
.boot_too_many_partitions_error:asciz "Too many partitions (or a loop).",13,10
.boot_partition_cycle_error:	asciz "Partition table cycle detected.",13,10
.boot_partition_not_found:	asciz "Partition not found.",13,10
.boot_access_error:	asciz "Read error.", 13,10
.boot_sector_too_large:	asciz "Sector size too small (< 32 bytes).", 13,10
.boot_sector_too_small:	asciz "Sector size too large (> 8192 bytes).", 13,10
.boot_sector_not_power:	asciz "Sector size not a power of two.", 13,10
.boot_invalid_sectors:	asciz "Invalid geometry sectors.", 13,10
.boot_invalid_heads:	asciz "Invalid geometry heads.", 13,10
.boot_file_not_found:	asciz "File not found.",13,10
.boot_file_too_big_error:	asciz "File too big.",13,10
.boot_file_too_small_error:	asciz "File too small.",13,10
.boot_badclusters:	asciz "Bad amount of clusters.",13,10
.boot_badchain:		asciz "Bad cluster chain.",13,10
.boot_badfat:		asciz "Bad File Allocation Table.",13,10
.boot_invalid_filename:	asciz "Invalid filename.",13,10
.boot_cannot_set_both:	asciz "Cannot set both "
.boot_and:		asciz " and "
.boot_dot_crlf:		asciz ".",13,10
.boot_internal_error:	asciz "! Internal error !",13,10
.boot_bpb_load_overlap:	asciz "BPB and load area overlap.",13,10
.boot_segment_too_low:	asciz "Segment too low.",13,10
.boot_bpb_too_low:	asciz "BPB too low.",13,10
.boot_auxbuff_crossing:	db "! Internal error !, "
			asciz "auxbuff crosses 64 KiB boundary.",13,10
.read:		asciz "READ"
.write:		asciz "WRITE"
.hidden:	asciz "HIDDEN"
.hiddenadd:	asciz "HIDDENADD"
.dir:		asciz "DIR"
.dirinsteadsize:countedb "   [DIR]"
.emptydirname:	asciz "/"
.boot:		asciz "BOOT"
.quit:		asciz "QUIT"
.protocol:	asciz "PROTOCOL"
.segment:	asciz "SEGMENT"
.entry:		asciz "ENTRY"
.bpb:		asciz "BPB"
.minpara:	asciz "MINPARA"
.maxpara:	asciz "MAXPARA"
.checkoffset:	asciz "CHECKOFFSET"
.checkvalue:	asciz "CHECKVALUE"
.sector:	asciz "SECTOR"
.sector_alt:	asciz "SECTORALT"
.freedos_kernel_name:	asciz "KERNEL.SYS"
.dosc_kernel_name:	asciz "IPL.SYS"
.edrdos_kernel_name:	asciz "DRBIO.SYS"
.ldos_kernel_name:	asciz "LDOS.COM"
.msdos7_kernel_name:
.msdos6_kernel_name:	asciz "IO.SYS"
.msdos6_add_name:	asciz "MSDOS.SYS"
.ibmdos_kernel_name:	asciz "IBMBIO.COM"
.ibmdos_add_name:	asciz "IBMDOS.COM"
.ntldr_kernel_name:	asciz "NTLDR"
.bootmgr_kernel_name:	asciz "BOOTMGR"
.chain_kernel_name:	asciz "BOOTSECT.DOS"
.rxdos.0_kernel_name:	asciz "RXDOSBIO.SYS"
.rxdos.1_kernel_name:	asciz "RXBIO.SYS"
.rxdos.0_add_name:
.rxdos.1_add_name:	asciz "RXDOS.SYS"
.rxdos.2_kernel_name:	asciz "RXDOS.COM"
.addname_empty:		asciz
.cannotbootquit_memsizes:	asciz "Cannot quit, memory size changed.",13,10
%endif
.uninstall:	db "UN"
.install:	asciz "INSTALL"
.toggle:	asciz "TOGGLE"
.alreadyenabled:asciz " is already enabled.",13,10
.alreadydisabled:asciz " is already disabled.",13,10
.tryenable:	asciz ": Trying to enable.",13,10
.trydisable:	asciz ": Trying to disable.",13,10
%if _AREAS_HOOK_SERVER || (_AREAS && _AREAS_HOOK_CLIENT)
.qqlate_areas_error:
		asciz "Internal error in Q command uninstalling areas.",13,10
%endif
%if _AREAS && _AREAS_HOOK_CLIENT
.areasinstalled:	asciz "Areas installed.",13,10
.areasalreadyinstalled:	asciz "Areas already installed.",13,10
.areasnodebuggerfound:	asciz "Areas not installed, no debugger AMIS interface found!",13,10
.areasnotsupported:	asciz "Areas not installed, debugger AMIS interface does not support function!",13,10
.areasnotinstalled:	db "Areas not installed, debugger returned code "
.areasnotinstalled.code:asciz "--h.",13,10
.areasalreadyuninstalled:	asciz "Areas already uninstalled.",13,10
.areasuninstalled:		db "Areas uninstalled, debugger returned code "
.areasuninstalled.code:		asciz "--h.",13,10
%endif
.then:		asciz "THEN"
.not:		asciz "NOT"
.rvv:		asciz "RVV"
.rvm:		asciz "RVM"
.rvp:		asciz "RVP"
.rvd:		asciz "RVD"
%if _MMXSUPP && _RM
.rm:		asciz "RM"
%endif
%if _BOOTLDR
.rvp_boot:		ascizline "Mode: Boot loaded"
%endif
%if _DEVICE
 %if _ATTACH
.rvp_device_attached:	ascizline "Mode: Device driver attached"
 %endif
.rvp_device:		ascizline "Mode: Device driver"
%endif
%if _APPLICATION
 %if _TSR
.rvp_tsr:		ascizline "Mode: Application installed as TSR"
 %endif
.rvp_application:	ascizline "Mode: Application"
%endif
.vm_codeseg:	counted "Code segment="
%if _DUALCODE
.vm_code2seg:	counted "Code2 segment="
%endif
.vm_dataseg:	counted "Data segment="
.vm_entryseg:	counted "Entry segment="
%if _MESSAGESEGMENT
.vm_messageseg:	counted "Message segment="
%endif
.vm_auxseg:	counted "Auxbuff segment="
%if _HISTORY_SEPARATE_FIXED && _HISTORY
.vm_hisseg:	counted "History segment="
%endif
%if _PM
.vm_selector:	counted " selector="
%endif
.vp_pspsegment:		counted "Client   PSP="
.vp_dpspsegment:	counted "Debugger PSP="
.vp_dparent:
.vp_parent:		counted " Parent="
.vp_dpra:
.vp_pra:		counted " Parent Return Address="
%if _PM
.vp_dpspsel:
.vp_pspsel:		counted " PSP Selector="
%endif
.rvd_not_device:	asciz "Not loaded in device mode.",13,10
%if _DEVICE
.rvd_deviceheader:	counted "Device header at "
.rvd_size:		counted ". Amount paragraphs allocated is "
%endif
.n_toolongtail:	asciz "Too long N command tail!",13,10
.n_toolongname:	asciz "Too long N command name!",13,10
.number:	asciz "NUMBER"
.counter:	asciz "COUNTER"
.id:		asciz "ID"
.when:		asciz "WHEN"
.offset:	asciz "OFFSET"
.questionmark:	asciz "?"
.or:		db "O"
.r:		asciz "R"
.nd:		asciz "ND"
.lr:		asciz "LR"
.remember:	asciz "REMEMBER"
.goto:		asciz "GOTO"
.sof:		asciz "SOF"
.eof:		asciz "EOF"
.goto_not_file:	asciz "Error: GOTO command not supported when not reading a script.",13,10
.goto_empty:	asciz "Error: GOTO needs a destination label.",13,10
.goto_not_found.1:	asciz "Error: GOTO destination label ",'"'
.goto_not_found.2:	asciz '"'," not found.",13,10
.guard_auxbuff_error:	asciz "Error: auxbuff already guarded!",13,10
.guard_re_error:	asciz "Error: Command not supported while reading from RE buffer.",13,10
.guard_rc_error:	asciz "Error: Command not supported while reading from RC buffer.",13,10
.unexpected_auxbuff_guard:	asciz "Error: Unexpected auxbuff guard!",13,10
%if _SYMBOLIC
.unexpected_nosymbols:		asciz "Error: Unexpected no symbols flag!",13,10
%endif
.unexpected_noneol_re:		asciz "Error: Unexpected non-EOL in RE processing!",13,10
.unexpected_noneol_rc:		asciz "Error: Unexpected non-EOL in RC processing!",13,10
.replace:	asciz "REPLACE"
.append:	asciz "APPEND"
.dword:		db "D"
.word:		asciz "WORD"
.3byte:		db "3"
.byte:		asciz "BYTE"
.lines:		asciz "LINES"
%if _RH
.rh:		asciz "RH"
.rh_enabled:	asciz "Register dump history enabled.",13,10
.rh_disabled:	asciz "Register dump history disabled.",13,10
.rh_step.1:	counted "  RH step="
.rh_step.2:	counted "  decimal: "
%endif
%if _HELP_COMPRESSED
.hshrink_error:	asciz "Internal error: Depack failure!",13,10
%endif
%if _TEST_HELP_FILE
.testhelp:	asciz "TESTHELP"
%endif
%if 1 || _STRNUM
.as:		asciz "AS"
%endif
%if _DTOP
.top:		asciz "TOP"
%endif
%if _COUNT || _RH
.count:		asciz "COUNT"
%endif
%if _CLEAR
.clear:		asciz "CLEAR"
.clear_sequence:asciz 27,"c",27,"[2J",13,10
	; Note that the first sequence will not have an effect
	;  when writing to int 10h in non-dumb mode. Therefore,
	;  it will advance the cursor to a nonzero column.
	; This is required by the dumb mode detection code.
%endif
%if _HHDIVREMAIN
.hh_div_remainder:
		counted "remainder is "
%endif
%if _CONFIG
.configkeyword:	asciz "::CONFIG:"
.scriptskeyword:asciz "::SCRIPTS:"
.emptykeyword:	asciz "::EMPTY:"
%endif
%if _EXTENSIONS
.ext_error_internal:	asciz "Internal error in EXT command.",13,10
.ext_error_io:		asciz "EXT command got I/O error.",13,10
.ext_error_invalid:	asciz "No or invalid Extension for lDebug header.",13,10
.ext_error_oom:		asciz "Out of memory in EXT command.",13,10
.ext:		asciz "EXT"
.eld_load_error:db "Extension for lDebug had an error during loading. Code: "
.eld_error_code:asciz "----.",13,10
%endif
.gib:		asciz "GIB"
.mib:		asciz "MIB"
.kib:		asciz "KIB"
.pages:		asciz "PAGES"
.paragraphs:	asciz "PARAGRAPHS"
.paras:		asciz "PARAS"
.qwords:	asciz "QWORDS"
.dwords:	db "D"
.words:		asciz "WORDS"
.bytes:		asciz "BYTES"
.length:	asciz "LENGTH"
.end:		asciz "END"
.range:		asciz "RANGE"
%if _VXCHG
.on:		asciz "ON"
.off:		asciz "OFF"
.vv_enable_failure:
		asciz "Unable to enable video swapping.",13,10
.vv_disabled:	asciz "Video swapping is disabled, use V ON to switch it on.",13,10
%endif
.reverse:	asciz "REVERSE"
.value:		asciz "VALUE"
.in:		asciz "IN"
.existing:	asciz "EXISTING"
.from:		asciz "FROM"
.to:		asciz "TO"
.executing:		asciz "EXECUTING"
.executing_value_range:	asciz "FROM LINEAR cs:cip LENGTH abo - cip"
.linear:	asciz "LINEAR"
%if _IMMASM
.immasm_error_eip:
		asciz "Error, branch targets EIP beyond 64 KiB.",13,10
%endif
%if _PM
.desctype:	asciz "DESCTYPE"
%endif
.nottaken:	db "NOT"
.taken:		asciz "TAKEN"
.nt:		db "N"
.t:		asciz "T"
.base:		asciz "BASE"
.group:		asciz "GROUP"
.width:		asciz "WIDTH"
%if _HISTORY
.history_internal_error:
		asciz 13,10,"Internal error in history handling!",13,10
%endif
%if _INT
%include "dimsg.asm"
%endif
.header:	asciz "header"
.header.length:	equ $ - 1 - .header
.trailer:	asciz "trailer"
.trailer.length:equ $ - 1 - .trailer
.at:		asciz "AT"
.while:		asciz "WHILE"
.silent:	asciz "SILENT"
.sleep:		asciz "SLEEP"
.seconds:	asciz "SECONDS"
.ticks:		asciz "TICKS"
.re_limit_reached:	asciz "RE processing reached RELIMIT, aborting.",13,10
.rc_limit_reached:	asciz "RC processing reached RCLIMIT, aborting.",13,10
.silent_error:	asciz "! Internal error during silent buffer handling !",13,10
.while_not_true:asciz "While condition not true, returning.",13,10
.while_terminated_before:	asciz "While condition ",'"'
.while_terminated_after:	asciz '"'," no longer true.",13,10
.no_progress:	asciz "No serial comm progress after 5 seconds, giving up. (Keyboard enabled.)",13,10
.serial_request_keep:	asciz 13,10,_PROGNAME," connected to serial port. Enter KEEP to confirm.",13,10
.serial_no_keep_timer:	asciz "No KEEP keyword confirmation after timeout, giving up. (Keyboard enabled.)",13,10
.serial_no_keep_enter:	asciz "No KEEP keyword confirmation, enabling keyboard.",13,10
%if _VXCHG
.nokeep:	db "NO"
%endif
.keep:		asciz "KEEP"
.cannot_hook_2D.invalid:	asciz "Error: Unable to hook interrupt 2Dh due to invalid handler.",13,10
.cannot_hook_2D.nofree:		asciz "Error: Unable to hook interrupt 2Dh, no free multiplex number.",13,10
.serial_cannot_unhook:		db "Warning: "
.serial_cannot_unhook.nowarn:	db "Unable to unhook interrupt "
.serial_cannot_unhook.int:	asciz "--h.",13,10
.serial_cannot_hook:		db "Error: Unable to hook interrupt "
.serial_cannot_hook.new_int:	db "--h because interrupt "
.serial_cannot_hook.old_int:	asciz "--h still hooked.",13,10
.serial_late_unhook:		db "Succeeded in unhooking interrupt "
.serial_late_unhook.int:	asciz "--h.",13,10
.line_out_overflow:	asciz "Internal error, line_out buffer overflowed!",13,10
%if _REGSHIGHLIGHT || _GETLINEHIGHLIGHT || _DHIGHLIGHT
.highlight:	counted 27,"[7m"
 %if _GETLINEHIGHLIGHT || _DHIGHLIGHT
		db 0
 %endif
.unhighlight:	counted 27,"[m"
 %if _GETLINEHIGHLIGHT || _DHIGHLIGHT
		db 0
 %endif
%endif
.prefixes:	asciz " kMGT"
.ll_unterm:	ascizline "Process loading aborted: Attached process didn't terminate!"
.qq_unterm:	ascizline "Cannot quit, attached process didn't terminate!"
%if _PM
.qq_still_pm:	ascizline "Cannot quit, still in PM after attached process terminated!"
%endif
.qq_a_unterminated:	ascizline "Attached process didn't terminate."
.qq_a_terminated:	ascizline "Attached process did terminate."
.ensure_no_memory:	ascizline "Cannot create empty attached process, out of memory!"
%if _DEVICE
.qq_device_none_selected:
 ascizline "Cannot quit normally when loaded as device driver! Try QC or QD command."
.qq_device_no_d:
 ascizline "Cannot quit to device driver initialisation, state modified!"
.qq_device_no_c:
 ascizline "Cannot quit from device driver container, not found!"
 %if _PM
.qq_device_pm:		ascizline "Cannot quit device driver in PM!"
 %endif
	align 2, db 0
.NULblank:		fill 8, 32, db "NUL"
%endif
.c0:	db "C0"
.cr:	db 13

.abort:			asciz "ABORT"
%if _EXTENSIONS || _INPUT_FILE_HANDLES || _INPUT_FILE_BOOT
.exists:		asciz "EXISTS"
.y:			asciz "Y"
.yy_requires_filename:	asciz " command requires a filename.",13,10
.yy_filename_empty:	asciz " command filename is empty.",13,10
.yy_too_many_handles:	asciz " command has too many open files.",13,10
.yy_error_file_open:	asciz " command failed to open file.",13,10
.yy_no_file:		asciz " command limited to label only valid in script file.",13,10
%endif
%if _INPUT_FILE_HANDLES || _EXTENSIONS
.yy_no_dos:		asciz " command requires DOS to be available.",13,10
.yy_filename_missing_unquote:
			asciz " command filename missing ending quote.",13,10
%endif
%if _INPUT_FILE_BOOT || _EXTENSIONS
.yy_too_large:		asciz " command file too large.",13,10
.yy_empty:		asciz " command file empty.",13,10
%endif

%if _SYMBOLIC
.zz_switch_s_received:	asciz "Allocating symbol table buffer of "
.zz_switch_s_received_xms:
			asciz "Allocating XMS symbol table buffer (including transfer buffer) of "
.zz_switch_s_freeing:	asciz "Freeing symbol table buffer.",13,10
.zz_switch_s_indos:	db "Can't change symbol table buffer allocation"
			asciz " while in DOS!",13,10
%if _BOOTLDR
.zz_switch_s_internal_error:
	asciz "Internal error in Z /S switch handling!",13,10
.zz_switch_s_boot_memsize_differ:
	asciz "Cannot change symbol table buffer allocation, memory size changed!",13,10
.zz_switch_s_boot_transfer_too_low:
	asciz "Cannot enlarge symbol table buffer that much, transfer buffer too low!",13,10
.zz_switch_s_boot_loaded_kernel:
	asciz "Cannot change symbol table buffer allocation, kernel has been loaded!",13,10
.zz_switch_s_boot_rpl:
	asciz "Cannot change symbol table buffer allocation, RPL has been loaded!",13,10
%endif
.zz_s_cannot_alloc_transfer:	asciz "Cannot allocate transfer buffer!",13,10
.zz_s_cannot_alloc_target:	asciz "Cannot allocate target buffer!",13,10
.zz_too_full:	asciz "Symbol tables are too full for this reallocation.",13,10
.zz_xms_not_freed_1:	asciz "Unable to free symbol table XMS handle = "
.zz_xms_not_freed_2:	asciz "h.",13,10
.invaliddata:		asciz "Invalid symbol table data!",13,10
 %if _SECOND_SLICE && (_XMS_SYMBOL_TABLE || _BUFFER_86MM_SLICE)
.error_second_slice:	asciz "Invalid symbol table access slice usage!",13,10
 %endif
.main_too_full:		asciz "Symbol main array is too full!",13,10
.main_too_full_crit1:	asciz "Symbol main array is too full! Critical error. (Earlier check succeeded.)",13,10
.hash_too_full_crit1:	asciz "Symbol hash array is too full! Critical error. (Earlier check succeeded.)",13,10
.hash_too_full_crit2:	asciz "Symbol hash array is too full! Critical error. (Main has space.)",13,10
.str_too_full:		asciz "Symbol string heap is too full!",13,10
.str_too_long:		asciz "Symbol string is too long!",13,10
.liststore.main.end.first:	asciz 13,10,"Main total:",9
.liststore.main.free.first:	asciz "Main free:",9
.liststore.main.used.first:	asciz "Main used:",9
.liststore.hash.end.first:	asciz 13,10,"Hash total:",9
.liststore.hash.free.first:	asciz "Hash free:",9
.liststore.hash.used.first:	asciz "Hash used:",9
.liststore.str.end.first:	asciz 13,10,"String total:",9
.liststore.str.free.first:	asciz "String free:",9
.liststore.str.used.first:	asciz "String used:",9
.liststore.second:		asciz " in "
.liststore.third.singular:	asciz " unit",13,10
.liststore.third.plural:	asciz " units",13,10
.liststore.str.first:	asciz "Strings size is "
.liststore.str.unref.first:	asciz "Unreferenced strings size is "
.liststore.str.unref.second:
.liststore.str.second:	asciz " in "
.liststore.str.unref.third.singular:
.liststore.str.third.singular:	asciz " string.",13,10
.liststore.str.unref.third.plural:
.liststore.str.third.plural:	asciz " strings.",13,10
.liststore.str.fourth:	asciz "Average string structure length is <= "
.liststore.str.invalid:	asciz "Error: Average string structure length too large"
.liststore.str.nofourth:asciz "Cannot calculate average string structure length, number of strings is zero"
.liststore.str.last:	asciz ".",13,10
.symhint:
.symhint_store_string:	db "..@symhint_"
.symhint_size:		equ $ - .symhint
			db "store_string_"
.symhint_store_string_size equ $ - .symhint_store_string
.trace_caller:		db "trace_caller"
.trace_caller_size: equ $ - .trace_caller
.trace_here:		db "trace_here"
.trace_here_size: equ $ - .trace_here
.skip_caller:		db "skip_caller_"
.skip_caller_size: equ $ - .skip_caller
.skip_here:		db "skip_here_"
.skip_here_size: equ $ - .skip_here
.asciz:			asciz "ASCIZ"
.zz_list_range_first:	asciz "Range: "
.zz_list_range_second:	asciz "h--"
.zz_list_range_third:	asciz "h"
.zz_list_add_none:	db ""
.zz_list_none:		asciz " No symbols found",13,10
.zz_list_start:		asciz 13,10
.zz_list_between:	asciz
.zz_list_first:		asciz " Linear="
.zz_list_second:	asciz " Offset="
.zz_list_middle:	asciz "h = ",'"'
.zz_list_last:		asciz '"',13,10
.zz_list_end:		asciz
.zz_list_add_range:	asciz "; "
.zz_list_add_first:	asciz "z add linear=("
.zz_list_base:		asciz " + v1"
.zz_list_base_symbol:	asciz " + sl."
.zz_list_add_second:	asciz ") offset="
.zz_list_add_middle:	asciz " symbol='"
.zz_list_add_last:	asciz "'",13,10
.zz_match_add_none:	db ";"
.zz_match_none:		asciz " No symbols found",13,10
.existing_block:	asciz "Symbol already exists and is being blocked.",13,10
.poison_block:		asciz "Symbol definition is poisoned and is being blocked.",13,10
.stat:			asciz "STAT"
.match:			asciz "MATCH"
.add:			asciz "ADD"
.commit:		asciz "COMMIT"
.del:			asciz "DEL"
.delete:		asciz "DELETE"
.unrefstring:		asciz "UNREFSTRING"
.reloc:			asciz "RELOC"
.relocate:		asciz "RELOCATE"
.symbol:		asciz "SYMBOL"
.flags:			asciz "FLAGS"
.sl:			asciz "SL"
.max:			asciz "MAX"
%if _XMS_SYMBOL_TABLE
.zz_no_xms:		asciz "No XMS driver detected!",13,10
.zz_fail_xms_alloc:	asciz "Failed to allocate XMS block!",13,10
.zz_fail_xms_access:	asciz "Failed to access XMS block!",13,10
%endif
.zz_main_hash_mismatch:	asciz "Compaction/expansion failed, differing amounts of hash and main entries.",13,10
.zz_main_not_first:	asciz "Compaction/expansion failed, main array is not first.",13,10
.zz_hash_not_second:	asciz "Compaction/expansion failed, hash array is not second.",13,10
.zz_str_not_third:	asciz "Compaction/expansion failed, string heap is not third.",13,10
.zz_table_not_full:	asciz "Compaction/expansion failed, table is not full.",13,10
.zz_too_much:		asciz "Symbol table size is too large. Internal error!",13,10
.zz_too_short:		asciz "Symbol table size is too short. Internal error!",13,10
.zz_str_overflow:	asciz "String symbol table got too large. Internal error!",13,10
.zz_length_mismatch:	asciz "Symbol table table size mismatch. Internal error!",13,10
.zz_too_small_str:	asciz "String symbol table target is too small.",13,10
.zz_too_small_hash:
.zz_too_small_mainhash:	asciz "Main/hash symbol table target is too small.",13,10
.zz_internal_error_expand:
			asciz "Internal error during symbol table expansion!",13,10
.zz_reloc_amount_none:	asciz "No symbols found in given source range.",13,10
.zz_del_amount_none:	asciz "Symbol not found!",13,10
.zz_reloc_amount_1:		asciz "Relocated "
.zz_del_amount_1:		asciz "Deleted "
.zz_reloc_amount_2.plural:
.zz_del_amount_2.plural:	asciz " symbols.",13,10
.zz_reloc_amount_2.singular:
.zz_del_amount_2.singular:	asciz " symbol.",13,10
.zz_reloc_overflow:	asciz "Cannot relocate, length of source range overflows!",13,10
.bb_sym_too_many:	asciz "Too many symbol breakpoints!",13,10
.bb_sym_beyond_linear:	asciz "Symbol breakpoint linear is beyond reach!",13,10
.bb_sym_beyond_offset:	asciz "Symbol breakpoint offset is beyond reach!",13,10
%endif

%if _BREAKPOINTS
.all:		asciz "ALL"
.new:		asciz "NEW"
.bb_no_new:	asciz "No unused breakpoint left!",13,10
.bb_hit.1:	counted "Hit permanent breakpoint "
.bb_hit.2.nocounter:
		counted 13,10
%if _SYMBOLIC
.bb_sym_hit.1:	counted "Hit symbol breakpoint "
.bb_sym_hit.2.nocounter:
		counted 13,10
%endif
.bb_pass.1:	counted "Passed permanent breakpoint "
.bb_hit.2.counter:
.bb_pass.2:	counted ", counter="
.bb_hit.3.counter.no_id:
.bb_pass.3.no_id:
.bb_hitpass_id.after:
		counted 13,10
.bb_hitpass_id.long:
		counted 13,10," ID: "
.bb_hitpass_id.short:
		counted ", ID: "
.bb_when:	asciz " WHEN "

.bp:		asciz "BP "
.bpenabled:	asciz " +"
.bpdisabled:	asciz " -"
.bpunused:	asciz " Unused"
.bpaddress:	asciz " Lin="
.bpcontent:	asciz " ("
.bpcounter:	asciz ") Counter="
%if 0
BP 00 Unused
BP 00 + Lin=12345678 (CC) Counter=8000
1234567890123456789012345678901234567890
%endif
.bpnone:	asciz "No breakpoints set currently.",13,10
.bpnone_at:	asciz "No breakpoint set at given address currently.",13,10
%endif
.cant_bp_the:			asciz "The "
.cant_bp_type_proceed:		asciz "proceed breakpoint"
.cant_bp_type_permanent:	db    "permanent breakpoint "
.cant_bp_type_permanent.index:	asciz "__"
%if _SYMBOLIC
.cant_bp_type_symbol:		db    "symbol breakpoint "
.cant_bp_type_symbol.index:	asciz "__"
%endif
.cant_bp_type_gg:		asciz " G breakpoint"
.cant_bp_linear:		db    " (linear "
.cant_bp_linear.address1:	db    "----_"
.cant_bp_linear.address2:	asciz "----) "
.cant_bp_write:			asciz "cannot be written."
.cant_bp_restore:		db    "cannot be restored to "
.cant_bp_restore.value:		asciz "__."
%if 0
The 15th G breakpoint (linear 0010_FFFF) cannot be written.
The proceed breakpoint (linear 0010_FFFF) cannot be written.
The permanent breakpoint 0F (linear 0010_FFFF) cannot be written.
The permanent breakpoint 0F (linear 0010_FFFF) cannot be restored to __.
12345678901234567890123456789012345678901234567890123456789012345678901234567890
%endif
.cant_bp_reason:		asciz 13,10," Reason: "
.cant_bp_reason0:		asciz "No error. (Internal error, report!)",13,10
.cant_bp_reason1:		asciz "It is read-only.",13,10
.cant_bp_reason2:		asciz "It is unreachable.",13,10
.cant_bp_reason3:		db    "It has been overwritten with "
.cant_bp_reason3.value:		asciz "__.",13,10
.cant_bp_reasonu:		asciz "Unknown error. (Internal error, report!)",13,10

.list_bp.first:	asciz "   "
.list_bp.second:db " G breakpoint, linear "
.list_bp.address1:
		db "----_"
.list_bp.address2:
		asciz "----"
.list_bp.third:	db ", content "
.list_bp.value:
		asciz "__"
.list_bp_not_cseip: equ crlf
%if _PM
.list_bp_cseip_32:
		asciz " (is at CS:EIP)",13,10
%endif
.list_bp_csip_16:
		asciz " (is at CS:IP)",13,10
.list_bp_none:
		asciz "The G breakpoint list is empty.",13,10
%if 0
   2nd G breakpoint, linear 0003_28D3 $3600:12345678, content CC (is at CS:EIP)
12345678901234567890123456789012345678901234567890123456789012345678901234567890
%endif
.empty_message:	asciz
.list:		asciz "LIST"
.again:		asciz "AGAIN"
%if _SYMBOLIC
.wrt:		asciz "WRT"
%endif
.uu_too_many_repeat:	asciz "Reached limit of repeating disassembly.",13,10
.uu_internal_error:	asciz "Internal error in disassembler!",13,10
.aa_internal_error:	asciz "Internal error in assembler!",13,10
.stack_overflow:	db "Stack overflow occurred, IP="
.stack_overflow.caller:	asciz "____h, due to "
.stack_overflow.indirection:	asciz "expression indirection.",13,10
.stack_overflow.parens:		asciz "expression parentheses.",13,10
.stack_overflow.precedence:	asciz "expression precedence.",13,10
.stack_overflow.value_in:	asciz "expression VALUE x IN y.",13,10
.stack_overflow.linear:		asciz "expression LINEAR.",13,10
%if _PM
.stack_overflow.desctype:	asciz "expression DESCTYPE.",13,10
%endif
.stack_overflow.cond:		asciz "expression conditional ?? x :: y.",13,10
%if _SYMBOLIC
.dd_after_symbol.non_wrt:
.uu_after_symbol.non_wrt:		 db ":"
.dd_after_symbol.2_wrt:
.memref_after_symbol.non_wrt:
.uu_after_symbol.wrt:
.memref_after_symbol.wrt:		asciz 13,10
.dd_after_symbol.1_wrt:
.uu_after_symbol_between_1.wrt:
.uu_between_symbol.wrt:			 db ":"
.memref_between_symbol.wrt:		asciz " wrt "
.uu_after_symbol_between_1.non_wrt:	 db ":"
.uu_after_symbol_between_2.wrt:		asciz " + "
.uu_after_symbol_between_3:		asciz 13,10
%endif
%if _MEMREF_AMOUNT
 %if _DEBUG2 || _SYMBOLIC
.memrefs_branchdirect:	asciz 9, "direct branch target = "
.memrefs_stringsource:	asciz 9, "string source        = "
.memrefs_stringdest:	asciz 9, "string destination   = "
.memrefs_memsource:	asciz 9, "memory source        = "
.memrefs_memdest:	asciz 9, "memory destination   = "
.memrefs_memsourcedest:	asciz 9, "memory source/dest   = "
.memrefs_mem_unknown:	asciz 9, "memory (unknown)     = "
.memrefs_unknown:	asciz 9, "unknown mem ref type = "
.memrefs_length:	counted " length="
 %endif
.memrefs_invalid_internal:
		asciz "Internal error, invalid use of too many memrefs!",13,10
%endif

%if 0
	align 2, db 0
.optiontable:	dw dispregs32, .r32off, .r32on
		dw traceints, .traceoff, .traceon
		dw cpdepchars, .cpoff, .cpon
		dw fakeindos, .dosoff, .doson
		dw nonpagingdevice, .nonpageoff, .nonpageon
		dw pagingdevice, .pageoff, .pageon
		dw hexrn, .readrnoff, .readrnon
		dw 0

.r32off:	asciz "Dump 16-bit register set"
.r32on:		asciz "Dump 32-bit register set"
.traceoff:	asciz "Interrupts are traced"
.traceon:	asciz "Interrupts are processed"
.cpoff:		asciz "Extended ASCII characters replaced"
.cpon:		asciz "Extended ASCII characters displayed"
.dosoff:	asciz "InDOS is checked"
.doson:		asciz "InDOS assumed on"
		;asciz "InDOS assumed off"
.nonpageoff:	asciz
.nonpageon:	asciz "Paging disabled"
.pageoff:	asciz
.pageon:	asciz "Paging enabled"
.readrnoff:	asciz "Readable RN enabled"
.readrnon:	asciz "Readable RN disabled"
%endif

.warnprefix:	asciz "Warning: Prefixes in excess of 14, using trace flag.",13,10

%if _DEBUG
.bu:		asciz "Breaking to next instance.",13,10
 %if _DEBUG_COND
.bu_disabled:	db "Debuggable mode is disabled.",13,10
		asciz "Enable with this command: r DCO6 or= ",_4digitshex(opt6_debug_mode),13,10
 %endif
%else
.notbu:		asciz "Already in topmost instance. (This is no debugging build of lDebug.)",13,10
%endif
%if _DUALCODE
.bu_relocated:	db "Inter-segment calls work. Sign="
.bu_relocated.sign:
		asciz "----h.",13,10
%endif
%if _DT
.tableheader:	db .tableheader.end - .tableheader.start
.tableheader.start:
 %assign ITERATION 0
 %rep 8
  %if ITERATION == 0
  %elif ITERATION == 1 || ITERATION == 2
		times 4 db 32
  %elif ITERATION == 3 || ITERATION == 4 || ITERATION == 5
		times 2 db 32
  %else
		times 3 db 32
  %endif
		db "Dec Hex"
  %assign ITERATION ITERATION + 1
 %endrep
		db 13,10
.tableheader.end:
.tableheadertop:db .tableheadertop.end - .tableheadertop.start
.tableheadertop.start:
 %assign ITERATION 0
 %rep 8
  %if ITERATION == 0
  %else
		times 3 db 32
  %endif
		db "Dec Hex"
  %assign ITERATION ITERATION + 1
 %endrep
		db 13,10
.tableheadertop.end:
%endif
%if _PM
.ofs32:		asciz "Cannot access 16-bit segment with 32-bit offset.",13,10
.d.a_success:	db "Allocated descriptor with selector "
.d.a_success_sel:
		asciz "----",13,10
.d.d_success:	asciz "Deallocated descriptor",13,10
.d.b_success:	asciz "Set descriptor base",13,10
.d.l_success:	asciz "Set descriptor limit",13,10
.d.t_success:	asciz "Set descriptor type",13,10
.d.a_error:	db "Error "
.d.a_error_code:
		asciz "----",13,10
.d.d_error equ .d.a_error
.d.d_error_code equ .d.a_error_code
.d.b_error equ .d.a_error
.d.b_error_code equ .d.a_error_code
.d.l_error equ .d.a_error
.d.l_error_code equ .d.a_error_code
.d.t_error equ .d.a_error
.d.t_error_code equ .d.a_error_code
%endif

%if _MCB
 %assign ELD 0
 %include "dmmsg.asm"
%endif


%define INSTALLMESSAGES db ""

%imacro installflag 6-*.nolist
 %xdefine %%one %1
 %xdefine %%two %2
 %xdefine %%three %3
 %ifempty %3
  %define %%three install_trying
 %endif
 %xdefine %%four %4
 %ifempty %4
  %define %%four 0
 %endif
 %xdefine %%five %5
%ifnidn %%one, 0
 %if ((%%two - 1) & %%two) == 0
  %if %%two > 0FFFFh
   %if %%two & 0FFFFh
    %error Flag set in both upper and lower word
   %endif
   %assign %%two %%two >> 16
   %xdefine %%one %%one + 2
  %endif
 %else
  %if (%%two & 0FFFF_0000h) != 0FFFF_0000h
   %if (%%two & 0FFFFh) != 0FFFFh
    %error Flag clear in both upper and lower word
   %endif
   %assign %%two (%%two >> 16) & 0FFFFh
   %xdefine %%one %%one + 2
  %endif
 %endif
%endif
  %xdefine INSTALLMESSAGES INSTALLMESSAGES, %%message_five:, {asciz %%five}
 %assign %%step 0
 %rep %0 - 5
  %xdefine INSTALLMESSAGES INSTALLMESSAGES, %%message_%[%%step]:, {asciz %6}
	dw %%message_%[%%step]
	dw %%message_five
	dw %%one, %%two, %%three, %%four
  %rotate 1
  %assign %%step %%step + 1
 %endrep
%endmacro

%imacro installmessages 0-*.nolist
 %rep %0
	%1
  %rotate 1
 %endrep
%endmacro

installformat equ 1


	align 4, db 0
installflags:
%if _PM
installflag options4, opt4_int_2F_hook,,, \
	"Interrupt 2Fh DPMI hook", "INT2F", "DPMIHOOK"
%endif
%if _CATCHINTFAULTCOND && (_CATCHINT0D || _CATCHINT0C)
installflag options4, opt4_int_fault_hook,,, \
	"R86M fault interrupts hook", "FAULTS", "FAULT", "INTFAULTS", "INTFAULT"
%endif
%if _CATCHINT08
installflag options4, opt4_int_08_hook,,, \
	"Interrupt 8 timer hook", "INT08", "INT8", "TIMER"
%endif
%if _CATCHINT2D
installflag options4, opt4_int_2D_hook,,, \
	"Interrupt 2Dh AMIS hook", "INT2D", "AMIS"
%endif
%if _AREAS && _AREAS_HOOK_CLIENT
installflag 0, install_areas,,, \
	"Areas", "AREAS"
%endif
installflag options, enable_serial, dmycmd,, \
	"Serial I/O", "SERIAL"
installflag options, fakeindos, dmycmd,, \
	"Force InDOS mode", "INDOS"
installflag options, opt_usegetinput, dmycmd,, \
	"Use DOS getinput mode", "GETINPUT"
installflag options3, opt3_r_highlight_full, dmycmd,, \
	"R register change highlighting", "RHIGHLIGHT"
installflag options3, ~ opt3_disable_autorepeat, dmycmd,, \
	"Autorepeat", "AUTOREPEAT"
installflag options, ~ nonpagingdevice, dmycmd,, \
	"Paging", "PAGING"
installflag options6, opt6_bios_output, dmycmd,, \
	"ROM-BIOS output", "BIOSOUT", "BIOSOUTPUT", "INT10OUT", "INT10OUTPUT"
installflag options6, opt6_flat_binary, dmycmd,, \
	"Flat binary read mode", "FLAT", "FLATBIN", \
	"FLATBINARY", "FSWITCH"
installflag options6, opt6_big_stack, dmycmd,, \
	".BIG style stack mode", "BIG", "BIGSTACK", "ESWITCH"
%if _RH
installflag options6, opt6_rh_mode, dmycmd,, \
	"Register dump history", "RH", "RHISTORY", "REGHISTORY"
%endif
%if _DEBUG && _DEBUG_COND
installflag options6, opt6_debug_mode,,, \
	"Debuggable mode", "DEBUG"
%endif
installflag options3, opt3_paging_rc, dmycmd,, \
	"Paging RC commands mode", "PAGINGRC"
installflag options3, opt3_paging_re, dmycmd,, \
	"Paging RE commands mode", "PAGINGRE"
installflag options3, opt3_paging_yy, dmycmd,, \
	"Paging Script for lDebug mode", "PAGINGY", "PAGINGYY", "PAGINGSCRIPT"
%ifn _ONLYNON386
installflag options, dispregs32, dmycmd,, \
	"386 register dump mode", "RX", "REGS386", "REGS32"
%endif
installflag options, traceints, dmycmd,, \
	"Trace interrupts Mode", "TM", "TRACEINTS", "TRACEINTERRUPTS"
installflag options7, opt7_houdini, dmycmd,, \
	"Houdini Mode", "HOUDINI"
installflag options7, opt7_no_free_oneshot_eld, dmycmd,, \
	"No free oneshot ELDs mode", "NOFREEONESHOT"
installflag options7, opt7_quiet_install, dmycmd,, \
	"Quiet ELD install", "QUIETINSTALL"
installflag options7, opt7_debug_optional_link, dmycmd,, \
	"Debug ELD optional link mode", "DEBUGOPTLINK"
installflag options7, opt7_quick_run, dmycmd,, \
	"Quick ELD run", "QUICKRUN"
installflag options7, opt7_no_ensure, dmycmd,, \
	"No ensure debuggee loaded", "NOENSURE"
	dw 0, 0

installmessages INSTALLMESSAGES


%if _MCB
smcbmsg smcb_messages

smcbmsg_unknown:	asciz "unknown"

%undef smcb_messages
%unimacro smcbtype 2.nolist
%unimacro smcbmsg 2-*.nolist
%endif


errcarat:	db "^ Error",7
crlf:		asciz 13,10

%if _SYMBOLIC
pre_str_list:
	db -1, "", 0
.end:
%endif


	align 4, db 0
msgtable_value_range:
	dw msg.executing, msg.executing_value_range
%if _ACCESS_VARIABLES_AMOUNT
	dw .reading, .reading_range
	dw .writing, .writing_range
	dw .memoperand, .memoperand_range
	dw .accessing, .accessing_range
%endif
	dw 0

%if _ACCESS_VARIABLES_AMOUNT
.reading:		asciz "READING"
.reading_range:
 %assign iicounter 0
 %define iiprefix ""
 %rep _ACCESS_VARIABLES_AMOUNT
	_autohexitsstrdef IIDEF, iicounter
			db iiprefix,"FROM readadr",_IIDEF," LENGTH readlen",_IIDEF
 %assign iicounter iicounter + 1
 %define iiprefix ", "
 %endrep
			asciz
.writing:		asciz "WRITING"
.writing_range:
 %assign iicounter 0
 %define iiprefix ""
 %rep _ACCESS_VARIABLES_AMOUNT
	_autohexitsstrdef IIDEF, iicounter
			db iiprefix,"FROM writadr",_IIDEF," LENGTH writlen",_IIDEF
 %assign iicounter iicounter + 1
 %define iiprefix ", "
 %endrep
			asciz

.memoperand:		asciz "MEMOPERAND"
.memoperand_range:	asciz "READING, WRITING"
.accessing:		asciz "ACCESSING"
.accessing_range:	asciz "READING, WRITING, EXECUTING"
%endif


%if _BOOTLDR
%define lot_list
%define lot_comma
%macro lot_entry 2.nolist
LOAD_%2 equ %1
	dw LOAD_%2, .%2
%defstr %%string %2
%xdefine lot_list lot_list lot_comma .%2:, db %%string, db 0
%define lot_comma ,
%endmacro

%macro lot_messages 0-*.nolist
%rep (%0 / 3)
%1
	%2
	%3
%rotate 3
%endrep
%endmacro

	align 4, db 0
loadoptiontable:
	lot_entry    1, SET_DL_UNIT
	lot_entry    2, SET_BL_UNIT
	lot_entry    4, SET_SIDI_CLUSTER
	lot_entry  10h, SET_DSSI_DPT
	lot_entry  20h, PUSH_DPT
	lot_entry  40h, DATASTART_HIDDEN
	lot_entry  80h, SET_AXBX_DATASTART
	lot_entry 100h, SET_DSBP_BPB
	lot_entry 200h, LBA_SET_TYPE
	lot_entry 400h, MESSAGE_TABLE
	lot_entry 800h, SET_AXBX_ROOT_HIDDEN
	lot_entry 1000h, CMDLINE
	lot_entry 2000h, NO_BPB
	lot_entry 4000h, SET_DSSI_PARTINFO
	dw 0, 0

.incompatible:
	dw LOAD_SET_BL_UNIT, LOAD_SET_AXBX_DATASTART
	dw LOAD_SET_BL_UNIT, LOAD_SET_AXBX_ROOT_HIDDEN
	dw LOAD_SET_AXBX_DATASTART, LOAD_SET_AXBX_ROOT_HIDDEN
	dw LOAD_SET_SIDI_CLUSTER, LOAD_SET_DSSI_DPT
	dw LOAD_SET_DSBP_BPB, LOAD_SET_DSSI_DPT
	dw LOAD_NO_BPB, LOAD_SET_DSBP_BPB
	dw LOAD_NO_BPB, LOAD_LBA_SET_TYPE
	dw LOAD_NO_BPB, LOAD_MESSAGE_TABLE
	dw LOAD_SET_DSSI_PARTINFO, LOAD_SET_SIDI_CLUSTER
	dw LOAD_SET_DSSI_PARTINFO, LOAD_SET_DSSI_DPT
	dw LOAD_SET_DSSI_PARTINFO, LOAD_SET_DSBP_BPB
	dw 0, 0

lot_messages lot_list

%unmacro lot_entry 2.nolist
%unmacro lot_messages 0-*.nolist


msdos7_message_table:
		; the first four bytes give displacements to the various
		;  messages. an ASCIZ message indicates that this was the
		;  last message. a message terminated by 0FFh indicates
		;  that the last message (displacement at table + 3) is
		;  to follow after this message.
		; the maximum allowed displacement is 7Fh. the minimum
		;  allowed displacement is 1, to avoid a zero displacement.
		; only the last message is terminated by a zero byte,
		;  as that zero byte indicates the end of the message table.
		;  (the entire table is treated as one ASCIZ string.)
		; MS-DOS 7.10 from MSW 98 SE seems to have at least 167h (359)
		;  bytes allocated to its buffer for these.
		;
		; this message table was discussed in a dosemu2 repo at
		;  https://github.com/stsp/dosemu2/issues/681
.:	db .msg_invalid_system - ($ + 1)
	db .msg_io_error - ($ + 1)
	db .msg_invalid_system - ($ + 1)
	db .msg_press_any_key - ($ + 1)

.msg_invalid_system:
	db 13,10,"Invalid system", -1

.msg_io_error:
	db 13,10,"I/O error", -1

.msg_press_any_key:
	db 13,10,"Change disk and press any key",13,10,0
.end:
.size: equ .end - .

%if .size > 150h
 %error Message table too large!
%endif



	align 4, db 0
loadsettings:
	istruc LOADSETTINGS
at lsKernelName,	dw msg.ldos_kernel_name
at lsAddName,		dw msg.addname_empty
at lsMinPara,		dw 60h
at lsMaxPara,		dw 0
at lsOptions,		dw LOAD_CMDLINE
at lsSegment,		dw 200h
at lsEntry,		dd 400h
at lsBPB,		dw 7C00h, -1
at lsCheckOffset,	dw 1020
at lsCheckValue,	db "lD"
at lsName,		asciz "LDOS"
	iend
	istruc LOADSETTINGS
at lsKernelName,	dw msg.freedos_kernel_name
at lsAddName,		dw msg.addname_empty
at lsMinPara,		dw 20h
at lsMaxPara,		dw -1
at lsOptions,		dw LOAD_SET_DL_UNIT | LOAD_SET_BL_UNIT
at lsSegment,		dw 60h
at lsEntry,		dd 0
at lsBPB,		dw 7C00h, -1
at lsName,		asciz "FREEDOS"
	iend
	istruc LOADSETTINGS
at lsKernelName,	dw msg.dosc_kernel_name
at lsAddName,		dw msg.addname_empty
at lsMinPara,		dw 20h
at lsMaxPara,		dw -1
at lsOptions,		dw LOAD_SET_DL_UNIT | LOAD_SET_BL_UNIT
at lsSegment,		dw 2000h
at lsEntry,		dd 0
at lsBPB,		dw 7C00h, 0
at lsName,		asciz "DOSC"
	iend
	istruc LOADSETTINGS
at lsKernelName,	dw msg.edrdos_kernel_name
at lsAddName,		dw msg.addname_empty
at lsMinPara,		dw 20h
at lsMaxPara,		dw -1
at lsOptions,		dw LOAD_SET_DL_UNIT | LOAD_SET_BL_UNIT \
			 | LOAD_SET_DSBP_BPB
at lsSegment,		dw 70h
at lsEntry,		dd 0
at lsBPB,		dw 7C00h, -1
at lsName,		asciz "EDRDOS"
	iend
	istruc LOADSETTINGS
at lsKernelName,	dw msg.msdos6_kernel_name
at lsAddName,		dw msg.msdos6_add_name
at lsMinPara,		dw 20h
at lsMaxPara,		dw 60h
at lsOptions,		dw LOAD_SET_DL_UNIT | LOAD_SET_AXBX_DATASTART \
			 | LOAD_DATASTART_HIDDEN | LOAD_SET_DSSI_DPT \
			 | LOAD_PUSH_DPT
at lsSegment,		dw 70h
at lsEntry,		dd 0
at lsBPB,		dw 7C00h, 0
at lsName,		asciz "MSDOS6"
	iend
	istruc LOADSETTINGS
at lsKernelName,	dw msg.rxdos.0_kernel_name
at lsAddName,		dw msg.rxdos.0_add_name
at lsMinPara,		dw 20h
at lsMaxPara,		dw 60h
at lsOptions,		dw LOAD_SET_DL_UNIT | LOAD_SET_AXBX_ROOT_HIDDEN \
			 | LOAD_SET_DSSI_DPT | LOAD_PUSH_DPT
at lsSegment,		dw 70h
at lsEntry,		dd 0
at lsBPB,		dw 7C00h, 0
at lsName,		asciz "RXDOS.0"
	iend
	istruc LOADSETTINGS
at lsKernelName,	dw msg.rxdos.1_kernel_name
at lsAddName,		dw msg.rxdos.1_add_name
at lsMinPara,		dw 20h
at lsMaxPara,		dw 60h
at lsOptions,		dw LOAD_SET_DL_UNIT | LOAD_SET_AXBX_ROOT_HIDDEN \
			 | LOAD_SET_DSSI_DPT | LOAD_PUSH_DPT
at lsSegment,		dw 70h
at lsEntry,		dd 0
at lsBPB,		dw 7C00h, 0
at lsName,		asciz "RXDOS.1"
	iend
	istruc LOADSETTINGS
at lsKernelName,	dw msg.rxdos.2_kernel_name
at lsAddName,		dw msg.addname_empty
at lsMinPara,		dw 60h
at lsMaxPara,		dw 0
at lsOptions,		dw LOAD_CMDLINE
at lsSegment,		dw 70h
at lsEntry,		dd 400h
at lsBPB,		dw 7C00h, -1
at lsName,		asciz "RXDOS.2"
	iend
	istruc LOADSETTINGS
at lsKernelName,	dw msg.rxdos.2_kernel_name
at lsAddName,		dw msg.addname_empty
at lsMinPara,		dw 60h
at lsMaxPara,		dw 0
at lsOptions,		dw LOAD_CMDLINE
at lsSegment,		dw 200h
at lsEntry,		dd 400h
at lsBPB,		dw 7C00h, -1
at lsCheckOffset,	dw 1020
at lsCheckValue,	db "lD"
at lsName,		asciz "RXDOS.3"
	iend
	istruc LOADSETTINGS
at lsKernelName,	dw msg.ibmdos_kernel_name
at lsAddName,		dw msg.ibmdos_add_name
at lsMinPara,		dw 20h
at lsMaxPara,		dw 80h
at lsOptions,		dw LOAD_SET_DL_UNIT | LOAD_SET_AXBX_DATASTART \
			 | LOAD_DATASTART_HIDDEN | LOAD_SET_DSSI_DPT \
			 | LOAD_PUSH_DPT
at lsSegment,		dw 70h
at lsEntry,		dd 0
at lsBPB,		dw 7C00h, 0
at lsName,		asciz "IBMDOS"
	iend
	istruc LOADSETTINGS
at lsKernelName,	dw msg.ibmdos_kernel_name
at lsAddName,		dw msg.ibmdos_add_name
at lsMinPara,		dw 20h
at lsMaxPara,		dw -1
at lsOptions,		dw LOAD_SET_DL_UNIT | LOAD_SET_AXBX_DATASTART \
			 | LOAD_DATASTART_HIDDEN | LOAD_SET_DSSI_DPT \
			 | LOAD_PUSH_DPT
at lsSegment,		dw 70h
at lsEntry,		dd 0
at lsBPB,		dw 7C00h, 0
at lsName,		asciz "DRDOS"
	iend
	istruc LOADSETTINGS
at lsKernelName,	dw msg.msdos7_kernel_name
at lsAddName,		dw msg.addname_empty
at lsMinPara,		dw 40h
at lsMaxPara,		dw 80h
at lsOptions,		dw LOAD_SET_DL_UNIT | LOAD_SET_SIDI_CLUSTER \
			 | LOAD_DATASTART_HIDDEN | LOAD_PUSH_DPT \
			 | LOAD_LBA_SET_TYPE | LOAD_MESSAGE_TABLE
at lsSegment,		dw 70h
at lsEntry,		dd 200h
at lsBPB,		dw 7C00h, -1
at lsCheckOffset,	dw 200h
at lsCheckValue,	db "BJ"
at lsName,		asciz "MSDOS7"
	iend
	istruc LOADSETTINGS
at lsKernelName,	dw msg.ntldr_kernel_name
at lsAddName,		dw msg.addname_empty
at lsMinPara,		dw 20h
at lsMaxPara,		dw -1
at lsOptions,		dw LOAD_SET_DL_UNIT \
			 | LOAD_DATASTART_HIDDEN
at lsSegment,		dw 2000h
at lsEntry,		dd 0
at lsBPB,		dw 7C00h, 0
at lsName,		asciz "NTLDR"
	iend
	istruc LOADSETTINGS
at lsKernelName,	dw msg.bootmgr_kernel_name
at lsAddName,		dw msg.addname_empty
at lsMinPara,		dw 20h
at lsMaxPara,		dw -1
at lsOptions,		dw LOAD_SET_DL_UNIT \
			 | LOAD_DATASTART_HIDDEN
at lsSegment,		dw 2000h
at lsEntry,		dd 0
at lsBPB,		dw 7C00h, 0
at lsName,		asciz "BOOTMGR"
	iend
	istruc LOADSETTINGS
at lsKernelName,	dw msg.chain_kernel_name
at lsAddName,		dw msg.addname_empty
at lsMinPara,		dw paras(512)
at lsMaxPara,		dw paras(8192)
at lsOptions,		dw LOAD_SET_DL_UNIT | LOAD_SET_DSSI_PARTINFO \
			 | LOAD_NO_BPB
at lsSegment,		dw 7C0h
at lsEntry,		dw 7C00h, -7C0h
at lsBPB,		dw 7C00h, 0
at lsCheckOffset,	dw 510
at lsCheckValue,	dw 0AA55h
at lsName,		asciz "CHAIN"
	iend
			dw 0
%endif


%if _APPLICATION || _DEVICE
dskerrs:	db dskerr0-dskerrs,dskerr1-dskerrs
		db dskerr2-dskerrs,dskerr3-dskerrs
		db dskerr4-dskerrs,dskerr9-dskerrs
		db dskerr6-dskerrs,dskerr7-dskerrs
		db dskerr8-dskerrs,dskerr9-dskerrs
		db dskerra-dskerrs,dskerrb-dskerrs
		db dskerrc-dskerrs
dskerr0:	asciz "Write protect error"
dskerr1:	asciz "Unknown unit error"
dskerr2:	asciz "Drive not ready"
dskerr3:	asciz "Unknown command"
dskerr4:	asciz "Data error (CRC)"
dskerr6:	asciz "Seek error"
dskerr7:	asciz "Unknown media type"
dskerr8:	asciz "Sector not found"
dskerr9:	asciz "Unknown error"
dskerra:	asciz "Write fault"
dskerrb:	asciz "Read fault"
dskerrc:	asciz "General failure"
reading:	asciz " read"
writing:	asciz " writ"
drive:		db "ing drive "
driveno:	asciz "_"
%endif
msg8088:	asciz "8086/88"
msgx86:		asciz "x86"
no_copr:	asciz " without coprocessor"
has_copr:	asciz " with coprocessor"
has_287:	asciz " with 287"
tmodes:		db "trace mode is "
tmodev:		asciz "_ - interrupts are "
tmode1:		asciz "traced"
tmode0:		asciz "processed"
unused:		asciz " (unused)"
needsmsg:
.:		db "[needs "
.digit_x_ofs: equ $ - .
		db "x8"
.digit_6_ofs: equ $ - .
		db "6]"
needsmsg_L:	equ $-needsmsg
needsmath:	db "[needs math coprocessor]"
needsmath_L:	equ $-needsmath
obsolete:	db "[obsolete]"
obsolete_L:	equ $-obsolete
int0msg:	asciz "Divide error",13,10
int1msg:	asciz "Unexpected single-step interrupt",13,10
int3msg:	asciz "Unexpected breakpoint interrupt",13,10
%if _CATCHINT06
int6msg:	asciz "Invalid opcode",13,10
%endif
%if _CATCHINT08
int8msg:	asciz "Detected Control pressed for a while",13,10
int8_kbd_msg:	asciz "Detected Control pressed for a while (Keyboard enabled)",13,10
runint_ctrlc_msg:
		asciz "Detected double Control-C via serial",13,10
%endif
%if _CATCHINT07
int7msg:	asciz "No x87 present",13,10
%endif
%if _CATCHINT0C
int0Cmsg:	asciz "Stack fault (in R86M)",13,10
%endif
%if _CATCHINT0D
int0Dmsg:	asciz "General protection fault (in R86M)",13,10
%endif
%if _CATCHINT18
int18msg:	asciz "Diskless boot hook called",13,10
%endif
%if _CATCHINT19
int19msg:	asciz "Boot load called",13,10
%endif
%if _CATCHSYSREQ
sysreqmsg:	asciz "SysReq detected",13,10
%endif
%if _PM
 %if _CATCHEXC06
exc6msg:	asciz "Invalid opcode fault",13,10
 %endif
 %if _CATCHEXC0C
excCmsg:	asciz "Stack fault",13,10
 %endif
excDmsg:	asciz "General protection fault",13,10
%endif
%if _PM || _CATCHINT07 || _CATCHINT0C || _CATCHINT0D
 %if _EXCCSIP
excloc:		db "CS:IP="
exccsip:	asciz "    :    ",13,10
 %endif
 %if _AREAS
msg.area_hh_indirection_memory_access:
		asciz "Expression indirection fault: "
msg.area_rr_variable_read_access:
		asciz "Memory variable read access fault: "
msg.area_rr_variable_write_access:
		asciz "Memory variable write access fault: "
msg.area_uu_referenced_memory_access:
		asciz 13,10,"Disassembly referenced memory fault: "
msg.area_uu_simulate_scas:
		asciz "Disassembly SCAS simulation fault: "
msg.area_uu_simulate_cmps:
		asciz "Disassembly CMPS simulation fault: "
msg.area_aa_access:
		asciz "Assembly fault: "
msg.area_dd_access:
		asciz "Dump data fault: "
msg.area_ee_interactive_access:
		db 13,10
msg.area_ee_access:
		asciz "Enter data fault: "
msg.area_rr_access:
		asciz "Register command fault: "
msg.area_sss_access:
		asciz "Search command fault: "
msg.area_run_access:
		asciz "Run command fault: "
msg.area_uu_access:
		asciz "Disassembly fault: "
 %endif
%endif
%if _PM
excEmsg:	asciz "Page fault",13,10
 %if _BREAK_INSTALLDPMI
installdpmimsg:	asciz "Entered Protected Mode",13,10
 %endif
nodosext:	asciz "Command not supported in protected mode without a DOS extender",13,10
nopmsupp:	asciz "Command not supported in protected mode",13,10
 %if _DISPHOOK
dpmihook:	db "DPMI entry hooked, new entry="
dpmihookcs:	asciz "____:",_4digitshex(mydpmientry+DATASECTIONFIXUP),13,10
  %if _DEBUG
dpmihookamis:	asciz "DPMI entry hooked by other debugger with AMIS callout",13,10
  %endif
 %endif
msg.dpmi_no_hook:	asciz "DPMI entry cannot be hooked!",13,10
nodesc:		asciz "resource not accessible in real mode",13,10
;descwrong:	asciz "descriptor not accessible",13,10
msg.msdos:	asciz "MS-DOS"
descriptor:	db "---- base="
.base:		db "-------- limit="
.limit:		db "-------- attr="
.attrib:	db "----",13,10
		asciz
%endif	; _PM
ph_msg:		asciz "Error in sequence of calls to hack.",13,10

progtrm:	db 13,10,"Program terminated normally ("
progexit:	asciz "____)",13,10
%if _APPLICATION || _DEVICE
nowhexe:	asciz "EXE and HEX files cannot be written",13,10
nownull:	asciz "Cannot write: no file name given",13,10
msg.hexerror:	asciz "Error in HEX file",13,10
wwmsg1:		asciz "Writing "
wwmsg2:		asciz " bytes",13,10
diskful:	asciz "Disk full",13,10
openerr:	db "Error "
openerr1:	asciz "____ opening file",13,10
doserr2:	asciz "File not found",13,10
doserr3:	asciz "Path not found",13,10
doserr5:	asciz "Access denied",13,10
doserr8:	asciz "Insufficient memory",13,10
doserr11:	asciz "Invalid format",13,10
%endif

%if _EMS
 %define dw_emserr dw
 %include "xxmsg.asm"
%endif

		align 4, db 0
flagbits:
.:		dw 800h,400h,200h, 80h,040h,010h,004h,001h
.amount: equ ($ - .) / 2
flagson:	dw "OV","DN","EI","NG","ZR","AC","PE","CY"
flagsoff:	dw "NV","UP","DI","PL","NZ","NA","PO","NC"
flagnames:	dw "OF","DF","IF","SF","ZF","AF","PF","CF"
%if _REGSREADABLEFLAGS
flagson_style23:dw "O1","D1","I1","S1","Z1","A1","P1","C1"
flagsoff_style2:dw "O0","D0","I0","S0","Z0","A0","P0","C0"
flagsoff_style3:dw "O_","D_","I_","S_","Z_","A_","P_","C_"
%endif
%if _40COLUMNS
flagbits_for_40: equ 1____0____0____1____1____0____0____1b
flagbits_for_80: equ 1____1____1____1____1____1____1____1b
flagbits_for_shl: equ 16 - flagbits.amount

		align 2, db 0
shortflagbits:
.:		dw 400h, 200h
.amount: equ ($ - .) / 2
shortflagson:	dw "D ", "E "
shortflagsoff:	dw "U ", "D "
%endif

		align 4, db 0
table_length_keywords:
	dw 30, msg.gib
	dw 20, msg.mib
	dw 10, msg.kib
	dw 9, msg.pages
	dw 4, msg.paragraphs
	dw 4, msg.paras
	dw 3, msg.qwords
	dw 2, msg.dwords
	dw 1, msg.words
	dw 0, msg.bytes		; end of table, shift count zero

flagvaron:	db 1
flagvaroff:	db 0		; must be directly behind flagvaron

%if _COND
msg.condnotjump:db "not "
msg.condjump:	asciz "jumping"
%endif

msg.matches:	asciz " matches",13,10

		align 4, db 0
reg8names:	dw "AL","AH","BL","BH","CL","CH","DL","DH"
; Even entries are xL registers, odd ones the xH ones.
; Order matches that of the first four regs entries.

reg16names:	dw "AX","BX","CX","DX","SP","BP","SI","DI"
		dw "DS","ES","SS","CS","FS","GS","IP","FL"
; 32-bit registers are the first eight and last two entries of
;  reg16names with 'E', which are all non-segment registers.
; Segment registers can be detected by the 'S' as second letter.
; FS and GS are the fourth- and third-to-last entries.
; Order matches that of the sixteen regs entries.


		; Table of recognised default (unsigned) types.
		;
		; If any number of characters match, use the type.
		; If an additional "S" is found in front of a valid
		;  type, the type is set to signed. (Word and byte
		;  types are sign-extended to a dword value.)
		;
		; Each odd entry is an alternative name for the even
		;  entry preceding it.
types:
	countedb "BYTE"		; ("B" is hexadecimal)
	countedb "CHAR"		; ("C" is hexadecimal, "CH" is a register)
	countedb "WORD"
	countedb "SHORT"	; (signed "SS" is a register)
	countedb "3BYTE"	; ("3" and "3B" are numeric)
	countedb "3BYTE"
	countedb "DWORD"	; ("D" is hexadecimal)
	countedb "LONG"
.addresses:
	countedb "POINTER"
	countedb "PTR"
	countedb "OFFSET"
	countedb "OFS"
	countedb "SEGMENT"
.end:

maxtypesize equ 7		; size of "SEGMENT" and "POINTER"
