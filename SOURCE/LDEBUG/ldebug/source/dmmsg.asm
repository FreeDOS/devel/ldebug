%if 0

lDebug DM command messages

Copyright (C) 2008-2023 E. C. Masloch

Usage of the works is permitted provided that this
instrument is retained with the works, so that any entity
that uses the works is notified of this instrument.

DISCLAIMER: THE WORKS ARE WITHOUT WARRANTY.

%endif


.invmcbadr:	asciz "End of chain: invalid MCB address.",13,10

%define smcb_messages ..@notype,""

	%imacro smcbtype 2.nolist
		dw %2, %%label
%if ELD
internaldatarelocation
%endif
%defstr %%str	%1
%xdefine smcb_messages smcb_messages,%%label,%%str
	%endmacro

	%imacro smcbmsg 2-*.nolist
%if %0 & 1
 %error Expected even number of parameters
%endif
%rotate 2
%rep (%0 - 2) / 2
%1:	asciz %2
%rotate 2
%endrep
	%endmacro

	align 4, db 0
smcbtypes:
smcbtype S_OTHER,	00h
smcbtype S_DOSENTRY,	01h
smcbtype S_DOSCODE,	02h
smcbtype S_DOSDATA,	03h
smcbtype S_IRQSCODE,	04h
smcbtype S_IRQSDATA,	05h
smcbtype S_CDS,		06h
smcbtype S_LFNCDS,	07h
smcbtype S_DPB,		08h
smcbtype S_UPB,		09h
smcbtype S_SFT,		0Ah
smcbtype S_FCBSFT,	0Bh
smcbtype S_CCB,		0Ch
smcbtype S_IRT,		0Dh
smcbtype S_SECTOR,	0Eh
smcbtype S_NLS,		0Fh
smcbtype S_EBDA,	10h
smcbtype S_INITCONFIG,	19h
smcbtype S_INITFATSEG,	1Ah
smcbtype S_INITSECTORSEG,	1Bh
smcbtype S_INITSTACKBPB,1Ch
smcbtype S_INITPSP,	1Dh
smcbtype S_ENVIRONMENT,	1Eh
smcbtype S_INITIALIZE,	1Fh
smcbtype S_DEVICE,	20h					; Device
smcbtype S_DEVICEMEMORY,21h					; Allocated by device
smcbtype S_EXCLDUMA,	30h					; Excluded UMA
smcbtype S_EXCLDUMASUB,	31h					; Excluded UMA with sub-chain of used MCBs
smcbtype S_EXCLDLH,	32h					; Excluded by LH
smcbtype S_EXCLDDOS,	33h
	dw -1, -1
