%if 0

lDebug help message pages

Copyright (C) 1995-2003 Paul Vojta
Copyright (C) 2008-2023 E. C. Masloch

Usage of the works is permitted provided that this
instrument is retained with the works, so that any entity
that uses the works is notified of this instrument.

DISCLAIMER: THE WORKS ARE WITHOUT WARRANTY.

%endif

%ifndef MESSAGE_INLINE
 %include "debug.mac"
 %include "options.mac"
%endif

	db "More options: (read/write DCO3, read DCS3)",13,10
	db _4digitshex(opt3_tt_no_paging)," T: do not page output",13,10
	db _4digitshex(opt3_tp_no_paging)," TP: do not page output",13,10
	db _4digitshex(opt3_pp_no_paging)," P: do not page output",13,10
	db _4digitshex(opt3_gg_no_paging)," G: do not page output",13,10
	db _4digitshex(opt3_silence_paging_set), " T/TP/P: modify paging for silent dump",13,10
	db _4digitshex(opt3_silence_paging_on), " T/TP/P: if ",_4digitshex(opt3_silence_paging_set)," set: turn paging on, else off",13,10
%if _REGSHIGHLIGHT
	db _6digitssephex(opt3_r_highlight_diff), " R: highlight changed digits (needs ANSI for DOS output)",13,10
	db _6digitssephex(opt3_r_highlight_dumb), " R: highlight escape sequences to int 10h, else video attributes",13,10
	db _6digitssephex(opt3_r_highlight_full), " R: highlight changed registers (overrides ",_6digitssephex(opt3_r_highlight_diff),")",13,10
	db _6digitssephex(opt3_r_highlight_eip), " R: include highlighting of EIP",13,10
%endif
%if _PM
	db _6digitssephex(opt3_ss_b_bit_set), " set PM ss B bit",13,10
 %if _BREAK_INSTALLDPMI
	db _6digitssephex(opt3_break_installdpmi), " break on entering Protected Mode",13,10
 %endif
%endif
%if _GETLINEHIGHLIGHT
	db _8digitssephex(opt3_getline_highlight), " highlight prefix/suffix in getinput if text parts are not visible",13,10
%endif

	db _8digitssephex(opt3_no_idle_2F), " do not call int 2F.1680 for idling",13,10
%if _DELAY_BEFORE_BP
	db _8digitssephex(opt3_delay_before_bp), " delay for a tick before writing breakpoints",13,10
%endif
	db _8digitssephex(opt3_no_call_amis), " do not call other lDebug instance's AMIS services",13,10
	db _8digitssephex(opt3_disable_autorepeat), " disable auto-repeat",13,10
	db _8digitssephex(opt3_check_ctrlc_keyb),   " check int 16h buffer for Control-C if inputting from int 16h",13,10
	db _8digitssephex(opt3_check_ctrlc_0bh),    " call DOS service 0Bh to check for Control-C",13,10
	db _8digitssephex(opt3_tsr_quit_leave_tf),  " when Q command is used while TSR, leave TF as is",13,10
