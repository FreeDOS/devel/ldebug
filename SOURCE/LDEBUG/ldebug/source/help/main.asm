%if 0

lDebug help message pages

Copyright (C) 1995-2003 Paul Vojta
Copyright (C) 2008-2023 E. C. Masloch

Usage of the works is permitted provided that this
instrument is retained with the works, so that any entity
that uses the works is notified of this instrument.

DISCLAIMER: THE WORKS ARE WITHOUT WARRANTY.

%endif

%ifndef MESSAGE_INLINE
 %include "debug.mac"
%endif

	db "assemble",9,	"A [address]",13,10
%if _ATTACH
	db "attach process",9,	"ATTACH psp",13,10
%endif
%if _BREAKPOINTS
	db "set breakpoint",9,	"BP index|AT|NEW address [[NUMBER=]number] [WHEN=cond] [ID=id]",13,10
	db " set ID",9,9,	"BI index|AT address [ID=]id",13,10
	db " set condition",9,	"BW index|AT address [WHEN=]cond",13,10
	db " set offset",9,	"BO index|AT address [OFFSET=]number",13,10
	db " set number",9,	"BN index|AT address|ALL number",13,10
	db " clear",9,9,	"BC index|AT address|ALL",13,10
	db " disable",9,	"BD index|AT address|ALL",13,10
	db " enable",9,9,	"BE index|AT address|ALL",13,10
	db " toggle",9,9,	"BT index|AT address|ALL",13,10
	db " swap",9,9,		"BS index1 index2",13,10
	db " list",9,9,		"BL [index|AT address|ALL]",13,10
%endif
%if _DEBUG
	db "break upwards",9,	"BU",13,10
%endif
	db "compare",9,9,	"C range address",13,10
	db "dump",9,9,		"D [range]",13,10
	db "dump bytes",9,	"DB [range]",13,10
	db "dump words",9,	"DW [range]",13,10
	db "dump dwords",9,	"DD [range]",13,10
%if _INT
	db "dump interrupts",9,	"DI[R][M][L] interrupt [count]",13,10
%endif
%if _PM
	db "dump LDT",9,	"DL selector [count]",13,10
%endif
%if _MCB
	db "dump MCB chain",9,	"DM [segment]",13,10
	;db "dump S/SD MCBs",9,	"DS",13,10
%endif
%if _DSTRINGS
	db "display strings",9,	"DZ/D$/D[W]# [address]",13,10
%endif
%if _PM
	db "dump ext memory",9,	"DX physical_address",13,10
	db "descriptor mod",9,	"D.A/D.D/D.B/D.L/D.T, D.? for help",13,10
%endif
%if _DT
	db "dump text table",9,	"DT [T] [number]",13,10
%endif
	db "enter",9,9,		"E [address [list]]",13,10
%if _EXTENSIONS
	db "run extension",9,	"EXT [partition/][extensionfile] [parameters]",13,10
%endif
	db "fill",9,9,		"F range [RANGE range|list]",13,10
	db "go",9,9,		"G [=address] [breakpts]",13,10
	db "goto",9,9,		"GOTO :label",13,10
	db "hex add/sub",9,	"H value1 [value2 [...]]",13,10
	db "base display",9,	"H BASE=number [GROUP=number] [WIDTH=number] value",13,10
	db "input",9,9,		"I[W|D] port",13,10
	db "if numeric",9,	"IF [NOT] (cond) THEN cmd",13,10
	db "if script file",9,	"IF [NOT] EXISTS Y file [:label] THEN cmd",13,10
	db "load program",9,	"L [address]",13,10
	db "load sectors",9,	"L address drive sector count",13,10
	db "move",9,9,		"M range address",13,10
	db "80x86/x87 mode",9,	"M [0..6|C|NC|C2|?]",13,10
	db "set name",9,	"N [[drive:][path]progname.ext [parameters]]",13,10
	db "set command",9,	"K [[drive:][path]progname.ext [parameters]]",13,10
	db "output",9,9,	"O[W|D] port value",13,10
	db "proceed",9,9,	"P [=address] [count [WHILE cond] [SILENT [count]]]",13,10
	db "quit",9,9,		"Q",13,10
	db "quit process",9,	"QA",13,10
	db "quit and break",9,	"QB",13,10
	db "register",9,	"R [register [value]]",13,10
	db "Run R extended",9,	"RE",13,10
	db "RE commands",9,	"RE.LIST|APPEND|REPLACE [commands]",13,10
	db "Run Commandline",9,	"RC",13,10
	db "RC commands",9,	"RC.LIST|APPEND|REPLACE [commands]",13,10
%if _MMXSUPP && _RM
	db "MMX register",9,	"RM [BYTES|WORDS|DWORDS|QWORDS]",13,10
%endif
%if _RN
	db "FPU register",9,	"RN",13,10
%endif
	db "toggle 386 regs",9,	"RX",13,10
	db "search",9,9,	"S range [REVERSE] [SILENT number] [RANGE range|list]",13,10
	db "sleep",9,9,		"SLEEP count [SECONDS|TICKS]",13,10
	db "trace",9,9,		"T [=address] [count [WHILE cond] [SILENT [count]]]",13,10
	db "trace (exc str)",9
	db			"TP [=address] [count [WHILE cond] [SILENT [count]]]",13,10
	db "trace mode",9,	"TM [0|1]",13,10
%if _TSR
	db "enter TSR mode",9,  "TSR",13,10
%endif
	db "unassemble",9,	"U [range]",13,10
%if _VXCHG
	db "view screen",9,	"V [ON|OFF [KEEP|NOKEEP]]",13,10
%endif
	db "write program",9,	"W [address]",13,10
	db "write sectors",9,	"W address drive sector count",13,10
%if _EMS
	db "expanded mem",9,	"XA/XD/XM/XR/XS, X? for help",13,10
%endif
	db "run script",9,	"Y [partition/][scriptfile] [:label]",13,10
	db 13,10
	db "Additional help topics:",13,10
%if _EXTHELP
	db " Registers",9,	"?R",13,10
	db " Flags",9,9,	"?F",13,10
 %if _COND
	db " Conditionals",9,	"?C",13,10
 %endif
 %if _EXPRESSIONS
	db " Expressions",9,	"?E",13,10
 %endif
 %if _VARIABLES || _OPTIONS || _PSPVARIABLES
	db " Variables",9,	"?V",13,10
 %endif
	db " R Extended",9,	"?RE",13,10
	db " Run keywords",9,	"?RUN",13,10
 %if _OPTIONS
	db " Options pages",9,	"?OPTIONS",13,10
	db " Options",9,	"?O",13,10
 %endif
 %if _BOOTLDR
	db " Boot loading",9,	"?BOOT",13,10
 %endif
%endif
	db " lDebug build",9,	"?BUILD",13,10
	db " lDebug build",9,	"?B",13,10
%if _EXTHELP
	db " lDebug sources",9,	"?SOURCE",13,10
	db " lDebug license",9,	"?L",13,10
%endif
%if _PM
	db 13,10
	db "Prompts: '-' = real or V86 mode; '#' = protected mode",13,10
%endif
