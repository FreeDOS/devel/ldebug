%if 0

lDebug help message pages

Copyright (C) 1995-2003 Paul Vojta
Copyright (C) 2008-2023 E. C. Masloch

Usage of the works is permitted provided that this
instrument is retained with the works, so that any entity
that uses the works is notified of this instrument.

DISCLAIMER: THE WORKS ARE WITHOUT WARRANTY.

%endif

%ifndef MESSAGE_INLINE
 %include "debug.mac"
%endif

	db "Recognized flags:",13,10
	db "Value",9,"Name",9,9,9,	"  Set",9,9,9,		"  Clear",13,10
	db "0800  OF  Overflow Flag",9,9,"OV  Overflow",9,9,	"NV  No overflow",13,10
	db "0400  DF  Direction Flag",9,"DN  Down",9,9,		"UP  Up",13,10
	db "0200  IF  Interrupt Flag",9,"EI  Enable interrupts",9,"DI  Disable interrupts",13,10
	db "0080  SF  Sign Flag",9,9,	"NG  Negative",9,9,	"PL  Plus",13,10
	db "0040  ZF  Zero Flag",9,9,	"ZR  Zero",9,9,		"NZ  Not zero",13,10
	db "0010  AF  Auxiliary Flag",9,"AC  Auxiliary carry",9,"NA  No auxiliary carry",13,10
	db "0004  PF  Parity Flag",9,9,	"PE  Parity even",9,9,	"PO  Parity odd",13,10
	db "0001  CF  Carry Flag",9,9,	"CY  Carry",9,9,	"NC  No carry",13,10
	db 13,10
	db "The short names of the flag states are displayed when dumping registers",13,10
	db "and can be entered to modify the symbolic F register with R. The short",13,10
	db "names of the flags can be modified by R.",13,10
