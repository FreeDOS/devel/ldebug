%if 0

lDebug help message pages

Copyright (C) 1995-2003 Paul Vojta
Copyright (C) 2008-2023 E. C. Masloch

Usage of the works is permitted provided that this
instrument is retained with the works, so that any entity
that uses the works is notified of this instrument.

DISCLAIMER: THE WORKS ARE WITHOUT WARRANTY.

%endif

%ifndef MESSAGE_INLINE
 %include "debug.mac"
%endif

	db "Expanded memory (EMS) commands:",13,10
	db "  Allocate",9,	"XA count",13,10
	db "  Deallocate",9,	"XD handle",13,10
	db "  Map memory",9,	"XM logical-page physical-page handle",13,10
	db "  Reallocate",9,	"XR handle count",13,10
	db "  Show status",9,	"XS",13,10
