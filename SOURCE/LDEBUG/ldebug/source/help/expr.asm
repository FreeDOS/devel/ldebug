%if 0

lDebug help message pages

Copyright (C) 1995-2003 Paul Vojta
Copyright (C) 2008-2023 E. C. Masloch

Usage of the works is permitted provided that this
instrument is retained with the works, so that any entity
that uses the works is notified of this instrument.

DISCLAIMER: THE WORKS ARE WITHOUT WARRANTY.

%endif

%ifndef MESSAGE_INLINE
 %include "debug.mac"
%endif

	db "Recognized operators in expressions:",13,10
	db "|",9,	"bitwise OR",9,9,		"||",9,	"boolean OR",13,10
	db "^",9,	"bitwise XOR",9,9,		"^^",9,	"boolean XOR",13,10
	db "&",9,	"bitwise AND",9,9,		"&&",9,	"boolean AND",13,10
	db ">>",9,	"bit-shift right",9,9,		">",9,"test if above",13,10
	db ">>>",9,	"signed bit-shift right",9,	"<",9,"test if below",13,10
	db "<<",9,	"bit-shift left",9,9,		">=",9,"test if above-or-equal",13,10
	db "><",9,	"bit-mirror",9,9,		"<=",9,"test if below-or-equal",13,10
	db "+",9,	"addition",9,9,			"==",9,"test if equal",13,10
	db "-",9,	"subtraction",9,9,		"!=",9,"test if not equal",13,10
	db "*",9,	"multiplication",9,9,		"=>",9,"same as >=",13,10
	db "/",9,	"division",9,9,			"=<",9,"same as <=",13,10
	db "%",9,	"modulo (A-(A/B*B))",9,		"<>",9,"same as !=",13,10
	db "**",9,	"power",13,10
	db 13,10
	db "Implicit operater precedence is handled in the listed order, with increasing",13,10
	db "precedence: (Brackets specify explicit precedence of an expression.)",13,10
	db " boolean operators OR, XOR, AND (each has a different precedence)",13,10
	db " comparison operators",13,10
	db " bitwise operators OR, XOR, AND (each has a different precedence)",13,10
	db " shift and bit-mirror operators",13,10
	db " addition and subtraction operators",13,10
	db " multiplication, division and modulo operators",13,10
	db " power operator",13,10
	db 13,10
	db "Recognized unary operators: (modifying the next number)",13,10
	db "+",9,	"positive (does nothing)",13,10
	db "-",9,	"negative",13,10
	db "~",9,	"bitwise NOT",13,10
	db "!",9,	"boolean NOT",13,10
	db "?",9,	"absolute value",13,10
	db "!!",9,	"convert to boolean",13,10
	db 13,10
	db "Note that the power operator does not affect unary operator handling.",13,10
	db 'For instance, "- 2 ** 2" is parsed as "(-2) ** 2" and evaluates to 4.',13,10
	db 13,10
	db "Although a negative unary and signed bit-shift right operator are provided",13,10
	db "the expression evaluator is intrinsically unsigned. Particularly the division,",13,10
	db "multiplication, modulo and all comparison operators operate unsigned. Due to",13,10
	db 'this, the expression "-1 < 0" evaluates to zero.',13,10
	db 13,10
	db "Recognized terms in an expression:",13,10
	db " 32-bit immediates",13,10
	db " 8-bit registers",13,10
	db " 16-bit registers including segment registers (except FS, GS)",13,10
	db " 32-bit compound registers made of two 16-bit registers (eg DXAX)",13,10
	db " 32-bit registers and FS, GS only if running on a 386+",13,10
  %if _MMXSUPP
	db " 64-bit MMX registers only if running on a CPU with MMX",13,10
	db "  MM0L accesses the low 32 bits of the register",13,10
	db "  MM0H accesses the high 32 bits of the register",13,10
	db "  MM0Z reads the low 32 bits; writes the full register (zero-extend)",13,10
	db "  MM0S reads the low 32 bits; writes the full register (sign-extend)",13,10
	db "  MM0 is an alias for the MM0Z syntax",13,10
  %endif
  %if _VARIABLES
	db " 32-bit variables V00..VFF",13,10
  %endif
  %if _OPTIONS || _PSPVARIABLES
	db " 32-bit special variable"
   %if _OPTIONS
	db "s DCO, DCS, DAO, DAS, DIF, DPI"
    %if _PSPVARIABLES
	db ","
    %endif
   %endif
   %if _PSPVARIABLES
	db " PPI"
   %endif
	db 13,10
	db " 16-bit special variables"
   %if _OPTIONS
	db " DPR, DPP"
    %if _PM
	db ", DPS"
    %endif
    %if _PSPVARIABLES
	db ","
    %endif
   %endif
   %if _PSPVARIABLES
	db " PSP, PPR"
   %endif
	db 13,10
	db "  (fuller variable reference in the manual)",13,10
  %endif
  %if _INDIRECTION
	db " byte/word/3byte/dword memory content (eg byte [seg:ofs], where both the",13,10
	db "  optional segment as well as the offset are expressions too)",13,10
  %endif
	db "The expression evaluator case-insensitively checks for names of variables",13,10
	db "and registers"
  %if _INDIRECTION
	db		" as well as size specifiers"
  %endif
	db					   '.',13,10
	db 13,10
	db "Enter ?R to display the recognized register names.",13,10
  %if _VARIABLES || _OPTIONS || _PSPVARIABLES
	db "Enter ?V to display the recognized variables.",13,10
  %endif
