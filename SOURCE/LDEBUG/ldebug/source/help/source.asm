%if 0

lDebug help message pages

Copyright (C) 1995-2003 Paul Vojta
Copyright (C) 2008-2023 E. C. Masloch

Usage of the works is permitted provided that this
instrument is retained with the works, so that any entity
that uses the works is notified of this instrument.

DISCLAIMER: THE WORKS ARE WITHOUT WARRANTY.

%endif

%ifndef MESSAGE_INLINE
 %include "debug.mac"
%endif

	db "The original lDebug sources can be obtained from the repo located at",13,10
	db "https://hg.pushbx.org/ecm/ldebug (E. C. Masloch's repo)",13,10
	db 13,10
	db "Releases of lDebug are available via the website at",13,10
	db "https://pushbx.org/ecm/web/#projects-ldebug",13,10
	db 13,10
	db "The most recent manual is hosted at https://pushbx.org/ecm/doc/ in the",13,10
	db "files ldebug.htm, ldebug.txt, and ldebug.pdf",13,10
