%if 0

lDebug help message pages

Copyright (C) 1995-2003 Paul Vojta
Copyright (C) 2008-2023 E. C. Masloch

Usage of the works is permitted provided that this
instrument is retained with the works, so that any entity
that uses the works is notified of this instrument.

DISCLAIMER: THE WORKS ARE WITHOUT WARRANTY.

%endif

%ifndef MESSAGE_INLINE
 %include "debug.mac"
%endif

	db "Available "
  %if _PSPVARIABLES && !(_VARIABLES || _OPTIONS)
	db "read-only "
  %endif
	db "lDebug variables:",13,10
  %if _VARIABLES
	db "V0..VF",9,"User-specified usage",13,10
  %endif
  %if _OPTIONS
	db "DCO",9,"Debugger Common Options",13,10
	db "DAO",9,"Debugger Assembler/disassembler Options",13,10
  %endif
  %if _OPTIONS || _PSPVARIABLES && (_OPTIONS || _VARIABLES)
	db " The following variables cannot be written:",13,10
  %endif
  %if _PSPVARIABLES
	db "PSP",9,"Debuggee Process"
  %if _PM
	db " (as real mode segment)"
  %endif
	db 13,10
	db "PPR",9,"Debuggee's Parent Process",13,10
	db "PPI",9,"Debuggee's Parent Process Interrupt 22h",13,10
  %endif
  %if _OPTIONS
	db "DIF",9,"Debugger Internal Flags",13,10
	db "DCS",9,"Debugger Common Startup options",13,10
	db "DAS",9,"Debugger Assembler/disassembler Startup options",13,10
	db "DPR",9,"Debugger Process"
   %if _PM
	db " (as Real mode segment)",13,10
	db "DPS",9,"Debugger Process Selector (zero in real mode)"
   %endif
	db 13,10
	db "DPP",9,"Debugger's Parent Process"
  %if _TSR
	db " (zero in TSR mode)"
  %endif
	db 13,10
	db "DPI",9,"Debugger's Parent process Interrupt 22h"
  %if _TSR
	db " (zero in TSR mode)"
  %endif
	db 13,10
	db 13,10
	db "Enter ?O to display the options and internal flags.",13,10
  %endif
