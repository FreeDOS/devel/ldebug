%if 0

lDebug help message pages

Copyright (C) 1995-2003 Paul Vojta
Copyright (C) 2008-2023 E. C. Masloch

Usage of the works is permitted provided that this
instrument is retained with the works, so that any entity
that uses the works is notified of this instrument.

DISCLAIMER: THE WORKS ARE WITHOUT WARRANTY.

%endif

%ifndef MESSAGE_INLINE
 %include "debug.mac"
%endif

	db "Descriptor modification commands:",13,10
	db " (only valid in Protected Mode)",13,10
	db "  Allocate",9,	"D.A",13,10
	db "  Deallocate",9,	"D.D selector",13,10
	db "  Set base",9,	"D.B selector base",13,10
	db "  Set limit",9,	"D.L selector limit",13,10
	db "  Set type",9,	"D.T selector type",13,10
