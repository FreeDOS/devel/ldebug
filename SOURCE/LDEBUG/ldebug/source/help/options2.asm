%if 0

lDebug help message pages

Copyright (C) 1995-2003 Paul Vojta
Copyright (C) 2008-2023 E. C. Masloch

Usage of the works is permitted provided that this
instrument is retained with the works, so that any entity
that uses the works is notified of this instrument.

DISCLAIMER: THE WORKS ARE WITHOUT WARRANTY.

%endif

%ifndef MESSAGE_INLINE
 %include "debug.mac"
 %include "options.mac"
%endif

	db "More options: (read/write DCO2, read DCS2)",13,10
	db _4digitshex(opt2_db_header),	" DB: show header",13,10
	db _4digitshex(opt2_db_trailer)," DB: show trailer",13,10
	db _4digitshex(opt2_dw_header),	" DW: show header",13,10
	db _4digitshex(opt2_dw_trailer)," DW: show trailer",13,10
	db _4digitshex(opt2_dd_header),	" DD: show header",13,10
	db _4digitshex(opt2_dd_trailer)," DD: show trailer",13,10
	db _4digitshex(opt2_getinput_dpmi)," use getinput function for int 21h interactive input in DPMI",13,10
	db _4digitshex(opt2_hh_compat),	" H: stay compatible to MS-DOS Debug",13,10
	db _4digitshex(opt2_getc_idle),	" idle and check for Ctrl-C in getc",13,10
	db _4digitshex(opt2_getc_idle_dpmi)," idle and check for Ctrl-C in getc in DPMI",13,10
	db _4digitshex(opt2_re_cancel_tpg)," T/TP/P/G: cancel run after RE command buffer execution",13,10
%if _MS_N_COMPAT
	db _6digitssephex(opt2_nn_compat)," N: operate in MS Debug style instead of K command alike",13,10
%endif
	db _6digitssephex(opt2_nn_capitalise)," N: capitalise command line tail",13,10
%if _MS_0RANGE_COMPAT
	db _6digitssephex(opt2_0range_compat)," explicit 0-length ranges operate in partial MS Debug style",13,10
%endif
	db _6digitssephex(opt2_rr16_compat)," R: 16-bit 80-column register dump in MS Debug style",13,10
%if _MS_PROMPT_COMPAT
	db _6digitssephex(opt2_r_prompt_compat)," R: do variable prompts in MS Debug style",13,10
%endif
%if _RSEPARATE
	db _6digitssephex(opt2_r_separate)," R: do variable prompts with underscore separator",13,10
%endif
%if _REGSLINEBREAK
	db _6digitssephex(opt2_r_linebreak_always)," display linebreak before R command register dump",13,10
%endif
