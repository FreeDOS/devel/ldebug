%if 0

lDebug help message pages

Copyright (C) 1995-2003 Paul Vojta
Copyright (C) 2008-2023 E. C. Masloch

Usage of the works is permitted provided that this
instrument is retained with the works, so that any entity
that uses the works is notified of this instrument.

DISCLAIMER: THE WORKS ARE WITHOUT WARRANTY.

%endif

%ifndef MESSAGE_INLINE
 %include "debug.mac"
 %include "options.mac"
%endif

	db "Available options: (read/write DCO, read DCS)",13,10
	db _4digitshex(dispregs32),	" RX: 32-bit register display",13,10
	db _4digitshex(traceints),	" TM: trace into interrupts",13,10
	db _4digitshex(cpdepchars),	" allow dumping of CP-dependent characters",13,10
	db _4digitshex(fakeindos),	" always assume InDOS flag non-zero, to debug DOS or TSRs",13,10
	db _4digitshex(nonpagingdevice)," disallow paged output to StdOut",13,10
	; db _4digitshex(pagingdevice),	" allow paged output to non-StdOut",13,10
	db _4digitshex(hexrn),		" display raw hexadecimal content of FPU registers",13,10
	db _4digitshex(nondospaging),	" when prompting during paging, do not use DOS for input",13,10
	db _4digitshex(nohlt),		" do not execute HLT instruction to idle",13,10
	db _4digitshex(biosidles),	" do not idle, the keyboard BIOS idles itself",13,10
	db _4digitshex(opt_usegetinput)," use getinput function for int 21h interactive input",13,10
	db _4digitshex(use_si_units),	" in disp_*_size use SI units (kB = 1000, etc)."
					db " overrides ",_4digitshex(use_jedec_units),"!",13,10
	db _4digitshex(use_jedec_units)," in disp_*_size use JEDEC units (KB = 1024)",13,10
	db _4digitshex(enable_serial),	" enable serial I/O (port ",_4digitshex(_UART_BASE),"h interrupt ",_2digitshex(_INTNUM),"h)",13,10
	db _4digitshex(int8_disable_serial),	" disable serial I/O when breaking after Ctrl pressed for a while",13,10
	db _8digitssephex(gg_do_not_skip_bp),	" gg: do not skip a breakpoint (bb or gg)",13,10
	db _8digitssephex(gg_no_autorepeat), 	" gg: do not auto-repeat",13,10
	db _8digitssephex(tp_do_not_skip_bp),	" T/TP/P: do not skip a (bb) breakpoint",13,10
	db _8digitssephex(gg_bb_hit_no_repeat),	" gg: do not auto-repeat after bb hit",13,10
	db _8digitssephex(tp_bb_hit_no_repeat),	" T/TP/P: do not auto-repeat after bb hit",13,10
	db _8digitssephex(gg_unexpected_no_repeat)," gg: do not auto-repeat after unexpectedinterrupt",13,10
	db _8digitssephex(tp_unexpected_no_repeat)," T/TP/P: do not auto-repeat after unexpectedinterrupt",13,10
	db _8digitssephex(ss_no_dump),		" S: do not dump data after matches",13,10
	db _8digitssephex(rr_disasm_no_rept),	" R: do not repeat disassembly",13,10
	db _8digitssephex(rr_disasm_no_show),	" R: do not show memory reference in disassembly",13,10
	db _8digitssephex(opt_cmdline_quiet_input)," quiet command line buffer input",13,10
	db _8digitssephex(opt_cmdline_quiet_output)," quiet command line buffer output",13,10
