%if 0

lDebug help message pages

Copyright (C) 1995-2003 Paul Vojta
Copyright (C) 2008-2023 E. C. Masloch

Usage of the works is permitted provided that this
instrument is retained with the works, so that any entity
that uses the works is notified of this instrument.

DISCLAIMER: THE WORKS ARE WITHOUT WARRANTY.

%endif

%ifndef MESSAGE_INLINE
 %include "debug.mac"
%endif

	db "Boot loading commands:",13,10
	db "BOOT LIST HDA",13,10
	db "BOOT DIR [partition] [dirname]",13,10
	db "BOOT READ|WRITE [partition] segment [[HIDDEN=sector] sector] [count]",13,10
  %if _DOSEMU
	db "BOOT QUIT",9,"[exits dosemu or shuts down using APM]",13,10
  %else
	db "BOOT QUIT",9,"[shuts down using APM]",13,10
  %endif
	db "BOOT [PROTOCOL=SECTOR] partition",13,10
	db "BOOT PROTOCOL=proto [opt] [partition] [filename1] [filename2] [cmdline]",13,10
	db 9,"the following partitions may be specified:",13,10
	db 9," HDAnum",9,"first hard disk, num = partition (1-4 primary, 5+ logical)",13,10
	db 9," HDBnum",9,"second hard disk (etc), num = partition",13,10
	db 9," HDA",9,"first hard disk (only valid for READ|WRITE|PROTOCOL=SECTOR)",13,10
	db 9," FDA",9,"first floppy disk",13,10
	db 9," FDB",9,"second floppy disk (etc)",13,10
	db 9," LDP",9,"partition the debugger loaded from",13,10
	db 9," YDP",9,"partition the most recent Y command loaded from",13,10
	db 9," SDP",9,"last used partition (default if no partition specified)",13,10
	db 9,"filename2 may be double-slash // for none",13,10
	db 9,"cmdline is only valid for lDOS, RxDOS.2, RxDOS.3 protocols",13,10
	db 9,"files' directory entries are loaded to 500h and 520h",13,10
	db 13,10
	db "Available protocols: (default filenames, load segment, then entrypoint)",13,10
	db " LDOS",9,9,	"LDOS.COM or L[D]DEBUG.COM at 200h, 0:400h",13,10
	db " FREEDOS",9,"KERNEL.SYS or METAKERN.SYS at 60h, 0:0",13,10
	db " DOSC",9,9,	"IPL.SYS at 2000h, 0:0",13,10
	db " EDRDOS",9,9,"DRBIO.SYS at 70h, 0:0",13,10
	db " MSDOS6",9,9,	"IO.SYS + MSDOS.SYS at 70h, 0:0",13,10
	db " MSDOS7",9,9,	"IO.SYS at 70h, 0:200h",13,10
	db " IBMDOS",9,9,	"IBMBIO.COM + IBMDOS.COM at 70h, 0:0",13,10
	db " DRDOS",9,9,	"IBMBIO.COM + IBMDOS.COM at 70h, 0:0",13,10
	db " NTLDR",9,9,	"NTLDR at 2000h, 0:0",13,10
	db " BOOTMGR",9,	"BOOTMGR at 2000h, 0:0",13,10
	db " RXDOS.0",9,"RXDOSBIO.SYS + RXDOS.SYS at 70h, 0:0",13,10
	db " RXDOS.1",9,"RXBIO.SYS + RXDOS.SYS at 70h, 0:0",13,10
	db " RXDOS.2",9,"RXDOS.COM at 70h, 0:400h",13,10
	db " RXDOS.3",9,"RXDOS.COM at 200h, 0:400h",13,10
	db " CHAIN",9,9,"BOOTSECT.DOS at 7C0h, -7C0h:7C00h",13,10
	db " SECTOR",9,9,"(default) load partition boot sector or MBR",13,10
	db " SECTORALT",9,"as SECTOR, but entry at 07C0h:0",13,10
	db 13,10
	db "Available options:",13,10
	db " MINPARA=num",9,9,	"load at least that many paragraphs",13,10
	db " MAXPARA=num",9,9,	"load at most that many paragraphs (0 = as many as fit)",13,10
	db " SEGMENT=num",9,9,	"change segment at that the kernel loads",13,10
	db " ENTRY=[num:]num",9,"change entrypoint (CS (relative) : IP)",13,10
	db " BPB=[num:]num",9,9, \
		"change BPB load address (segment -1 = auto-BPB)",13,10
	db " CHECKOFFSET=num",9,"set address of word to check, must be even",13,10
	db " CHECKVALUE=num",9,9,"set value of word to check (0 = no check)",13,10
	db "Boolean options: [opt=bool]",13,10
	db " SET_DL_UNIT",9,9,"set dl to load unit",13,10
	db " SET_BL_UNIT",9,9,"set bl to load unit",13,10
	db " SET_SIDI_CLUSTER",9,"set si:di to first cluster",13,10
	db " SET_DSSI_DPT",9,9,"set ds:si to DPT address",13,10
	db " PUSH_DPT",9,9,"push DPT address and DPT entry address",13,10
	db " DATASTART_HIDDEN",9,"add hidden sectors to datastart var",13,10
	db " SET_AXBX_DATASTART",9,"set ax:bx to datastart var",13,10
	db " SET_DSBP_BPB",9,9,"set ds:bp to BPB address",13,10
	db " LBA_SET_TYPE",9,9,"set LBA partition type in BPB",13,10
	db " MESSAGE_TABLE",9,9, \
		"provide message table pointed to at 1EEh",13,10
	db " SET_AXBX_ROOT_HIDDEN",9, \
		"set ax:bx to root start with hidden sectors",13,10
	db " NO_BPB",9,9,9, "do not load BPB",13,10
	db " SET_DSSI_PARTINFO",9, "load part table to 600h, point ds:si + ds:bp to it",13,10
	db " CMDLINE",9,9,  "pass a kernel command line (recent FreeDOS extension)",13,10
