%if 0

lDebug help message pages

Copyright (C) 1995-2003 Paul Vojta
Copyright (C) 2008-2023 E. C. Masloch

Usage of the works is permitted provided that this
instrument is retained with the works, so that any entity
that uses the works is notified of this instrument.

DISCLAIMER: THE WORKS ARE WITHOUT WARRANTY.

%endif

%ifndef MESSAGE_INLINE
 %include "debug.mac"
%endif

	db "Enter one of the following commands to get a corresponding help page:",13,10
	db 13,10
	db "?O1",9,"DCO1 - Options",13,10
	db "?O2",9,"DCO2 - More Options",13,10
	db "?O3",9,"DCO3 - More Options",13,10
	db "?O4",9,"DCO4 - Interrupt Hooking Options",13,10
	db "?O6",9,"DCO6 - More Options",13,10
	db "?OI",9,"DIF - Internal Flags",13,10
	db "?OA",9,"DAO - Assembler/Disassembler Options",13,10
