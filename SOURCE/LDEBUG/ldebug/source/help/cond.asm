%if 0

lDebug help message pages

Copyright (C) 1995-2003 Paul Vojta
Copyright (C) 2008-2023 E. C. Masloch

Usage of the works is permitted provided that this
instrument is retained with the works, so that any entity
that uses the works is notified of this instrument.

DISCLAIMER: THE WORKS ARE WITHOUT WARRANTY.

%endif

%ifndef MESSAGE_INLINE
 %include "debug.mac"
%endif

	db "In the register dump displayed by the R, T, P and G commands, conditional",13,10
	db "jumps are displayed with a notice that shows whether the instruction will",13,10
	db "cause a jump depending on its condition and the current register and flag",13,10
	db 'contents. This notice shows either "jumping" or "not jumping" as appropriate.',13,10
	db 13,10
	db "The conditional jumps use these conditions: (second column negates)",13,10
	db " jo",9,9,"jno",9,9,"OF",13,10
	db " jc jb jnae",9,"jnc jnb jae",9,"CF",13,10
	db " jz je",9,9,"jnz jne",9,9,"ZF",13,10
	db " jbe jna",9,"jnbe ja",9,9,"ZF||CF",13,10
	db " js",9,9,"jns",9,9,"SF",13,10
	db " jp jpe",9,9,"jnp jpo",9,9,"PF",13,10
	db " jl jnge",9,"jnl jge",9,9,"OF^^SF",13,10
	db " jle jng",9,"jnle jg",9,9,"OF^^SF || ZF",13,10
	db " j(e)cxz",9,9,9,"(e)cx==0",13,10
	db " loop",9,9,9,9,"(e)cx!=1",13,10
	db " loopz loope",9,9,9,"(e)cx!=1 && ZF",13,10
	db " loopnz loopne",9,9,9,"(e)cx!=1 && !ZF",13,10
	db 13,10
	db "Enter ?F to display a description of the flag names.",13,10
