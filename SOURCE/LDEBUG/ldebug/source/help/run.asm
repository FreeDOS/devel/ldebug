%if 0

lDebug help message pages

Copyright (C) 1995-2003 Paul Vojta
Copyright (C) 2008-2023 E. C. Masloch

Usage of the works is permitted provided that this
instrument is retained with the works, so that any entity
that uses the works is notified of this instrument.

DISCLAIMER: THE WORKS ARE WITHOUT WARRANTY.

%endif

%ifndef MESSAGE_INLINE
 %include "debug.mac"
%endif

	db "T (trace), TP (trace except proceed past string operations), and P (proceed)",13,10
	db "can be followed by a number of repetitions and then the keyword WHILE,",13,10
	db "which must be followed by a conditional expression.",13,10
	db 13,10
	db "The selected run command is repeated as many times as specified by the",13,10
	db "number, or until the WHILE condition evaluates no longer to true.",13,10
	db 13,10
	db "After the number of repetitions or (if present) after the WHILE condition",13,10
	db "the keyword SILENT may follow. If that is the case, all register dumps",13,10
	db "done during the run are buffered by the debugger and the run remains",13,10
	db "silent. After the run, the last dumps are replayed from the buffer",13,10
	db "and displayed. At most as many dumps as fit into the buffer are",13,10
	db "displayed. (The buffer is currently 8 KiB sized by default, though the",13,10
	db "/A switch can be specified to init to grow it up to 24 KiB.)",13,10
	db 13,10
	db "If a number follows behind the SILENT keyword, only at most that many",13,10
	db "dumps are displayed from the buffer. The dumps that are displayed",13,10
	db "are always those last written into the buffer, thus last occurred.",13,10
