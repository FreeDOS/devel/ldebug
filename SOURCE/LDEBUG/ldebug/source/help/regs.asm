%if 0

lDebug help message pages

Copyright (C) 1995-2003 Paul Vojta
Copyright (C) 2008-2023 E. C. Masloch

Usage of the works is permitted provided that this
instrument is retained with the works, so that any entity
that uses the works is notified of this instrument.

DISCLAIMER: THE WORKS ARE WITHOUT WARRANTY.

%endif

%ifndef MESSAGE_INLINE
 %include "debug.mac"
%endif

	db "Available 16-bit registers:",9,9,"Available 32-bit registers: (386+)",13,10
	db "AX",9,"Accumulator",9,9,9,"EAX",13,10
	db "BX",9,"Base register",9,9,9,"EBX",13,10
	db "CX",9,"Counter",9,9,9,9,"ECX",13,10
	db "DX",9,"Data register",9,9,9,"EDX",13,10
	db "SP",9,"Stack pointer",9,9,9,"ESP",13,10
	db "BP",9,"Base pointer",9,9,9,"EBP",13,10
	db "SI",9,"Source index",9,9,9,"ESI",13,10
	db "DI",9,"Destination index",9,9,"EDI",13,10
	db "DS",9,"Data segment",13,10
	db "ES",9,"Extra segment",13,10
	db "SS",9,"Stack segment",13,10
	db "CS",9,"Code segment",13,10
	db "FS",9,"Extra segment 2 (386+)",13,10
	db "GS",9,"Extra segment 3 (386+)",13,10
	db "IP",9,"Instruction pointer",9,9,"EIP",13,10
	db "FL",9,"Flags",9,9,9,9,"EFL",13,10
	db 13,10
 %if _MMXSUPP && 0
	db "Available 64-bit Matrix Math Extension (MMX) registers: (if supported)",13,10
	db "MMx",9,"MM(x)",9,"MMX register x, where x is 0 to 7",13,10
	db 13,10
 %endif
	db "Enter ?F to display the recognized flags.",13,10
