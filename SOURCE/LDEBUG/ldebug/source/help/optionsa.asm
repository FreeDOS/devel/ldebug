%if 0

lDebug help message pages

Copyright (C) 1995-2003 Paul Vojta
Copyright (C) 2008-2023 E. C. Masloch

Usage of the works is permitted provided that this
instrument is retained with the works, so that any entity
that uses the works is notified of this instrument.

DISCLAIMER: THE WORKS ARE WITHOUT WARRANTY.

%endif

%ifndef MESSAGE_INLINE
 %include "debug.mac"
 %include "options.mac"
%endif

	db "Available assembler/disassembler options: (read/write DAO, read DAS)",13,10
	db _2digitshex(disasm_lowercase), " Disassembler: lowercase output",13,10
	db _2digitshex(disasm_commablank)," Disassembler: output blank behind comma",13,10
	db _2digitshex(disasm_nasm),      " Disassembler: output addresses in NASM syntax",13,10
	db _2digitshex(disasm_lowercase_refmem)
	db				  " Disassembler: lowercase referenced memory location segreg",13,10
	db _2digitshex(disasm_show_short)," Disassembler: always show SHORT keyword",13,10
	db _2digitshex(disasm_show_near), " Disassembler: always show NEAR keyword",13,10
	db _2digitshex(disasm_show_far),  " Disassembler: always show FAR keyword",13,10
	db _2digitshex(disasm_nec),       " Disassembler: NEC V20 repeat rules (for segregs)",13,10
%if _40COLUMNS
	db _4digitshex(disasm_40_columns)," Disassembler: 40-column friendly mode (only 4 bytes machine code per line)",13,10
	db _4digitshex(disasm_no_indent), " Disassembler: do not indent disassembly operands",13,10
 %if _MS_MNEMON_COMPAT
	db _4digitshex(disasm_msdebug_mnemonofs), " Disassembler: MS Debug style opcode field width",13,10
 %endif
%endif
	db _4digitshex(disasm_a16_memref)," Disassembler: access data in a16 referenced memory operand",13,10
	db _4digitshex(disasm_a32_memref)," Disassembler: access data in a32 referenced memory operand",13,10
	db _4digitshex(disasm_a16_string)," Disassembler: simulate repeated a16 scas/cmps string operation",13,10
	db _4digitshex(disasm_a32_string)," Disassembler: simulate repeated a32 scas/cmps string operation",13,10
	db _6digitssephex(disasm_hide_modrm)," Disassembler: hide needed MODRM keywords",13,10
	db _6digitssephex(disasm_asize_loop_nasm)," Disassembler: use LOOP rel, (E)CX rather than LOOPW/LOOPD",13,10
	db _6digitssephex(disasm_modrm_always)," Disassembler: always display MODRM keyword even if not needed",13,10
