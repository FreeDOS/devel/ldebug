%if 0

lDebug help message pages

Copyright (C) 1995-2003 Paul Vojta
Copyright (C) 2008-2023 E. C. Masloch

Usage of the works is permitted provided that this
instrument is retained with the works, so that any entity
that uses the works is notified of this instrument.

DISCLAIMER: THE WORKS ARE WITHOUT WARRANTY.

%endif

%ifndef MESSAGE_INLINE
 %include "debug.mac"
 %include "options.mac"
%endif

	db "Internal flags: (read DIF)",13,10
	db _6digitssephex(oldpacket),	" Int25/Int26 packet method available",13,10
	db _6digitssephex(newpacket),	" Int21.7305 packet method available",13,10
  %if _VDD
	db _6digitssephex(ntpacket),	" VDD registered and usable",13,10
  %endif
	db _6digitssephex(pagedcommand),	" internal flag for paged output",13,10
	db _6digitssephex(notstdinput),	" DEBUG's input isn't StdIn",13,10
	db _6digitssephex(inputfile),	" DEBUG's input is a file",13,10
	db _6digitssephex(notstdoutput),	" DEBUG's output isn't StdOut",13,10
	db _6digitssephex(outputfile),	" DEBUG's output is a file",13,10
  %if _PM
	db _6digitssephex(hooked2F),	" Int2F.1687 hooked",13,10
	db _6digitssephex(nohook2F),	" Int2F.1687 won't be hooked",13,10
	db _6digitssephex(dpminohlt),	" do not execute HLT to idle in PM",13,10
	db _6digitssephex(protectedmode),	" in protected mode",13,10
  %endif
	db _6digitssephex(debuggeeA20),	" state of debuggee's A20",13,10
	db _6digitssephex(debuggerA20),	" state of debugger's A20 (not implemented: same as previous)",13,10
  %if _BOOTLDR
	db _6digitssephex(nodosloaded),	" debugger booted independent of a DOS",13,10
  %endif
	db _6digitssephex(has386),		" CPU is at least a 386 (32-bit CPU)",13,10
	db _6digitssephex(usecharcounter),	" internal flag for tab output processing",13,10
 %if _VDD
	db _6digitssephex(runningnt),	" running inside NTVDM",13,10
 %endif
 %if _PM
	db _6digitssephex(canswitchmode),	" DPMI raw mode switch usable to set breakpoints",13,10
	db _6digitssephex(modeswitched),	" internal flag for mode switching",13,10
 %endif
	db _6digitssephex(promptwaiting),	" internal flag for paged output",13,10
 %if _PM
	db _6digitssephex(switchbuffer),	" internal flag for mode switching",13,10
 %endif
 %if _TSR
	db _6digitssephex(tsrmode),	" in TSR mode (detached debugger process)",13,10
 %endif
 %if _DOSEMU
	db _8digitssephex(runningdosemu),	" running inside dosemu",13,10
 %endif
	db _8digitssephex(tt_while)
	db " T/TP/P: while condition specified",13,10
	db _8digitssephex(tt_p)
	db " TP: P specified (proceed past string ops)",13,10
	db _8digitssephex(tt_silent_mode)
	db " T/TP/P: silent mode (SILENT specified)",13,10
	db _8digitssephex(tt_silence)
	db " T/TP/P: silent mode is active, writing to silent buffer",13,10
