%if 0

lDebug help message pages

Copyright (C) 1995-2003 Paul Vojta
Copyright (C) 2008-2024 E. C. Masloch

Usage of the works is permitted provided that this
instrument is retained with the works, so that any entity
that uses the works is notified of this instrument.

DISCLAIMER: THE WORKS ARE WITHOUT WARRANTY.

%endif

%ifndef MESSAGE_INLINE
 %include "debug.mac"
%endif

	db "lDebug - libre 86-DOS debugger",13,10
	db 13,10
	db "Copyright (C) 1995-2003 Paul Vojta",13,10
	db "Copyright (C) 2008-2024 E. C. Masloch",13,10
	db 13,10
	db "Usage of the works is permitted provided that this",13,10
	db "instrument is retained with the works, so that any entity",13,10
	db "that uses the works is notified of this instrument.",13,10
	db 13,10
	db "DISCLAIMER: THE WORKS ARE WITHOUT WARRANTY.",13,10
	db 13,10
	db 13,10
	db "All contributions by Paul Vojta or E. C. Masloch to the debugger are available",13,10
	db "under a choice of three different licenses. These are the Fair License, the",13,10
	db "Simplified 2-Clause BSD License, or the MIT License.",13,10
	db 13,10
	db "This is the license and copyright information that applies to lDebug; but note",13,10
	db "that there have been substantial contributions to the code base that are not",13,10
	db "copyrighted (public domain).",13,10
