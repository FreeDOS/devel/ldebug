%if 0

lDebug help message pages

Copyright (C) 1995-2003 Paul Vojta
Copyright (C) 2008-2023 E. C. Masloch

Usage of the works is permitted provided that this
instrument is retained with the works, so that any entity
that uses the works is notified of this instrument.

DISCLAIMER: THE WORKS ARE WITHOUT WARRANTY.

%endif

%ifndef MESSAGE_INLINE
 %include "debug.mac"
%endif

	db 13,10
 %if _PM
	db "DPMI-capable",13,10
  %if _NOEXTENDER
	db " DPMI host without extender",13,10
  %endif
  %if 0
   %if _WIN9XSUPP
	db " No Windows 4 DPMI hook",13,10
   %endif
   %if _PM && _DOSEMU
	db " No DOSEMU DPMI hook",13,10
   %endif
  %else
	db " Automatic DPMI entrypoint hook detection",13,10
  %endif
  %if _EXCCSIP
	db " Display exception address",13,10
  %endif
  %if _DISPHOOK
	db " Display hooking DPMI entry",13,10
  %endif
 %endif
 %if _DEBUG
	db "Debuggable",13,10
  %if _DEBUG_COND
	db "Conditionally Debuggable",13,10
  %endif
 %endif
 %if _INT
	db "DI command",13,10
 %endif
 %if _MCB
	db "DM command",13,10
 %endif
 %if _DSTRINGS
	db "D string commands",13,10
 %endif
 %if _SDUMP
	db "S match dumps line of following data",13,10
 %endif
 %if _RN
	db "RN command",13,10
 %endif
 %if _USESDA
	db "Access SDA current PSP field",13,10
 %endif
 %if _VDD
	db "Load NTVDM VDD for sector access",13,10
 %endif
 %if _EMS
	db "X commands for EMS access",13,10
 %endif
 %if _MMXSUPP
	db "RM command and reading MMX registers as variables",13,10
 %endif
 %if _EXPRESSIONS
	db "Expression evaluator",13,10
 %endif
 %if _INDIRECTION
	db " Indirection in expressions",13,10
 %endif
 %if _VARIABLES
	db "Variables with user-defined purpose",13,10
 %endif
 %if _OPTIONS
	db "Debugger option and status variables",13,10
 %endif
 %if _PSPVARIABLES
	db "PSP variables",13,10
 %endif
 %if _COND
	db "Conditional jump notice in register dump",13,10
 %endif
 %if _TSR
	db "TSR mode (Process detachment)",13,10
 %endif
 %if _DEVICE
	db "Loadable device driver",13,10
 %endif
 %if _BOOTLDR
	db "Boot loader",13,10
 %endif
 %if _BREAKPOINTS
	db "Permanent breakpoints",13,10
 %endif
%push
	db "Intercepted"
%if _PM
	db " 86M"
%endif
	db " interrupts:"
 %define %$pref " "
%macro dispint 2.nolist
 %if %1
	db %$pref, %2
  %define %$pref ", "
 %endif
%endmacro
	dispint _CATCHINT00, "00"
	dispint _CATCHINT01, "01"
	dispint _CATCHINT03, "03"
	dispint _CATCHINT06, "06"
	dispint _CATCHINT07, "07"
	dispint _CATCHINT0C, "0C"
	dispint _CATCHINT0D, "0D"
	dispint _CATCHINT18, "18"
	dispint _CATCHINT19, "19"
 %ifidn %$pref," "
	db " none"
 %endif
	db 13,10
 %if _PM || _CATCHINT08
	db "Processed"
  %if _PM
	db " 86M"
  %endif
	db " interrupts:"
  %define %$pref " "
	dispint _CATCHINT08, "08"
	dispint _PM, "2F.1687"
  %ifidn %$pref," "
	db " none"
  %endif
	db 13,10
 %endif
 %if _PM
	db "Intercepted DPMI exceptions:"
  %define %$pref " "
	dispint _CATCHEXC00, "00"
	dispint _CATCHEXC01, "01"
	dispint _CATCHEXC03, "03"
	dispint _CATCHEXC06, "06"
	dispint _CATCHEXC0C, "0C"
	dispint _CATCHEXC0D, "0D"
	dispint _CATCHEXC0E, "0E"
  %ifidn %$pref," "
	db " none"
  %endif
	db 13,10
 %endif
 %if _PM && _CATCHPMINT41
	db "Intercepted DPMI interrupts:"
  %define %$pref " "
	dispint _CATCHPMINT41, "41.004F"
  %ifidn %$pref," "
	db " none"
  %endif
	db 13,10
 %endif
 %if _PM && _CATCHPMINT214C
	db "Processed DPMI interrupts:"
  %define %$pref " "
	dispint _CATCHPMINT214C, "21.4C"
  %ifidn %$pref," "
	db " none"
  %endif
	db 13,10
 %endif
%unmacro dispint 2.nolist
%pop
 %if _EXTHELP
	db "Extended built-in help pages",13,10
 %endif
 %if _ONLYNON386
	db "Only supports non-386 operation",13,10
 %endif
 %if _ONLY386
	db "Only supports 386+ operation",13,10
 %endif
