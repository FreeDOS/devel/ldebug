%if 0

lDebug help message pages

Copyright (C) 1995-2003 Paul Vojta
Copyright (C) 2008-2023 E. C. Masloch

Usage of the works is permitted provided that this
instrument is retained with the works, so that any entity
that uses the works is notified of this instrument.

DISCLAIMER: THE WORKS ARE WITHOUT WARRANTY.

%endif

%ifndef MESSAGE_INLINE
 %include "debug.mac"
 %include "options.mac"
%endif

	db "More options: (read/write DCO4, read DCS4)",13,10
%if _PM
	db _4digitshex(opt4_int_2F_hook)," enable interrupt 2Fh hook while in 86 Mode",13,10
%endif
%if _CATCHINT08
	db _4digitshex(opt4_int_08_hook)," enable interrupt 8 hook",13,10
%endif
%if _CATCHINT2D
	db _4digitshex(opt4_int_2D_hook)," enable interrupt 2Dh hook",13,10
%endif
%if _CATCHINTFAULTCOND && (_CATCHINT0D || _CATCHINT0C)
	db _4digitshex(opt4_int_fault_hook)," enable 86 Mode fault interrupt hooks",13,10
%endif
	db _8digitssephex(opt4_int_serial_force)," force serial interrupt unhooking",13,10
%if _PM
	db _8digitssephex(opt4_int_2F_force)," force interrupt 2Fh unhooking",13,10
%endif
%if _CATCHINT08
	db _8digitssephex(opt4_int_08_force)," force interrupt 8 unhooking",13,10
%endif
%if _CATCHINT2D
	db _8digitssephex(opt4_int_2D_force)," force interrupt 2Dh unhooking",13,10
%endif
%if _CATCHINT0D
	db _8digitssephex(opt4_int_0D_force)," force interrupt 0Dh unhooking",13,10
%endif
%if _CATCHINT0C
	db _8digitssephex(opt4_int_0C_force)," force interrupt 0Ch unhooking",13,10
%endif
%if _CATCHINT00
	db _8digitssephex(opt4_int_00_force)," force interrupt 0 unhooking",13,10
%endif
%if _CATCHINT01
	db _8digitssephex(opt4_int_01_force)," force interrupt 1 unhooking",13,10
%endif
%if _CATCHINT03
	db _8digitssephex(opt4_int_03_force)," force interrupt 3 unhooking",13,10
%endif
%if _CATCHINT06
	db _8digitssephex(opt4_int_06_force)," force interrupt 6 unhooking",13,10
%endif
%if _CATCHINT18
	db _8digitssephex(opt4_int_18_force)," force interrupt 18h unhooking",13,10
%endif
%if _CATCHINT19
	db _8digitssephex(opt4_int_19_force)," force interrupt 19h unhooking",13,10
%endif
%if _CATCHSYSREQ
 %if _SYSREQINT == 09h
	db _8digitssephex(opt4_int_09_force)," force interrupt 9 unhooking",13,10
 %elif _SYSREQINT == 15h
	db _8digitssephex(opt4_int_15_force)," force interrupt 15h unhooking",13,10
 %else
  %error Unknown SysReq interrupt
 %endif
%endif
%if _CATCHINT07
	db _8digitssephex(opt4_int_07_force)," force interrupt 7 unhooking",13,10
%endif
