%if 0

lDebug help message pages

Copyright (C) 1995-2003 Paul Vojta
Copyright (C) 2008-2023 E. C. Masloch

Usage of the works is permitted provided that this
instrument is retained with the works, so that any entity
that uses the works is notified of this instrument.

DISCLAIMER: THE WORKS ARE WITHOUT WARRANTY.

%endif

%ifndef MESSAGE_INLINE
 %include "debug.mac"
%endif

	db "The RUN commands (T, TP, P, G) and the RE command use the RE command",13,10
	db "buffer to run commands. Most commands are allowed to be run from the",13,10
	db "RE buffer. Disallowed commands include program-loading L, A, E that",13,10
	db "switches the line input mode, TSR, Q, Y, RE, and further RUN commands.",13,10
	db "When the RE buffer is used as input during T, TP, or P with the",13,10
	db "SILENT keyword, commands that use the auxbuff are also disallowed and",13,10
	db "will emit an error noting the conflict.",13,10
	db 13,10
	db "RE.LIST shows the current RE buffer contents in a format usable by",13,10
	db "the other RE commands. RE.APPEND appends the following commands to",13,10
	db "the buffer, if they fit. RE.REPLACE appends to the start of the",13,10
	db "buffer. When specifying commands, an unescaped semicolon is parsed",13,10
	db "as a linebreak to break apart individual commands. Backslashes can",13,10
	db "be used to escape semicolons and backslashes themselves.",13,10
	db 13,10
	db "Prefixing a line with an @ (AT sign) causes the command not to be",13,10
	db "shown to the standard output of the debugger when run. Otherwise,",13,10
	db "the command will be shown with a percent sign % or ~% prompt.",13,10
	db 13,10
	db "The default RE buffer content is @R. This content is also",13,10
	db "detected and handled specifically; if found as the only command",13,10
	db "the handler directly calls the register dump implementation",13,10
	db "without setting up and tearing down the special execution",13,10
	db "environment used to run arbitrary commands from the RE buffer.",13,10
