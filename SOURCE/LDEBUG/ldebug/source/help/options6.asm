%if 0

lDebug help message pages

Copyright (C) 1995-2003 Paul Vojta
Copyright (C) 2008-2023 E. C. Masloch

Usage of the works is permitted provided that this
instrument is retained with the works, so that any entity
that uses the works is notified of this instrument.

DISCLAIMER: THE WORKS ARE WITHOUT WARRANTY.

%endif

%ifndef MESSAGE_INLINE
 %include "debug.mac"
 %include "options.mac"
%endif

	db "More options: (read/write DCO6, read DCS6)",13,10
%if _VXCHG
	db _4digitshex(opt6_vv_mode)," enable video screen swapping",13,10
	db _4digitshex(opt6_vv_keep)," keep video screen when disabling swapping",13,10
	db _4digitshex(opt6_vv_int16)," read key from interrupt 16h when swapping (V command)",13,10
%endif
%if _DEBUG
	db _4digitshex(opt6_debug_exception_late)," run breakpoint late in debugger exception",13,10
	db _4digitshex(opt6_debug_exception_early)," run breakpoint early in debugger exception",13,10
 %if _DEBUG_COND
	db _4digitshex(opt6_debug_exception)," enable debug mode when debugger exception occurs",13,10
	db _4digitshex(opt6_debug_mode)," enable debug mode (and BU command)",13,10
 %endif
%endif
	db _4digitshex(opt6_bios_output)," use ROM-BIOS output even when DOS available",13,10
	db _4digitshex(opt6_flat_binary)," load and write .EXE and .COM files like flat .BIN files (/F+)",13,10
	db _4digitshex(opt6_big_stack)," for loading flat .BIN files set up Stack Segment != PSP (/E+)",13,10
%if _40COLUMNS
	db _4digitshex(opt6_40_columns)," enable 40-column friendly mode",13,10
	db _4digitshex(opt6_40_indent_odd)," in 40-column mode indent odd D lines more",13,10
	db _4digitshex(opt6_40_dash)," in 40-column mode display dashes at half of D length",13,10
%endif
	db _6digitssephex(opt6_share_serial_irq)," allow to share serial IRQ handler",13,10
%if _DEBUG
	db _6digitssephex(opt6_debug_putrunint_early)," run breakpoint early in putrunint",13,10
 %if _DEBUG_COND
	db _6digitssephex(opt6_debug_putrunint)," enable debug mode when putrunint called",13,10
 %endif
%endif
	db _8digitssephex(opt6_bios_io)," use ROM-BIOS I/O even when DOS available (disables script file read)",13,10
%if _REGSREADABLEFLAGS
	db _8digitssephex(opt6_r_flags_style2)," display flags in style 2 for R command register dump",13,10
	db _8digitssephex(opt6_r_flags_style3)," display flags in style 3 for R command register dump",13,10
%endif
%if _REGSLINEBREAK
	db _8digitssephex(opt6_r_linebreak_conditional)," linebreak before R register dump if not column 0 (int 10h only)",13,10
%endif
