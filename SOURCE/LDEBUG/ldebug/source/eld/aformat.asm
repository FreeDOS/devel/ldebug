
; Public Domain

; test install command: install; .; aformat uninstall
; test run command: ; .; .

%include "lmacros3.mac"
%include "eld.mac"
%include "eldcall.mac"
%include "elddata.mac"

	numdef BYTESPERLINE, 16
	numdef BYTESMAX, 512

	cpu 8086

	addsection RELOCATEDDATA, nobits vstart=_ELD_RELOC_VSTART
relocateddata:

	addsection HEADER, start=0

	istruc ELD_HEADERX
at eldhxHeader
		; ELD executable header
	istruc ELD_HEADER
at eldhSignature,	db "ELD1"
			db 0,0,0
			db 26
at eldhCodeOffset,	dd CODEOFFSET
at eldhCodeImageLength,	dw code_size
at eldhCodeAllocLength,	dw 0
at eldhDataOffset,	dd DATAOFFSET
at eldhDataImageLength,	dw data_size
at eldhDataAllocLength,	dw total_data_size - data_size
at eldhCodeEntrypoint,	dw linker - code
at eldhReserved
at eldhExtensionSize,	dw header_extension_end - $$
	iend
at eldhxDescriptionOffset,	dd description
at eldhxHelpOffset,		dd DATAOFFSET + msg.help - datastart
PUT_ELDHX_DATETIME_OFFSET
header_extension_end:
	iend

description:		asciz "Format written data after each assembly line."


	align 16, db 0

CODEOFFSET equ $ - $$
	addsection CODE, follows=HEADER vstart=_ELD_CODE_VSTART
%define CODEFIXUP - code + 0
code:
		; ELD instance header
	istruc ELD_INSTANCE
at eldiStartCode
at eldiEndCode
at eldiStartData
at eldiEndData
at eldiIdentifier,	fill 8, 32, db "AFORMAT"
at eldiListing,		asciz _ELD_LISTING
	iend


command:
	jmp strict short .entry
.chain:
	extcall cmd3_not_ext, required	; must NOT be extcallcall
	times 10 - ($ - command) nop
.entry:
	push si
	cmp al, '-'
	jne @F
	extcallcall skipcomma
@@:
	dec si

	mov dx, msg.aformat
internaldatarelocation
	extcallcall isstring?
	je .ours
	pop si
	dec si
	lodsb
	jmp .chain

.ours:
	pop ax
	extcallcall skipcomma
	dec si
reloc2	mov word [relocateddata], relocateddata
linkdatarelocation lastcmd, -4
linkdatarelocation dmycmd
	mov dx, relocateddata
linkdatarelocation msg.uninstall
	extcallcall isstring?
	je uninstall
	mov dx, msg.off
internaldatarelocation
	extcallcall isstring?
	je .off
	mov dx, msg.on
internaldatarelocation
	extcallcall isstring?
	je .on
	mov dx, msg.toggle
internaldatarelocation
	extcallcall isstring?
	je .toggle
.status:
	lodsb
	extcallcall chkeol
	mov dx, msg.status_on
internaldatarelocation
	rol byte [status], 1
internaldatarelocation
	jc @F
	mov dx, msg.status_off
internaldatarelocation
@@:
	extcallcall putsz
	extcallcall cmd3

.off:
	lodsb
	extcallcall chkeol
	mov byte [status], 0
internaldatarelocation -3
	extcallcall cmd3

.on:
	lodsb
	extcallcall chkeol
	mov byte [status], 0FFh
internaldatarelocation -3
	extcallcall cmd3

.toggle:
	lodsb
	extcallcall chkeol
	not byte [status]
internaldatarelocation
	extcallcall cmd3


preprocess:
.:	jmp strict short .entry
.chain:
	extcall cmd3_preprocessed, required	; must NOT be extcallcall
	times 10 - ($ - .) nop
.entry:
	mov byte [in_cmd3], 0FFh	; true
internaldatarelocation -3
	jmp .chain


before_getline:
.:	jmp strict short .entry
.chain:
	extcall aa_before_getline, required	; must NOT be extcallcall
	times 10 - ($ - .) nop
.entry:
	push di
	lframe
	lenter
	mov si, relocateddata
linkdatarelocation line_out
	mov byte [di], 13
	lodsb
	extcallcall getword
	lvar word, segsel
	 push dx
	cmp al, ':'
	jne .go_chain
	lodsb
	extcallcall getdword
	lvar dword, offset
	 push bx
	 push dx

	rol byte [in_cmd3], 1
internaldatarelocation
	jc .store_address

	rol byte [status], 1
internaldatarelocation
	jnc .store_address

	mov si, relocateddata
linkdatarelocation line_out
	neg si
	add si, di
	cmp si, 4 + 1 + 4 + 1
	je @F
	cmp si, 4 + 1 + 8 + 1
	jne .store_address
@@:
	lvar word, prefix
	 push si
	cmp word [bp + ?prefix], 4 + 1 + 8 + 1
	jne .16prepare
.32prepare:
subcpu 386
	mov ecx, dword [bp + ?offset]
	mov esi, dword [offset]
internaldatarelocation
	sub ecx, esi
	cmp ecx, _BYTESMAX
	ja .store_address
	jmp @F
subcpureset

.16prepare:
	mov cx, word [bp + ?offset]
	mov si, word [offset]
internaldatarelocation
	sub cx, si
	cmp cx, _BYTESMAX
	ja .store_address
@@:
	mov dx, cx

.loop:
	mov cx, word [bp + ?prefix]
	mov al, 32
	mov di, buffer
internaldatarelocation
	rep stosb

	mov cx, _BYTESPERLINE
	sub dx, cx			; another full line ?
	jae @F				; yes -->
	add dx, cx			; restore
	jz .store_address		; if zero -->
	xor cx, cx
	xchg cx, dx			; dx = 0, cx = remainder
@@:
	jcxz .store_address
	mov ds, word [segsel]		; => user segment
internaldatarelocation
@@:
	cmp word [bp + ?prefix], 4 + 1 + 8 + 1
	jne .16lod
.32lod:	a32
.16lod:	lodsb				; lod byte
	extcallcall hexbyte		; write to buffer
	loop @B
	 push ss
	 pop ds				; reset
	mov ax, 13 | 10 << 8
	stosw				; linebreak
	push dx
	mov dx, buffer			; -> buffer
internaldatarelocation
	mov cx, di			; -> behind text to write
	sub cx, dx			; = length
	extcallcall puts
	pop dx
	jmp .loop

.store_address:
	mov ax, word [bp + ?segsel]
	mov word [segsel], ax
internaldatarelocation
	mov ax, word [bp + ?offset + 2]
	mov word [offset + 2], ax
internaldatarelocation
	mov ax, word [bp + ?offset]
	mov word [offset], ax
internaldatarelocation

.go_chain:
	lleave
	pop di
	jmp .chain


after_getline:
.:	jmp strict short .entry
.chain:
	extcall aa_after_getline, required	; must NOT be extcallcall
	times 10 - ($ - .) nop
.entry:
	mov byte [in_cmd3], 0		; false, do format next time
internaldatarelocation -3
	dec si
	push si
	mov dx, msg.org
internaldatarelocation
	extcallcall isstring?
	jne @F
	not byte [in_cmd3]		; true, do not format
internaldatarelocation
@@:
	pop si
	lodsb
	jmp .chain


uninstall:
	lodsb
	extcallcall chkeol

	call get_es_ext

	push es
	pop ds

	mov si, hooktable
internaldatarelocation
	lframe
	lenter
	lvar word, table
	 push si

.loop_table:
	rol byte [ss:si + htInstalled], 1
	jnc .next_table
	xor bx, bx		; = 0 (no prior, modify handler)
	mov di, word [ss:si + htEntry]	; di -> us
	mov si, word [ss:si + htHandler]; -> handler
	mov si, word [ss:si]	; si -> first
	test si, si		; none installed ?
	jz .error		; error -->

.loop:
	cmp di, si		; found ?
	je .bx			; yes, use bx -->
	mov bx, si		; bx -> prior handler
	lodsw			; skip entrypoint jmp strict short
	lodsb			; get first byte of chainer
	cmp al, 0E9h		; expecting jmp near ?
	jne .error		; no, error -->
	lodsw			; get rel16 displacement
	add si, ax		; -> next handler
	jmp .loop

.bx:
	test bx, bx		; any prior ?
	jnz .bxnz		; yes -->
	scasw			; skip entrypoint jmp strict short
	cmp byte [di], 0E8h	; is it a call to cmd3_not_ext ?
	jne @F			; no -->
				; yes, reset ext_command_handler to zero
.setbx:
	mov si, word [bp + ?table]
	mov si, word [ss:si + htHandler]; -> handler
	mov word [ss:si], bx
	jmp .done

@@:
	cmp byte [di], 0E9h	; validate
	jne .error		; failure -->
	inc di			; -> rel16 displacement
	mov bx, word [di]	; get displacement
	scasw			; -> after jmp near
	add bx, di		; -> next handler
	jmp .setbx		; set ext_command_handler to next

.bxnz:
	mov si, bx		; -> prior handler with us as downlink
	xchg di, si		; si -> ours, di -> prior
	cmpsw			; skip entrypoint jmp strict short
	movsb			; copy 0E8h/0E9h
	lodsw			; ax = near rel16 displacement
	add ax, si		; add in our base (= absolute offset)
	sub ax, di
	dec ax
	dec ax			; subtract new base (= relative displacement)
	stosw			; store new rel16 displacement
	movsw			; jmp strict short
	movsw			; linkcall target
	movsb			; trailer
.done:
	mov si, word [bp + ?table]
	not byte [ss:si + htInstalled]

.next_table:
	add si, HOOKTABLE_size
	mov word [bp + ?table], si
	cmp si, strict word hooktable_end
internaldatarelocation
	jb .loop_table


	clropt [code + eldiFlags], eldifResident
internalcoderelocation -3	; mark block as free
	mov dx, msg.uninstall_done
internaldatarelocation
@@:
	lleave
	push ss
	pop ds
	extcallcall putsz
	extcallcall cmd3	; return

.error:
	mov ax, 0E01h
	extcallcall setrc
	mov dx, msg.uninstall_error
internaldatarelocation
	jmp @B


get_es_ext:
	mov es, word [relocateddata]
linkdatarelocation extdssel
	extcallcall ispm
	jz @F
	mov es, word [relocateddata]
linkdatarelocation extseg
@@:
	retn


	eldcall_dump_callcall ELDCALL_CALLCALL_LIST

endinstalled equ ($ + CODEFIXUP + 15) & ~15


start:
	mov bx, es
	 push ss
	 pop es
	call skipcomma
	dec si
	mov dx, relocateddata
linkdatarelocation msg.install
	call isstring?
	je install
	mov dx, msg.help
internaldatarelocation
	extcall putsz
	call uninstall_oneshot
	xor ax, ax
	retf


uninstall_oneshot:
	testopt [ss:relocateddata], 1
linkdatarelocation options7, -3
	jnz @F

	mov ax, word [cs:code + eldiEndCode]
internalcoderelocation
	sub ax, word [cs:code + eldiStartCode]
internalcoderelocation
	sub word [relocateddata], ax
linkdatarelocation extseg_used

	mov ax, word [cs:code + eldiEndData]
internalcoderelocation
	sub ax, word [cs:code + eldiStartData]
internalcoderelocation
	sub word [relocateddata], ax
linkdatarelocation extdata_used
@@:
	retn


DATAOFFSET equ CODEOFFSET + code_size
	addsection DATA, follows=CODE vstart=_ELD_DATA_VSTART
%define DATAFIXUP - datastart + 0
datastart:
PUT_ELD_DATETIME

	struc HOOKTABLE
htEntry:		resw 1
htHandler:		resw 1
htInstalled:		resw 1
	endstruc

	align 2, db 0
hooktable:
		; command last so uninstall abort will leave command installed
	istruc HOOKTABLE
at htEntry,		dw before_getline
internalcoderelocation
at htHandler,		dw relocateddata
linkdatarelocation ext_aa_before_getline_handler
at htInstalled,		dw 0
	iend
	istruc HOOKTABLE
at htEntry,		dw after_getline
internalcoderelocation
at htHandler,		dw relocateddata
linkdatarelocation ext_aa_after_getline_handler
at htInstalled,		dw 0
	iend
	istruc HOOKTABLE
at htEntry,		dw preprocess
internalcoderelocation
at htHandler,		dw relocateddata
linkdatarelocation ext_preprocess_handler
at htInstalled,		dw 0
	iend
	istruc HOOKTABLE
at htEntry,		dw command
internalcoderelocation
at htHandler,		dw relocateddata
linkdatarelocation ext_command_handler
at htInstalled,		dw 0
	iend
hooktable_end:

in_cmd3:		db 0FFh
status:			db 0FFh


msg:
.aformat:		asciz "AFORMAT"
.org:			asciz "ORG"
.on:			asciz "ON"
.off:			asciz "OFF"
.toggle:		asciz "TOGGLE"
.status_on:		asciz "AFORMAT is enabled.",13,10
.status_off:		asciz "AFORMAT is disabled.",13,10
.uninstall_done:	db "AFORMAT uninstalled."
%if _ELD_RECLAIM_HINT
			db " (Don't forget to use reclaim.eld)"
%endif
			asciz 13,10
.uninstall_error:	asciz "AFORMAT unable to uninstall!",13,10

uinit_data: equ $

.installed:	asciz "AFORMAT installed.",13,10
.help:		db "Install this ELD using an INSTALL keyword.",13,10
		db 13,10
		db "In the assembler, entering any line will display the",13,10
		db "written bytes formatted as hexadecimal bytes.",13,10
		db 13,10
		db "Run with AFORMAT UNINSTALL to uninstall.",13,10
		asciz


	align 16, db 0
init_data_end:
data_size equ $ - datastart
transient_data_size equ data_size

	absolute uinit_data

	alignb 2
offset:		resd 1
segsel:		resw 1
buffer:		resb (4 + 1 + 8 + 1) + _BYTESPERLINE * 2 + 2

	alignb 16
uinit_data_end:
resident_data_end:
resident_data_size equ resident_data_end - datastart

%if uinit_data_end >= init_data_end
 total_data_size equ $ - datastart
%else
 total_data_size equ init_data_end - datastart
%endif
%assign _DATA_SIZE total_data_size


	usesection CODE

install:
	lodsb
	extcall chkeol

	houdini
	mov es, bx		; => ext seg (writable)

	mov ax, endresident - endinstalled
	sub word [es:code + eldiEndCode], ax
internalcoderelocation		; adjust size
	sub word [relocateddata], ax
linkdatarelocation extseg_used	; adjust size

%if (transient_data_size - resident_data_size) > 0
	mov ax, transient_data_size - resident_data_size
	sub word [es:code + eldiEndData], ax
internalcoderelocation		; adjust size
	sub word [relocateddata], ax
linkdatarelocation extdata_used	; adjust size
%endif

	mov si, hooktable_end - HOOKTABLE_size
internaldatarelocation

.loop_table:
	mov bx, word [si + htHandler]; -> handler
	mov bx, word [bx]	; -> prior
	mov di, word [si + htEntry]	; -> our handler
	test bx, bx		; installing as first ?
	jz .only_first		; yes, simple --> (leave as extcall cmd3_not_ext)
	scasw			; skip entrypoint jmp strict short
	mov al, 0E9h		; = jmp near opcode
	stosb			; store
	xchg ax, bx		; ax -> next handler
	sub ax, di
	dec ax
	dec ax			; ax = ax - (di + 2)
	stosw			; store our downlink as rel16 displacement

.only_first:
	mov bx, word [si + htHandler]; -> handler
	mov ax, word [si + htEntry]
	mov word [bx], ax	; -> our entrypoint
	not byte [si + htInstalled]

.next_table:
	sub si, HOOKTABLE_size
	cmp si, strict word hooktable
internaldatarelocation
	jae .loop_table

	setopt [es:code + eldiFlags], eldifResident
internalcoderelocation -3	; mark block as resident

	testopt [relocateddata], 4
linkdatarelocation options7, -3
	jnz @F
	mov dx, msg.installed
internaldatarelocation
	call putsz
@@:
	xor ax, ax
	retf


%include "eldlink.asm"

	align 16
code_size equ $ - code
