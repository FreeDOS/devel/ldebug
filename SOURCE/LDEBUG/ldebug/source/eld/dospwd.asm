
; Public Domain

; test run command: ; .; .

%include "lmacros3.mac"
%include "eld.mac"
%include "eldcall.mac"
%include "elddata.mac"

	cpu 8086

	addsection RELOCATEDDATA, nobits vstart=_ELD_RELOC_VSTART
relocateddata:

	addsection HEADER, start=0

	istruc ELD_HEADERX
at eldhxHeader
		; ELD executable header
	istruc ELD_HEADER
at eldhSignature,	db "ELD1"
			db 0,0,0
			db 26
at eldhCodeOffset,	dd CODEOFFSET
at eldhCodeImageLength,	dw code_size
at eldhCodeAllocLength,	dw 0
at eldhDataOffset,	dd DATAOFFSET
at eldhDataImageLength,	dw data_size
at eldhDataAllocLength,	dw total_data_size - data_size
at eldhCodeEntrypoint,	dw linker - code
at eldhReserved
at eldhExtensionSize,	dw header_extension_end - $$
	iend
at eldhxDescriptionOffset,	dd description
PUT_ELDHX_DATETIME_OFFSET
header_extension_end:
	iend

description:		asciz "Display DOS current working directory."


	align 16, db 0

CODEOFFSET equ $ - $$
	addsection CODE, follows=HEADER vstart=_ELD_CODE_VSTART
%define CODEFIXUP - code + 0
code:
		; ELD instance header
	istruc ELD_INSTANCE
at eldiStartCode
at eldiEndCode
at eldiStartData
at eldiEndData
at eldiIdentifier,	fill 8, 32, db "DOS PWD"
at eldiListing,		asciz _ELD_LISTING
	iend


start:
	mov bx, es
	 push ss
	 pop es
	extcallcall skipcomma

	extcallcall InDOS
	jz @F
.error:
	extcallcall error

@@:

	extcallcall iseol?
	je .default

	dec si
	mov dx, msg.number
internaldatarelocation
	extcallcall isstring?
	lodsb
	jne .letter
	extcallcall getbyte
	extcallcall chkeol
	xchg ax, dx
	cmp al, 32
	jae .error
	jmp @F

.letter:
	cmp byte [si], ':'
	jne .error
	extcallcall uppercase
	push ax
	inc si
	lodsb
	extcallcall chkeol
	pop ax
	jmp @FF

.default:
	mov ah, 19h
	extcallcall _doscall
@@:
	add al, 'A'
@@:
	mov dl, al
	sub dl, 'A' - 1
	mov ah, ':'
	mov di, buffer
internaldatarelocation
	stosw
	mov al, '\'
	stosb

	mov ah, 47h
	mov si, buffer + 3
internaldatarelocation
	extcallcall _doscall

	mov dx, buffer
internaldatarelocation
	jnc @F

	mov byte [buffer + 2], 0
internaldatarelocation -3
	push dx
	mov dx, msg.error.1
internaldatarelocation
	extcallcall putsz
	pop dx
	extcallcall putsz
	mov dx, msg.error.2
internaldatarelocation
	extcallcall putsz
	mov ax, 0E27h
	extcallcall setrc
	jmp .end

@@:
	extcallcall putsz
	mov dx, msg.linebreak
internaldatarelocation
	extcallcall putsz

.end:
	call uninstall_oneshot
	xor ax, ax
	retf


uninstall_oneshot:
	testopt [ss:relocateddata], 1
linkdatarelocation options7, -3
	jnz @F

	mov ax, word [cs:code + eldiEndCode]
internalcoderelocation
	sub ax, word [cs:code + eldiStartCode]
internalcoderelocation
	sub word [relocateddata], ax
linkdatarelocation extseg_used

	mov ax, word [cs:code + eldiEndData]
internalcoderelocation
	sub ax, word [cs:code + eldiStartData]
internalcoderelocation
	sub word [relocateddata], ax
linkdatarelocation extdata_used
@@:
	retn


DATAOFFSET equ CODEOFFSET + code_size
	addsection DATA, follows=CODE vstart=_ELD_DATA_VSTART
%define DATAFIXUP - datastart + 0
datastart:
PUT_ELD_DATETIME

msg:
.number:		asciz "NUMBER"
.error.1:		asciz "Error getting working directory for drive "
.error.2:
.linebreak:		asciz 13,10

uinit_data: equ $

	align 16, db 0
data_size equ $ - datastart

	absolute uinit_data

	alignb 2
buffer:		resb 128 + 4

	alignb 16
uinit_data_end:

total_data_size equ $ - datastart
%assign _DATA_SIZE total_data_size

	usesection CODE

%include "eldlink.asm"

	align 16
code_size equ $ - code

