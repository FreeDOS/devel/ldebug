
; Public Domain

; test install command: install; if ext "if ext" then r; if ext uninstall
; test run command: if ext eldcomp then r; .; .

%include "lmacros3.mac"
%include "eld.mac"
%include "eldcall.mac"
%include "elddata.mac"

%assign _CATCHINT0C 0
%assign _CATCHINT0D 0
%assign _CATCHINTFAULTCOND 0
%assign _DEBUG 0
%assign _DELAY_BEFORE_BP 0
%assign _DHIGHLIGHT 0
%assign _GETLINEHIGHLIGHT 0
%assign _IMMASM 0
%assign _INPUT_FILE_BOOT 0
%assign _INPUT_FILE_HANDLES 0
%assign _MS_0RANGE_COMPAT 0
%assign _PM 0
%assign _RH 0
%assign _SYMBOLIC 0
%assign _VXCHG 0
%include "options.mac"

	cpu 8086

	addsection RELOCATEDDATA, nobits vstart=_ELD_RELOC_VSTART
relocateddata:

	addsection HEADER, start=0

	istruc ELD_HEADERX
at eldhxHeader
		; ELD executable header
	istruc ELD_HEADER
at eldhSignature,	db "ELD1"
			db 0,0,0
			db 26
at eldhCodeOffset,	dd CODEOFFSET
at eldhCodeImageLength,	dw code_size
at eldhCodeAllocLength,	dw 0
at eldhDataOffset,	dd DATAOFFSET
at eldhDataImageLength,	dw data_size
at eldhDataAllocLength,	dw total_data_size - data_size
at eldhCodeEntrypoint,	dw linker - code
at eldhReserved
at eldhExtensionSize,	dw header_extension_end - $$
	iend
at eldhxDescriptionOffset,	dd description
at eldhxHelpOffset,		dd DATAOFFSET + msg.help - datastart
PUT_ELDHX_DATETIME_OFFSET
header_extension_end:
	iend

description:		asciz "Provide IF EXT conditional construct."


	align 16, db 0

CODEOFFSET equ $ - $$
	addsection CODE, follows=HEADER vstart=_ELD_CODE_VSTART
%define CODEFIXUP - code + 0
code:
code_start:
		; ELD instance header
	istruc ELD_INSTANCE
at eldiStartCode
at eldiEndCode
at eldiStartData
at eldiEndData
at eldiIdentifier,	fill 8, 32, db "IF EXT"
at eldiListing,		asciz _ELD_LISTING
	iend


DATAOFFSET equ CODEOFFSET + code_size
	addsection DATA, follows=CODE vstart=_ELD_DATA_VSTART
%define DATAFIXUP - datastart + 0
datastart:
PUT_ELD_DATETIME


	usesection CODE

command:
	jmp strict short .entry
.chain:
	extcall cmd3_not_ext, required	; must NOT be extcallcall
	times 10 - ($ - command) nop
.entry:
	push si
	cmp al, '-'
	jne @F
	extcallcall skipcomma
@@:
	dec si

	lodsw
	and ax, ~2020h
	cmp ax, "IF"
	je @F
.transfer_to_chain:
.not_if:
	pop si
	dec si
	lodsb
	jmp .chain

@@:

	extcallcall skipcomma
	extcall isoperator?
	jne .if
	mov bx, cx
	add bx, bx			; bh = 0 !
	push ax
	extcall ..@call_operator_dispatchers
	pop ax
	test bx, bx
	jnz .not_if
	extcallcall skipcomma
.if:
	call parse_not

	mov dx, msg.ext
internaldatarelocation
	extcallcall isstring?
	jne .transfer_to_chain

.ours:
	pop ax
	extcallcall skipcomma
	dec si
reloc2	mov word [relocateddata], relocateddata
linkdatarelocation lastcmd, -4
linkdatarelocation dmycmd
	mov dx, relocateddata
linkdatarelocation msg.uninstall
	extcallcall isstring?
	je uninstall

	call do_if
	jmp word [relocateddata]	; cannot be zero. may re-enter us
linkdatarelocation ext_command_handler


parse_not:
	clropt [relocateddata], dif3_if_not
linkdatarelocation internalflags3, -3
	dec si
	mov dx, relocateddata
linkdatarelocation msg.not
	extcallcall isstring?
	lodsb
	jne @F
	extcallcall skipcomm0
	setopt [relocateddata], dif3_if_not
linkdatarelocation internalflags3, -3

@@:
	dec si
	retn


do_if:
	mov bx, si
	lodsb
	cmp al, '"'
	je quoted
	cmp al, "'"
	je quoted
unquoted:
	dec si
.loop:
	lodsb
	cmp al, 32
	je end
	cmp al, 9
	je end
	cmp al, ','
	je end
	extcallcall iseol?.notsemicolon
	jne .loop
	jmp unexpected_eol

quoted:
houdini
	inc bx
	mov ah, al
.loop:
	lodsb
	cmp al, ah
	je end_quoted
	extcallcall iseol?.notsemicolon
	jne .loop
unexpected_eol:
	mov ax, 0E40h
	extcallcall setrc
	mov dx, msg.unexpected_eol
internaldatarelocation
	extcallcall putsz
cmd3_j:
	extcallcall cmd3

end_quoted:
	lea cx, [si - 1]
	lodsb
	jmp @F

end:
	lea cx, [si - 1]
@@:
	extcallcall skipcomm0
	dec si
	mov dx, relocateddata
linkdatarelocation msg.then
	extcallcall isstring?
	jne error

	push si
	sub cx, bx
	jz notfound
	cmp cx, 8
	ja notfound
	mov si, bx
	mov di, buffer
internaldatarelocation
	mov dx, cx
@@:
	lodsb
	extcallcall uppercase
	stosb
	loop @B
	neg dx
	add dx, 8
	xchg cx, dx
	mov al, 32
	rep stosb
	mov si, buffer
internaldatarelocation
	push cs
	pop es
	xor bx, bx
loop_match:
	cmp bx, word [relocateddata]
linkdatarelocation extseg_used
	jae notfound
	test byte [es:bx + eldiFlags], eldifResident
	jz next_match
	lea di, [bx + eldiIdentifier]
	push si
	mov cx, 8
@@:
	repe cmpsb
	je @F
	mov al, [es:di - 1]
	extcallcall uppercase
	cmp al, [si - 1]
	je @B
@@:
	pop si
	je found
next_match:
	mov bx, [es:bx + eldiEndCode]
	jmp loop_match

notfound:
	mov al, 0
	db __TEST_IMM16			; (skip mov)
found:
	mov al, 0FFh

	push ss
	pop es

	testopt [relocateddata], dif3_if_not
linkdatarelocation internalflags3, -3
	jz @F
	not al
@@:
	pop si
	test al, al
	jz cmd3_j			; do not run -->
	extcallcall skipcomma
	retn


get_es_ext:
	mov es, word [relocateddata]
linkdatarelocation extdssel
	extcallcall ispm
	jz @F
	mov es, word [relocateddata]
linkdatarelocation extseg
@@:
	retn


uninstall:
	lodsb
	extcallcall chkeol

	call get_es_ext

	push es
	pop ds
	xor bx, bx		; = 0 (no prior, modify ext_command_handler)
	mov di, command		; di -> us
internalcoderelocation
	mov si, word [ss:relocateddata]
linkdatarelocation ext_command_handler
				; si -> first
	test si, si		; none installed ?
	jz .error		; error -->

.loop:
	cmp di, si		; found ?
	je .bx			; yes, use bx -->
	mov bx, si		; bx -> prior handler
	lodsw			; skip entrypoint jmp strict short
	lodsb			; get first byte of chainer
	cmp al, 0E9h		; expecting jmp near ?
	jne .error		; no, error -->
	lodsw			; get rel16 displacement
	add si, ax		; -> next handler
	jmp .loop

.bx:
	test bx, bx		; any prior ?
	jnz .bxnz		; yes -->
	scasw			; skip entrypoint jmp strict short
	cmp byte [di], 0E8h	; is it a call to cmd3_not_ext ?
	jne @F			; no -->
				; yes, reset ext_command_handler to zero
.setbx:
	mov word [ss:relocateddata], bx
linkdatarelocation ext_command_handler
	jmp .done

@@:
	cmp byte [di], 0E9h	; validate
	jne .error		; failure -->
	inc di			; -> rel16 displacement
	mov bx, word [di]	; get displacement
	scasw			; -> after jmp near
	add bx, di		; -> next handler
	jmp .setbx		; set ext_command_handler to next

.bxnz:
	mov si, bx		; -> prior handler with us as downlink
	xchg di, si		; si -> ours, di -> prior
	cmpsw			; skip entrypoint jmp strict short
	movsb			; copy 0E8h/0E9h
	lodsw			; ax = near rel16 displacement
	add ax, si		; add in our base (= absolute offset)
	sub ax, di
	dec ax
	dec ax			; subtract new base (= relative displacement)
	stosw			; store new rel16 displacement
	movsw			; jmp strict short
	movsw			; linkcall target
	movsb			; trailer
.done:
	clropt [code + eldiFlags], eldifResident
internalcoderelocation -3	; mark block as free
	mov dx, msg.uninstall_done
internaldatarelocation
@@:
	push ss
	pop ds
	extcallcall putsz
	extcallcall cmd3	; return

.error:
	mov ax, 0E01h
	extcallcall setrc
	mov dx, msg.uninstall_error
internaldatarelocation
	jmp @B


error:
	extcall error

	eldcall_dump_callcall ELDCALL_CALLCALL_LIST

endinstalled equ ($ + CODEFIXUP + 15) & ~15


start:
	mov bx, es
	 push ss
	 pop es

	call skipcomma
	dec si
	mov dx, relocateddata
linkdatarelocation msg.install
	call isstring?
	je install
	mov dx, msg.if
internaldatarelocation
	call isstring?
	je oneshot
	mov dx, msg.help
internaldatarelocation
	call putsz
	call uninstall_oneshot
	xor ax, ax
	retf


oneshot:
	call skipcomma
	call parse_not
	mov dx, msg.ext
internaldatarelocation
	call isstring?
	jne error
	call skipcomma
	dec si
	call do_if

	dec si
	mov di, si
	mov al, 13
	mov cx, 255
	repne scasb
	xchg ax, di
	sub ax, si
	mov cx, ax
	dec ax

	mov di, buffer
internaldatarelocation
	stosb
	rep movsb

	mov ax, word [relocateddata]
linkdatarelocation ext_inject_handler
	mov word [priorinject], ax
internaldatarelocation


	call get_es_ext
	setopt [es:code + eldiFlags], eldifResident
internalcoderelocation -3		; mark block as resident

	mov word [relocateddata], .inject
linkdatarelocation ext_inject_handler, -4
internalcoderelocation
	call cmd3

.inject:
	houdini

	mov di, relocateddata + 1
linkdatarelocation line_in
	mov si, buffer
internaldatarelocation

	extcall yy_reset_buf
	push di
	mov cx, words(256)
	rep movsw
	pop si

	mov ax, word [priorinject]
internaldatarelocation
	mov word [relocateddata], ax
linkdatarelocation ext_inject_handler

	inc si
	extcall skipwhite

	call get_es_ext
	clropt [es:code + eldiFlags], eldifResident
internalcoderelocation -3		; mark block as free
	 push ss
	 pop es

	extcall cmd3_injected


uninstall_oneshot:
	testopt [ss:relocateddata], 1
linkdatarelocation options7, -3
	jnz @F

	mov ax, word [cs:code + eldiEndCode]
internalcoderelocation
	sub ax, word [cs:code + eldiStartCode]
internalcoderelocation
	sub word [relocateddata], ax
linkdatarelocation extseg_used

	mov ax, word [cs:code + eldiEndData]
internalcoderelocation
	sub ax, word [cs:code + eldiStartData]
internalcoderelocation
	sub word [relocateddata], ax
linkdatarelocation extdata_used
@@:
	retn


	usesection DATA
msg:
.ext:			asciz "EXT"
.unexpected_eol:	asciz "IF EXT command error: Unexpected end of line!",13,10
.uninstall_done:	db "IF EXT command uninstalled."
%if _ELD_RECLAIM_HINT
			db " (Don't forget to use reclaim.eld)"
%endif
			asciz 13,10
.uninstall_error:	asciz "IF EXT command unable to uninstall!",13,10

uinit_data: equ $

.if:		asciz "IF"
.installed:	asciz "IF EXT command installed.",13,10
.help:		db "Install this ELD using the INSTALL keyword parameter.",13,10
		db 13,10
		db 'Run as IF [NOT] EXT "name" THEN command to check if an ELD is installed.',13,10
		db "Run as IF EXT UNINSTALL to uninstall this ELD.",13,10
		asciz

	align 16, db 0
init_data_end:
data_size equ $ - datastart
transient_data_size equ data_size

	absolute uinit_data

	alignb 2
priorinject:	resw 1
buffer:		resb 8

	alignb 16
resident_data_end:
resident_data_size equ resident_data_end - datastart

	absolute buffer
		resb 256

	alignb 16
uinit_data_end:

%if uinit_data_end >= init_data_end
 total_data_size equ $ - datastart
%else
 total_data_size equ init_data_end - datastart
%endif
%assign _DATA_SIZE total_data_size


	usesection CODE

install:
	lodsb
	extcall chkeol

	houdini
	mov es, bx		; => ext seg (writable)

	mov ax, endresident - endinstalled
	sub word [es:code + eldiEndCode], ax
internalcoderelocation		; adjust size
	sub word [relocateddata], ax
linkdatarelocation extseg_used	; adjust size

%if (transient_data_size - resident_data_size) > 0
	mov ax, transient_data_size - resident_data_size
	sub word [es:code + eldiEndData], ax
internalcoderelocation		; adjust size
	sub word [relocateddata], ax
linkdatarelocation extdata_used	; adjust size
%endif

	mov bx, word [relocateddata]
linkdatarelocation ext_command_handler
				; -> prior
	mov di, command		; -> our handler
internalcoderelocation
	test bx, bx		; installing as first ?
	jz .only_first		; yes, simple --> (leave as extcall cmd3_not_ext)
	scasw			; skip entrypoint jmp strict short
	mov al, 0E9h		; = jmp near opcode
	stosb			; store
	xchg ax, bx		; ax -> next handler
	sub ax, di
	dec ax
	dec ax			; ax = ax - (di + 2)
	stosw			; store our downlink as rel16 displacement

.only_first:
	setopt [es:code + eldiFlags], eldifResident
internalcoderelocation -3	; mark block as resident
	mov word [relocateddata], command
linkdatarelocation ext_command_handler, -4
internalcoderelocation		; -> our entrypoint

	testopt [relocateddata], 4
linkdatarelocation options7, -3
	jnz @F
	mov dx, msg.installed
internaldatarelocation
	call putsz
@@:
	xor ax, ax
	retf


%include "eldlink.asm"

	align 16
code_size equ $ - code
