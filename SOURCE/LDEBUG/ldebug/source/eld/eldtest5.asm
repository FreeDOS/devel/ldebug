
; Public Domain

%include "lmacros3.mac"
%include "eld.mac"
%include "eldcall.mac"
%include "elddata.mac"

	cpu 8086

	addsection RELOCATEDDATA, nobits vstart=_ELD_RELOC_VSTART
relocateddata:

	addsection HEADER, start=0

		; ELD executable header
	istruc ELD_HEADER
at eldhSignature,	db "ELD1"
			db 0,0,0
			db 26
at eldhCodeOffset,	dd CODEOFFSET
at eldhCodeImageLength,	dw code_size
at eldhCodeAllocLength,	dw 0
at eldhDataOffset,	dd DATAOFFSET
at eldhDataImageLength,	dw data_size
at eldhDataAllocLength,	dw 0
at eldhCodeEntrypoint,	dw linker - code
at eldhReserved
	iend


	align 16, db 0

CODEOFFSET equ $ - $$
	addsection CODE, follows=HEADER vstart=_ELD_CODE_VSTART
%define CODEFIXUP - code + 0
code:
		; ELD instance header
	istruc ELD_INSTANCE
at eldiStartCode
at eldiEndCode
at eldiStartData
at eldiEndData
at eldiIdentifier,	fill 8, 32, db "ELDTEST"
at eldiListing,		asciz _ELD_LISTING
	iend

start:

	push ss
	pop es
	extcallcall skipcomma
	dec si
	mov bx, table - 4
internaldatarelocation
@@:
	add bx, 4
	mov dx, word [bx]
	test dx, dx
	jz notfound
	extcallcall isstring?
	jne @B

	extcallcall skipcomma
	jmp word [bx + 2]

say_install:
	mov dx, msg.install
internaldatarelocation
	extcallcall putsz
	jmp end

say_uninstall:
	mov dx, msg.uninstall
internaldatarelocation
	extcallcall putsz
	jmp end

say_run:
	mov dx, msg.run
internaldatarelocation
	extcallcall putsz
	jmp end

notfound:
	mov dx, msg.notfound
internaldatarelocation
	extcallcall putsz
	jmp end


end:
	mov ax, word [cs:code + eldiEndCode]
internalcoderelocation
	sub ax, word [cs:code + eldiStartCode]
internalcoderelocation
	sub word [relocateddata], ax
linkdatarelocation extseg_used

	mov ax, word [cs:code + eldiEndData]
internalcoderelocation
	sub ax, word [cs:code + eldiStartData]
internalcoderelocation
	sub word [relocateddata], ax
linkdatarelocation extdata_used

	xor ax, ax
	retf

DATAOFFSET equ CODEOFFSET + code_size
	addsection DATA, follows=CODE vstart=_ELD_DATA_VSTART
%define DATAFIXUP - datastart + 0
datastart:
	dw 26h, 38h, 42h, 0

	align 4, db 0
table:
	dw relocateddata
linkdatarelocation msg.install
	dw say_install
internalcoderelocation
	dw relocateddata
linkdatarelocation msg.uninstall
	dw say_uninstall
internalcoderelocation
	dw relocateddata
linkdatarelocation msg.run
	dw say_run
internalcoderelocation
	dw 0

msg:
.install:	asciz "Installing.",13,10
.uninstall:	asciz "Uninstalling.",13,10
.run:		asciz "Running.",13,10
.notfound:	asciz "No command matched.",13,10

	align 16, db 0
data_size equ $ - datastart
%assign _DATA_SIZE data_size

	usesection CODE

%include "eldlink.asm"

	align 16
code_size equ $ - code
