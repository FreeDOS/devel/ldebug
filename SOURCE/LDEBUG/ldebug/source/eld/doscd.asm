
; Public Domain

; test run command: .; .; .

%include "lmacros3.mac"
%include "eld.mac"
%include "eldcall.mac"
%include "elddata.mac"

	cpu 8086

	addsection RELOCATEDDATA, nobits vstart=_ELD_RELOC_VSTART
relocateddata:

	addsection HEADER, start=0

	istruc ELD_HEADERX
at eldhxHeader
		; ELD executable header
	istruc ELD_HEADER
at eldhSignature,	db "ELD1"
			db 0,0,0
			db 26
at eldhCodeOffset,	dd CODEOFFSET
at eldhCodeImageLength,	dw code_size
at eldhCodeAllocLength,	dw 0
at eldhDataOffset,	dd DATAOFFSET
at eldhDataImageLength,	dw data_size
at eldhDataAllocLength,	dw total_data_size - data_size
at eldhCodeEntrypoint,	dw linker - code
at eldhReserved
at eldhExtensionSize,	dw header_extension_end - $$
	iend
at eldhxDescriptionOffset,	dd description
PUT_ELDHX_DATETIME_OFFSET
header_extension_end:
	iend

description:		asciz "Change DOS current working directory."


	align 16, db 0

CODEOFFSET equ $ - $$
	addsection CODE, follows=HEADER vstart=_ELD_CODE_VSTART
%define CODEFIXUP - code + 0
code:
		; ELD instance header
	istruc ELD_INSTANCE
at eldiStartCode
at eldiEndCode
at eldiStartData
at eldiEndData
at eldiIdentifier,	fill 8, 32, db "DOS CD"
at eldiListing,		asciz _ELD_LISTING
	iend


start:
	mov bx, es
	 push ss
	 pop es
	extcallcall skipcomma

	extcallcall InDOS
	jz @F
.error:
	extcallcall error

@@:

	extcallcall iseol?
	je .error

	push si
	extcallcall uppercase
	cmp byte [si], ':'
	jne .set
	sub al, 'A'
	mov dl, al
	inc si
	extcallcall skipwhite
	extcallcall iseol?
	jne .set

	pop ax			; discard
	call .setdrive
	jmp .end


.setdrive:
	mov ah, 0Eh
	extcallcall _doscall
		; function 0Eh is CP/M-ish, doesn't return an error status

	mov ah, 19h
	extcallcall _doscall	; get current drive
	cmp al, dl		; matches ?
	je @F			; yes -->

	pop dx			; discard near return address
	mov dx, msg.error_drive
internaldatarelocation
	mov ax, 0E20h
	jmp .error_putsz_setrc

@@:
	retn


.set:
	pop si
	push si
	dec si
	mov dx, msg.both
internaldatarelocation
	extcallcall isstring?
	je .both
	mov dx, msg.ifboth
internaldatarelocation
	extcallcall isstring?
	jne @F

.ifboth:
	not byte [ifboth]
internaldatarelocation

.both:
	extcallcall skipcomma
	extcallcall iseol?
	je @F

	pop ax
	not byte [changeboth]
internaldatarelocation

	db __TEST_IMM8		; skip pop
@@:
	pop si
	; extcallcall InDOS
	cmp al, al
	extcallcall yy_dos_parse_name
	extcallcall chkeol

	rol byte [changeboth], 1
internaldatarelocation
	jnc .dironly

	mov si, bx
	lodsb
	extcallcall uppercase
	cmp byte [si], ':'
	je .both_have_drive
	rol byte [ifboth], 1
internaldatarelocation
	jnc .error
	jmp .dironly

.both_have_drive:
	sub al, 'A'
	mov dl, al
	call .setdrive

.dironly:
	mov ah, 3Bh
	mov dx, bx
	extcallcall _doscall
	jnc .end

	mov dx, msg.error_dir
internaldatarelocation
	mov ax, 0E21h
.error_putsz_setrc:
	extcallcall setrc
	extcallcall putsz

.end:
	call uninstall_oneshot
	xor ax, ax
	retf


uninstall_oneshot:
	testopt [ss:relocateddata], 1
linkdatarelocation options7, -3
	jnz @F

	mov ax, word [cs:code + eldiEndCode]
internalcoderelocation
	sub ax, word [cs:code + eldiStartCode]
internalcoderelocation
	sub word [relocateddata], ax
linkdatarelocation extseg_used

	mov ax, word [cs:code + eldiEndData]
internalcoderelocation
	sub ax, word [cs:code + eldiStartData]
internalcoderelocation
	sub word [relocateddata], ax
linkdatarelocation extdata_used
@@:
	retn


DATAOFFSET equ CODEOFFSET + code_size
	addsection DATA, follows=CODE vstart=_ELD_DATA_VSTART
%define DATAFIXUP - datastart + 0
datastart:
PUT_ELD_DATETIME

changeboth:	db 0
ifboth:		db 0

msg:
.error_dir:	asciz "Error setting current directory!",13,10
.error_drive:	asciz "Error setting current drive!",13,10
.ifboth:	db "IF"
.both:		asciz "BOTH"

uinit_data: equ $

	align 16, db 0
data_size equ $ - datastart

	absolute uinit_data

	alignb 16
uinit_data_end:

total_data_size equ $ - datastart
%assign _DATA_SIZE total_data_size

	usesection CODE

%include "eldlink.asm"

	align 16
code_size equ $ - code

