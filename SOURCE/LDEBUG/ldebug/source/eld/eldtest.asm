
; Public Domain

%include "lmacros3.mac"
%include "eld.mac"

	cpu 8086

	istruc ELD_HEADERX
at eldhxHeader
		; ELD executable header
	istruc ELD_HEADER
at eldhSignature,	db "ELD1"
			db 0,0,0
			db 26
at eldhCodeOffset,	dd code
at eldhCodeImageLength,	dw code_size
at eldhCodeAllocLength,	dw 0
at eldhDataOffset,	dd data
at eldhDataImageLength,	dw data_size
at eldhDataAllocLength,	dw 0
at eldhCodeEntrypoint,	dw code_entry - code
at eldhReserved
at eldhExtensionSize,	dw header_extension_end - $$
	iend
at eldhxDescriptionOffset,	dd description
at eldhxHelpOffset,		dd help
header_extension_end:
	iend

description:		asciz "Test ELD."
help:
	times 16 db "0123456789abcdef,0123456789abcdef,0123456789abcdef",13,10
	db 0


	align 16, db 0
code:
		; ELD instance header
	istruc ELD_INSTANCE
at eldiStartCode
at eldiEndCode
at eldiStartData
at eldiEndData
at eldiIdentifier,	fill 8, 32, db "ELDTEST"
at eldiListing,		asciz _ELD_LISTING
	iend

code_entry:
	xor ax, ax
	retf

	align 16
code_size equ $ - code

data:
	dw 26h, 38h, 42h, 0

	align 16, db 0
data_size equ $ - data
