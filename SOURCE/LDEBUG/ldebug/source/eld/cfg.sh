#! /bin/bash

# Public Domain

# If you want to override configuration without the
# hassle of having to exclude differences in this file
# (cfg.sh) from the SCM, you may provide ovr.sh.
# The meaning of this line allows you to copy cfg.sh
# to serve as a template for ovr.sh without changes.
[ -f ovr.sh ] && [[ "${BASH_SOURCE[0]##*/}" != ovr.sh ]] && . ovr.sh

# As the below only are set if no value is provided
# yet, any value can be overridden from the shell.
[ -z "$LMACROS_DIR" ] && LMACROS_DIR=../../../lmacros/
[ -z "$SCANPTAB_DIR" ] && SCANPTAB_DIR=../../../scanptab/
[ -z "$LDEBUG_DIR" ] && LDEBUG_DIR=../
[ -z "$BIN_DIR" ] && BIN_DIR=../../bin/
[ -z "$LST_DIR" ] && LST_DIR=../../lst/
[ -z "$TMP_DIR" ] && TMP_DIR=../../tmp/

[ -z "$use_build_xld" ] && use_build_xld=1
[ -z "$use_build_eld" ] && use_build_eld=1
[ -z "$use_build_eldcomp" ] && use_build_eldcomp=1
[ -z "$use_build_eld_datetime" ] && use_build_eld_datetime=0
[ -z "$use_build_eld_houdini" ] && use_build_eld_houdini=0

[ -z "$NASM" ] && NASM=nasm

[ -z "$HEATSHRINK_W" ] && HEATSHRINK_W=12
[ -z "$HEATSHRINK_L" ] && HEATSHRINK_L=5
