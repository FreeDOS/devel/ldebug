
; Public Domain

%include "lmacros3.mac"
%include "eld.mac"
%include "eldcall.mac"
%include "elddata.mac"

%assign _DEBUG 1
%assign _CATCHINT0C 0
%assign _CATCHINT0D 0
%assign _CATCHINTFAULTCOND 0
%assign _DEBUG_COND 1
%assign _DELAY_BEFORE_BP 0
%assign _DHIGHLIGHT 0
%assign _GETLINEHIGHLIGHT 0
%assign _IMMASM 0
%assign _INPUT_FILE_BOOT 0
%assign _INPUT_FILE_HANDLES 0
%assign _MS_0RANGE_COMPAT 0
%assign _PM 0
%assign _RH 0
%assign _SYMBOLIC 0
%assign _VXCHG 0
%include "options.mac"

%include "iniload.mac"

	cpu 8086

	addsection RELOCATEDDATA, nobits vstart=_ELD_RELOC_VSTART
relocateddata:

	addsection HEADER, start=0

	istruc ELD_HEADERX
at eldhxHeader
		; ELD executable header
	istruc ELD_HEADER
at eldhSignature,	db "ELD1"
			db 0,0,0
			db 26
at eldhCodeOffset,	dd CODEOFFSET
at eldhCodeImageLength,	dw code_size
at eldhCodeAllocLength,	dw 0
at eldhDataOffset,	dd DATAOFFSET
at eldhDataImageLength,	dw data_size
at eldhDataAllocLength,	dw 0
at eldhCodeEntrypoint,	dw linker - code
at eldhReserved
at eldhExtensionSize,	dw header_extension_end - $$
	iend
at eldhxLibTable,		dd libtable - $$
header_extension_end:
	iend

libtable:
 istruc ELD_LIBTABLE
at eldltFormat,		dw 0
at eldltAmount,		dw (lib_end - lib) / 12
at eldltOffset,		dd LIBOFFSET
 iend
lib:
	dd embedded_eld - lib_start
	fill 8, 32, db "embedded"
lib_end:


	align 16, db 0

CODEOFFSET equ $ - $$
	addsection CODE, follows=HEADER vstart=_ELD_CODE_VSTART
%define CODEFIXUP - code + 0
code:
		; ELD instance header
	istruc ELD_INSTANCE
at eldiStartCode
at eldiEndCode
at eldiStartData
at eldiEndData
at eldiIdentifier,	fill 8, 32, db "ELDTESTD"
at eldiListing,		asciz _ELD_LISTING
	iend

start:

	push ss
	pop es
%if 0
; test ;run command: ; .; .
	xor ax, ax
	retf
%endif

	mov bx, relocateddata
linkdatarelocation load_data_lowest	; make yy_boot_get and yy_boot_update no-ops
	testopt [relocateddata], nodosloaded
linkdatarelocation internalflags, -3
	jnz @F
	mov bx, [relocateddata]
linkdatarelocation ext_handle
	extcallcall InDOS
	jz @F
	extcallcall error
@@:
	push si

	mov ax, relocateddata
linkdatarelocation ext_finish.bp_is_set

	mov ax, 4201h
lib_displacement equ LIBOFFSET + embedded_eld - lib_start - DATAOFFSET - data_size
	mov cx, (lib_displacement >> 16) & 0FFFFh
	mov dx, lib_displacement & 0FFFFh

	testopt [relocateddata], nodosloaded
linkdatarelocation internalflags, -3
	jz @F
	extcallcall yy_boot_seek_current.bx

	mov ax, word [relocateddata - LOADDATA2 + ldCurrentSeek]
linkdatarelocation load_data
	mov dx, word [relocateddata - LOADDATA2 + ldCurrentSeek + 2]
linkdatarelocation load_data
	jmp @FF

@@:
	extcallcall _doscall
@@:
	jc .io_error
	mov word [eldheader], ax
internaldatarelocation
	mov word [eldheader + 2], dx
internaldatarelocation


%assign _BOOTLDR 1
%assign _APPLICATION 1
%assign _DEVICE 1
%assign _PM 1
%define relocated(address) relocateddata
%define doscall extcallcall _doscall
%assign ELD 1

%include "loadeld.asm"

.error_internal:
	mov dx, msg.ext_error_internal
internaldatarelocation
	jmp @F

.io_error:
	mov dx, msg.ext_error_io
internaldatarelocation
	jmp @F

.invalid:
	mov dx, msg.ext_error_invalid
internaldatarelocation
	jmp @F

.oom:
	mov dx, msg.ext_error_oom
internaldatarelocation

@@:
	push ss
	pop ds
	extcallcall putsz
	extcallcall cmd3


get_es_ext:
reloc	mov es, word [relocateddata]
linkdatarelocation extdssel
	extcallcall ispm
	jz @F
reloc	mov es, word [relocateddata]
linkdatarelocation extseg
@@:
	retn


	align 16
relocator:
	push si
	push di
	push ds
	push es

	 push es
	 pop ds
	mov cx, 0
.codelength equ $ - 2
	mov si, 0
.codesource equ $ - 2
	mov di, code
.codedestination equ $ - 2
internalcoderelocation
	rep movsb

	 push ss
	 pop es
	 push ss
	 pop ds
	mov cx, 0
.datalength equ $ - 2
	mov si, 0
.datasource equ $ - 2
	mov di, datastart
.datadestination equ $ - 2
internaldatarelocation
	rep movsb

	pop es
	mov word [relocateddata], 0
linkdatarelocation extseg_used, -4
.extseg_used equ $ - 2
	mov word [relocateddata], 0
linkdatarelocation extdata_used, -4
.extdata_used equ $ - 2

	mov cx, 0
.entry equ $ - 2
	pop ds
	pop di
	pop si
	jmp cx

	align 16
	endarea relocator


DATAOFFSET equ CODEOFFSET + code_size
	addsection DATA, follows=CODE vstart=_ELD_DATA_VSTART
datastart:

eldheader:		dd 0

msg:
.ext_error_internal:	asciz "Internal error in EXT LIB command.",13,10
.ext_error_io:		asciz "EXT LIB command got I/O error.",13,10
.ext_error_invalid:	asciz "EXT LIB: No or invalid Extension for lDebug header.",13,10
.ext_error_oom:		asciz "Out of memory in EXT LIB command.",13,10

	align 16, db 0
data_size equ $ - datastart
%assign _DATA_SIZE data_size

	usesection CODE

%include "eldlink.asm"

	align 16
code_size equ $ - code

LIBOFFSET equ DATAOFFSET + data_size
	addsection LIB, follows=DATA vstart=0
lib_start:

	strdef EMBEDDED, "extlib.eld"
%strcat PATHNAME "../../bin/", _EMBEDDED

embedded_eld:
	incbin PATHNAME
