
; Public Domain

; test run command: ; .; .

%include "lmacros3.mac"
%include "eld.mac"
%include "eldcall.mac"
%include "elddata.mac"

	cpu 8086

	addsection RELOCATEDDATA, nobits vstart=_ELD_RELOC_VSTART
relocateddata:

	addsection HEADER, start=0

	istruc ELD_HEADERX
at eldhxHeader
		; ELD executable header
	istruc ELD_HEADER
at eldhSignature,	db "ELD1"
			db 0,0,0
			db 26
at eldhCodeOffset,	dd CODEOFFSET
at eldhCodeImageLength,	dw code_size
at eldhCodeAllocLength,	dw 0
at eldhDataOffset,	dd DATAOFFSET
at eldhDataImageLength,	dw data_size
at eldhDataAllocLength,	dw total_data_size - data_size
at eldhCodeEntrypoint,	dw linker - code
at eldhReserved
at eldhExtensionSize,	dw header_extension_end - $$
	iend
at eldhxDescriptionOffset,	dd description
PUT_ELDHX_DATETIME_OFFSET
header_extension_end:
	iend

description:		asciz "Display DOS current drive or a pathname's drive."


	align 16, db 0

CODEOFFSET equ $ - $$
	addsection CODE, follows=HEADER vstart=_ELD_CODE_VSTART
%define CODEFIXUP - code + 0
code:
		; ELD instance header
	istruc ELD_INSTANCE
at eldiStartCode
at eldiEndCode
at eldiStartData
at eldiEndData
at eldiIdentifier,	fill 8, 32, db "DOSdrive"
at eldiListing,		asciz _ELD_LISTING
	iend


start:
	mov bx, es
	 push ss
	 pop es
	extcallcall skipcomma

	extcallcall InDOS
	jz @F
.error:
	extcallcall error

@@:

	extcallcall iseol?
	je .get_default

	dec si
	mov dx, msg.get
internaldatarelocation
	extcallcall isstring?
	je .get
	mov dx, msg.set
internaldatarelocation
	extcallcall isstring?
	jne .error

.set:
	extcallcall skipcomma

	dec si
	mov dx, msg.quiet
internaldatarelocation
	extcallcall isstring?
	jne @F
	not byte [quiet]
internaldatarelocation

@@:
	extcallcall skipcomma

	extcallcall iseol?
	je .set_quiet_or_error

	; extcallcall InDOS
	cmp al, al
	extcallcall yy_dos_parse_name
	extcallcall chkeol

.set_from_path:
	mov si, bx
	lodsb
	test al, al
	jz .set_quiet_or_error
	cmp byte [si], ':'
	jne .set_quiet_or_error
	extcallcall uppercase
	sub al, 'A'
	cmp al, 32
	jae .error
	mov dl, al

.setdrive:
	mov ah, 0Eh
	extcallcall _doscall
		; function 0Eh is CP/M-ish, doesn't return an error status

	mov ah, 19h
	extcallcall _doscall	; get current drive
	cmp al, dl		; matches ?
	je .end			; yes -->

	mov dx, msg.error_drive
internaldatarelocation
	mov ax, 0E26h
	extcallcall setrc
	jmp .putsz_end

.set_quiet_or_error:
	rol byte [quiet], 1
internaldatarelocation
	jc .end
	jmp .error


.get:
	extcallcall skipcomma

	extcallcall iseol?
	je .get_default

	; extcallcall InDOS
	cmp al, al
	extcallcall yy_dos_parse_name
	extcallcall chkeol

.get_from_path:
	mov si, bx
	lodsb
	test al, al
	jz .get_default
	cmp byte [si], ':'
	jne .get_default
	extcallcall uppercase
	mov dl, al
	sub dl, 'A'
	cmp dl, 32
	jae .error
	jmp @FF

.get_default:
	mov ah, 19h
	extcallcall _doscall
@@:
	add al, 'A'
@@:
	mov ah, ':'
	mov di, buffer
internaldatarelocation
	stosw
	mov al, 0
	stosb

	mov dx, buffer
internaldatarelocation
	extcallcall putsz
	mov dx, msg.linebreak
internaldatarelocation
.putsz_end:
	extcallcall putsz

.end:
	call uninstall_oneshot
	xor ax, ax
	retf


uninstall_oneshot:
	testopt [ss:relocateddata], 1
linkdatarelocation options7, -3
	jnz @F

	mov ax, word [cs:code + eldiEndCode]
internalcoderelocation
	sub ax, word [cs:code + eldiStartCode]
internalcoderelocation
	sub word [relocateddata], ax
linkdatarelocation extseg_used

	mov ax, word [cs:code + eldiEndData]
internalcoderelocation
	sub ax, word [cs:code + eldiStartData]
internalcoderelocation
	sub word [relocateddata], ax
linkdatarelocation extdata_used
@@:
	retn


DATAOFFSET equ CODEOFFSET + code_size
	addsection DATA, follows=CODE vstart=_ELD_DATA_VSTART
%define DATAFIXUP - datastart + 0
datastart:
PUT_ELD_DATETIME

quiet:		db 0

msg:
.get:			asciz "GET"
.set:			asciz "SET"
.quiet:			asciz "QUIET"
.error_drive:		db "Error setting current drive!"
.linebreak:		asciz 13,10

uinit_data: equ $

	align 16, db 0
data_size equ $ - datastart

	absolute uinit_data

	alignb 2
buffer:		resb 4

	alignb 16
uinit_data_end:

total_data_size equ $ - datastart
%assign _DATA_SIZE total_data_size

	usesection CODE

%include "eldlink.asm"

	align 16
code_size equ $ - code

