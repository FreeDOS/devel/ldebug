#! /bin/bash

# Public Domain

. cfg.sh

for file in eldcomp ldmem history reclaim instnoun \
	di dm rn rm withhdr printf ifext set variable \
	amount amismsg dx list kdisplay config x dtadisp \
	co aformat bases useparat amisoth ldmemoth dbitmap \
	dospwd doscd dosdir dosdrive instnoth path amitsrs \
	extname linfo bootdir amiscmd inject quit dosseek \
	alias dpb rdumpidx rdumpstr checksum hint hintoth \
	chstool s reserve tsc \
	extlib extpak
do
  if (( use_build_eldcomp )) || [[ "$file" != eldcomp ]]
  then
    ./makone.sh "$file" "$@"
  fi
done
