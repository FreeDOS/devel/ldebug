
; Public Domain

%include "lmacros3.mac"
%include "eld.mac"
%include "eldcall.mac"
%include "elddata.mac"

	cpu 8086

	addsection RELOCATEDDATA, nobits vstart=_ELD_RELOC_VSTART
relocateddata:

	addsection HEADER, start=0

		; ELD executable header
	istruc ELD_HEADER
at eldhSignature,	db "ELD1"
			db 0,0,0
			db 26
at eldhCodeOffset,	dd CODEOFFSET
at eldhCodeImageLength,	dw code_size
at eldhCodeAllocLength,	dw 0
at eldhDataOffset,	dd DATAOFFSET
at eldhDataImageLength,	dw data_size
at eldhDataAllocLength,	dw 0
at eldhCodeEntrypoint,	dw linker - code
at eldhReserved
	iend


	align 16, db 0

CODEOFFSET equ $ - $$
	addsection CODE, follows=HEADER vstart=_ELD_CODE_VSTART
%define CODEFIXUP - code + 0
code:
		; ELD instance header
	istruc ELD_INSTANCE
at eldiStartCode
at eldiEndCode
at eldiStartData
at eldiEndData
at eldiIdentifier,	fill 8, 32, db "ELDTEST"
at eldiListing,		asciz _ELD_LISTING
	iend

start:

	push ss
	pop es
	lodsb
.loop:
	extcallcall skipwh0
	lea dx, [si - 1]
@@:
	cmp al, 13
	je @F
	cmp al, 32
	je .dump
	cmp al, 9
	je .dump
	lodsb
	jmp @B

.dump:
	push ax
	call .do_dump
	pop ax
	jmp .loop

.do_dump:
	lea cx, [si - 1]
	sub cx, dx
	jz .retn
	push cx
	push dx
	mov cx, 1
	mov dx, startmessage
internaldatarelocation
	extcallcall puts
	pop dx
	pop cx
	extcallcall puts
	mov cx, 3
	mov dx, endmessage
internaldatarelocation
	extcallcall puts
.retn:
	retn

@@:
	call .do_dump

	mov ax, word [cs:code + eldiEndCode]
internalcoderelocation
	sub ax, word [cs:code + eldiStartCode]
internalcoderelocation
	sub word [relocateddata], ax
linkdatarelocation extseg_used

	mov ax, word [cs:code + eldiEndData]
internalcoderelocation
	sub ax, word [cs:code + eldiStartData]
internalcoderelocation
	sub word [relocateddata], ax
linkdatarelocation extdata_used

	xor ax, ax
	retf

DATAOFFSET equ CODEOFFSET + code_size
	addsection DATA, follows=CODE vstart=_ELD_DATA_VSTART
datastart:
	dw 26h, 38h, 42h, 0

startmessage:	db ">"
endmessage:	db "<",13,10

	align 16, db 0
data_size equ $ - datastart
%assign _DATA_SIZE data_size

	usesection CODE

%include "eldlink.asm"

	align 16
code_size equ $ - code
