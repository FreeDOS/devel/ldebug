
; Public Domain

%include "lmacros3.mac"
%include "eld.mac"

	cpu 8086

	istruc ELD_HEADERX
at eldhxHeader
		; ELD executable header
	istruc ELD_HEADER
at eldhExtensionSize,	dw header_extension_end - $$
	iend
at eldhxDescriptionOffset,	dd description
header_extension_end:
	iend

description:
	fill 128, 0CCh, asciz "Test ELD."
