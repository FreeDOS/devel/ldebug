#! /bin/bash

# Public Domain

. cfg.sh

file="$1"
shift

if [[ "$file" == extpak ]]
then
  origfile="$file"
  file=packlib

  if (( use_build_eld ))
  then
  "$NASM" -I "$LDEBUG_DIR" -I "$LMACROS_DIR" -I "$SCANPTAB_DIR" \
    -I "$TMP_DIR" -I "$BIN_DIR" \
    "$file".asm -w+reloc-rel -w+error=reloc-rel \
    -o "$TMP_DIR$file".bin -D_ELD_DATETIME="$use_build_eld_datetime" \
    -D_HOUDINI="$use_build_eld_houdini" \
    -l "$LST_DIR$file".lst -D_ELD_LISTING="'$file.lst'" "$@"
  fi
  if (( use_build_xld ))
  then
  "$NASM" -I "$LDEBUG_DIR" -I "$LMACROS_DIR" -I "$SCANPTAB_DIR" \
    -I "$TMP_DIR" -I "$BIN_DIR" \
    "$file".asm -w+reloc-rel -w+error=reloc-rel \
    -o "$TMP_DIR$file".xbi -D_ELD_DATETIME="$use_build_eld_datetime" \
    -D_HOUDINI="$use_build_eld_houdini" \
    -l "$LST_DIR$file".xls -D_ELD_LISTING="'$file.xls'" \
    -D_ELD_DATA_VSTART=0 -D_ELD_CODE_VSTART=0 -D_ELD_RELOC_VSTART=0 \
    -D_ELD_LINKER_PASSES=0 -D_XLD=1 "$@"
  fi

  if (( use_build_eld ))
  then
  heatshrink -v -w "$HEATSHRINK_W" -l "$HEATSHRINK_L" "$TMP_DIR$file.bin" "$TMP_DIR$file.ets" && \
    (printf "\x$(printf "%02X" "$HEATSHRINK_W")\x$(printf "%02X" "$HEATSHRINK_L")";
    cat "$TMP_DIR$file.ets") > "$TMP_DIR$file.ehs"
  fi
  if (( use_build_xld ))
  then
  heatshrink -v -w "$HEATSHRINK_W" -l "$HEATSHRINK_L" "$TMP_DIR$file.xbi" "$TMP_DIR$file.xts" && \
    (printf "\x$(printf "%02X" "$HEATSHRINK_W")\x$(printf "%02X" "$HEATSHRINK_L")";
    cat "$TMP_DIR$file.xts") > "$TMP_DIR$file.xhs"
  fi
  file="$origfile"
fi

if (( use_build_eld ))
then
  "$NASM" -I "$LDEBUG_DIR" -I "$LMACROS_DIR" -I "$SCANPTAB_DIR" \
    -I "$TMP_DIR" -I "$BIN_DIR" \
    "$file".asm -w+reloc-rel -w+error=reloc-rel \
    -o "$BIN_DIR$file".eld -D_ELD_DATETIME="$use_build_eld_datetime" \
    -D_HOUDINI="$use_build_eld_houdini" \
    -l "$LST_DIR$file".lst -D_ELD_LISTING="'$file.lst'" "$@"
fi
if (( use_build_xld ))
then
  "$NASM" -I "$LDEBUG_DIR" -I "$LMACROS_DIR" -I "$SCANPTAB_DIR" \
    -I "$TMP_DIR" -I "$BIN_DIR" \
    "$file".asm -w+reloc-rel -w+error=reloc-rel \
    -o "$BIN_DIR$file".xld -D_ELD_DATETIME="$use_build_eld_datetime" \
    -D_HOUDINI="$use_build_eld_houdini" \
    -l "$LST_DIR$file".xls -D_ELD_LISTING="'$file.xls'" \
    -D_ELD_DATA_VSTART=0 -D_ELD_CODE_VSTART=0 -D_ELD_RELOC_VSTART=0 \
    -D_ELD_LINKER_PASSES=0 -D_XLD=1 "$@"
fi

if (( use_build_eld ))
then
if [[ "$file" == extpak && -f "$BIN_DIR$file.eld" ]]
then
  truncate -c \
    -s "$(tail --bytes=4 "$BIN_DIR$file.eld" | od -An --endian=little -t u4)" \
    "$BIN_DIR$file.eld"
fi
fi

if (( use_build_xld ))
then
if [[ "$file" == extpak && -f "$BIN_DIR$file.xld" ]]
then
  truncate -c \
    -s "$(tail --bytes=4 "$BIN_DIR$file.xld" | od -An --endian=little -t u4)" \
    "$BIN_DIR$file.xld"
fi
fi
