
; Public Domain

%include "lmacros3.mac"
%include "eld.mac"
%include "eldcall.mac"
%include "elddata.mac"

%assign _DEBUG 1
%assign _CATCHINT0C 0
%assign _CATCHINT0D 0
%assign _CATCHINTFAULTCOND 0
%assign _DEBUG_COND 1
%assign _DELAY_BEFORE_BP 0
%assign _DHIGHLIGHT 0
%assign _GETLINEHIGHLIGHT 0
%assign _IMMASM 0
%assign _INPUT_FILE_BOOT 0
%assign _INPUT_FILE_HANDLES 0
%assign _MS_0RANGE_COMPAT 0
%assign _PM 0
%assign _RH 0
%assign _SYMBOLIC 0
%assign _VXCHG 0
%include "options.mac"

%include "iniload.mac"

	cpu 8086

	addsection RELOCATEDDATA, nobits vstart=_ELD_RELOC_VSTART
relocateddata:

	addsection HEADER, start=0

		; ELD executable header
	istruc ELD_HEADER
at eldhSignature,	db "ELD1"
			db 0,0,0
			db 26
at eldhCodeOffset,	dd CODEOFFSET
at eldhCodeImageLength,	dw code_size
at eldhCodeAllocLength,	dw 0
at eldhDataOffset,	dd DATAOFFSET
at eldhDataImageLength,	dw data_size
at eldhDataAllocLength,	dw 0
at eldhCodeEntrypoint,	dw linker - code
at eldhReserved
	iend


	align 16, db 0

CODEOFFSET equ $ - $$
	addsection CODE, follows=HEADER vstart=_ELD_CODE_VSTART
%define CODEFIXUP - code + 0
code:
		; ELD instance header
	istruc ELD_INSTANCE
at eldiStartCode
at eldiEndCode
at eldiStartData
at eldiEndData
at eldiIdentifier,	fill 8, 32, db "ELDTEST"
at eldiListing,		asciz _ELD_LISTING
	iend

start:

	push ss
	pop es

	mov di, relocateddata
linkdatarelocation line_out
	mov si, msg.offset
internaldatarelocation
	extcallcall copy_single_counted_string

	mov bx, relocateddata
linkdatarelocation load_data_lowest	; make yy_boot_get and yy_boot_update no-ops
	testopt [relocateddata], nodosloaded
linkdatarelocation internalflags, -3
	jnz @F
	mov bx, [relocateddata]
linkdatarelocation ext_handle
	extcallcall InDOS
	jz @F
	extcallcall error
@@:

	mov ax, 4201h
	mov cx, ((- DATAOFFSET - data_size) >> 16) & 0FFFFh
	mov dx, (- DATAOFFSET - data_size) & 0FFFFh

	testopt [relocateddata], nodosloaded
linkdatarelocation internalflags, -3
	jz @F
	extcallcall yy_boot_seek_current.bx

	mov ax, word [relocateddata - LOADDATA2 + ldCurrentSeek]
linkdatarelocation load_data
	mov dx, word [relocateddata - LOADDATA2 + ldCurrentSeek + 2]
linkdatarelocation load_data
	jmp @FF

@@:
	extcallcall _doscall
@@:

	xchg dx, ax
	extcallcall hexword
	mov al, '_'
	stosb
	xchg dx, ax
	extcallcall hexword

	extcallcall putsline_crlf

	mov ax, word [cs:code + eldiEndCode]
internalcoderelocation
	sub ax, word [cs:code + eldiStartCode]
internalcoderelocation
	sub word [relocateddata], ax
linkdatarelocation extseg_used

	mov ax, word [cs:code + eldiEndData]
internalcoderelocation
	sub ax, word [cs:code + eldiStartData]
internalcoderelocation
	sub word [relocateddata], ax
linkdatarelocation extdata_used

	xor ax, ax
	retf

DATAOFFSET equ CODEOFFSET + code_size
	addsection DATA, follows=CODE vstart=_ELD_DATA_VSTART
datastart:

msg:
.offset:	counted "ELD loaded from file offset "

	align 16, db 0
data_size equ $ - datastart
%assign _DATA_SIZE data_size

	usesection CODE

%include "eldlink.asm"

	align 16
code_size equ $ - code
