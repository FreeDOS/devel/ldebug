
; Public Domain

; test install command: install; .; history uninstall
; test run command: ; .; .

%include "lmacros3.mac"
%include "eld.mac"
%include "eldcall.mac"
%include "elddata.mac"

	cpu 8086

	addsection RELOCATEDDATA, nobits vstart=_ELD_RELOC_VSTART
relocateddata:

	addsection HEADER, start=0

	istruc ELD_HEADERX
at eldhxHeader
		; ELD executable header
	istruc ELD_HEADER
at eldhSignature,	db "ELD1"
			db 0,0,0
			db 26
at eldhCodeOffset,	dd CODEOFFSET
at eldhCodeImageLength,	dw code_size
at eldhCodeAllocLength,	dw 0
at eldhDataOffset,	dd DATAOFFSET
at eldhDataImageLength,	dw data_size
at eldhDataAllocLength,	dw total_data_size - data_size
at eldhCodeEntrypoint,	dw linker - code
at eldhReserved
at eldhExtensionSize,	dw header_extension_end - $$
	iend
at eldhxDescriptionOffset,	dd description
at eldhxHelpOffset,		dd DATAOFFSET + msg.help - datastart
PUT_ELDHX_DATETIME_OFFSET
header_extension_end:
	iend

description:		asciz "Provide HISTORY utility."


	align 16, db 0

CODEOFFSET equ $ - $$
	addsection CODE, follows=HEADER vstart=_ELD_CODE_VSTART
%define CODEFIXUP - code + 0
code:
		; ELD instance header
	istruc ELD_INSTANCE
at eldiStartCode
at eldiEndCode
at eldiStartData
at eldiEndData
at eldiIdentifier,	fill 8, 32, db "HISTORY"
at eldiListing,		asciz _ELD_LISTING
	iend


command:
	jmp strict short .entry
.chain:
	extcall cmd3_not_ext, required	; must NOT be extcallcall
	times 10 - ($ - command) nop
.entry:
	push si
	cmp al, '-'
	jne @F
	extcallcall skipcomma
@@:
	dec si

	mov dx, msg.history
internaldatarelocation
	extcallcall isstring?
	je .ours
	pop si
	dec si
	lodsb
	jmp .chain

.ours:
	pop ax
	extcallcall skipcomma
	dec si
reloc2	mov word [relocateddata], relocateddata
linkdatarelocation lastcmd, -4
linkdatarelocation dmycmd
	mov dx, relocateddata
linkdatarelocation msg.uninstall
	extcallcall isstring?
	je uninstall
	mov dx, msg.clear
internaldatarelocation
	extcallcall isstring?
	je clear
	mov dx, msg.show
internaldatarelocation
	extcallcall isstring?
	; je
	lodsb
	extcallcall chkeol
	call run_show
@@:
	extcallcall cmd3

clear:
	call run_clear
	jmp @B

uninstall:
	lodsb
	extcallcall chkeol

	mov es, word [relocateddata]
linkdatarelocation extdssel
	extcallcall ispm
	jz @F
	mov es, word [relocateddata]
linkdatarelocation extseg
@@:
	push es
	pop ds
	xor bx, bx		; = 0 (no prior, modify ext_command_handler)
	mov di, command		; di -> us
internalcoderelocation
	mov si, word [ss:relocateddata]
linkdatarelocation ext_command_handler
				; si -> first
	test si, si		; none installed ?
	jz .error		; error -->

.loop:
	cmp di, si		; found ?
	je .bx			; yes, use bx -->
	mov bx, si		; bx -> prior handler
	lodsw			; skip entrypoint jmp strict short
	lodsb			; get first byte of chainer
	cmp al, 0E9h		; expecting jmp near ?
	jne .error		; no, error -->
	lodsw			; get rel16 displacement
	add si, ax		; -> next handler
	jmp .loop

.bx:
	test bx, bx		; any prior ?
	jnz .bxnz		; yes -->
	scasw			; skip entrypoint jmp strict short
	cmp byte [di], 0E8h	; is it a call to cmd3_not_ext ?
	jne @F			; no -->
				; yes, reset ext_command_handler to zero
.setbx:
	mov word [ss:relocateddata], bx
linkdatarelocation ext_command_handler
	jmp .done

@@:
	cmp byte [di], 0E9h	; validate
	jne .error		; failure -->
	inc di			; -> rel16 displacement
	mov bx, word [di]	; get displacement
	scasw			; -> after jmp near
	add bx, di		; -> next handler
	jmp .setbx		; set ext_command_handler to next

.bxnz:
	mov si, bx		; -> prior handler with us as downlink
	xchg di, si		; si -> ours, di -> prior
	cmpsw			; skip entrypoint jmp strict short
	movsb			; copy 0E8h/0E9h
	lodsw			; ax = near rel16 displacement
	add ax, si		; add in our base (= absolute offset)
	sub ax, di
	dec ax
	dec ax			; subtract new base (= relative displacement)
	stosw			; store new rel16 displacement
	movsw			; jmp strict short
	movsw			; linkcall target
	movsb			; trailer
.done:
	clropt [code + eldiFlags], eldifResident
internalcoderelocation -3	; mark block as free
	mov dx, msg.uninstall_done
internaldatarelocation
@@:
	push ss
	pop ds
	extcallcall putsz
	extcallcall cmd3	; return

.error:
	mov ax, 0E01h
	extcallcall setrc
	mov dx, msg.uninstall_error
internaldatarelocation
	jmp @B


run_clear:
	call get_history_first_last
	mov ax, word [bx]
	xchg dx, bx
	mov word [bx], ax
	retn


run_show:
	houdini
	push ss
	pop es
	mov byte [history_data_entry], 0
internaldatarelocation -3
	mov ax, relocateddata
linkdatarelocation historyformat, -2, optional
	test ax, ax
	jnz @F
	mov ax, 0E3Dh
	extcallcall setrc
	mov dx, msg.history_none
internaldatarelocation
.putsz_end:
	extcallcall putsz
	jmp .end

@@:
	cmp ax, 2
	jb .in_data_entry
	je .in_separate

	mov ax, 0E3Eh
	extcallcall setrc
	mov dx, msg.history_unknown
internaldatarelocation
	jmp .putsz_end

.in_data_entry:
	not byte [history_data_entry]
internaldatarelocation
.in_separate:
	call get_history_first_last
	mov si, dx
	xchg si, bx
	mov si, [si]
	mov bx, [bx]
houdini
	dec si
	dec si
	xor dx, dx
	rol byte [history_data_entry], 1
internaldatarelocation
	jnc .not_entry
	mov dx, relocateddata
linkdatarelocation historybuffer, -2, optional
	test dx, dx
	jnz @F
	call history_missing
@@:
.not_entry:

.loop:
	mov di, relocateddata
linkdatarelocation line_out
	cmp si, bx
	jb .done

	rol byte [history_data_entry], 1
internaldatarelocation
	jc .not_seg
	push bx
	mov bx, relocateddata
linkdatarelocation history.segorsel, -2, optional
	test bx, bx
	jz history_missing	; bx on stack discarded
	mov ds, word [bx]
	pop bx
.not_seg:
	; extcallcall decword
	lodsw			; ax -> behind entry
	add ax, dx
	mov cx, [si]		; cx -> behind prior entry, start of this
	add cx, dx
	xchg ax, si		; si -> behind
	sub si, cx		; si = length
	xchg si, cx		; cx = length, si -> text
	rep movsb
	xchg ax, si		; restore si -> next high array entry
	sub si, 4
	push ss
	pop ds
	 push dx
	 push bx
	extcallcall putsline_crlf
	 pop bx
	 pop dx
	jmp .loop

.done:
.end:
	retn


get_history_first_last:
	mov bx, relocateddata
linkdatarelocation history.first, -2, optional
	mov dx, relocateddata
linkdatarelocation history.last, -2, optional
	test bx, bx
	jz @F
	test dx, dx
	jnz @FF

@@:
history_missing:
	mov ax, 0E3Fh
	extcallcall setrc
	mov dx, msg.history_missing
internaldatarelocation
	extcallcall putsz
	pop cx
@@:
	retn


	eldcall_dump_callcall ELDCALL_CALLCALL_LIST

endinstalled equ ($ + CODEFIXUP + 15) & ~15


start:
	mov bx, es
	 push ss
	 pop es
	call skipcomma
	dec si
	mov dx, relocateddata
linkdatarelocation msg.install
	call isstring?
	je install
	mov dx, msg.show
internaldatarelocation
	call isstring?
	je .show
	mov dx, msg.clear
internaldatarelocation
	call isstring?
	je .clear

	lodsb
	call chkeol
	mov dx, msg.help
internaldatarelocation
	call putsz
@@:
	call uninstall_oneshot
	xor ax, ax
	retf

.show:
	call run_show
	jmp @B

.clear:
	call run_clear
	jmp @B


uninstall_oneshot:
	testopt [ss:relocateddata], 1
linkdatarelocation options7, -3
	jnz @F

	mov ax, word [cs:code + eldiEndCode]
internalcoderelocation
	sub ax, word [cs:code + eldiStartCode]
internalcoderelocation
	sub word [relocateddata], ax
linkdatarelocation extseg_used

	mov ax, word [cs:code + eldiEndData]
internalcoderelocation
	sub ax, word [cs:code + eldiStartData]
internalcoderelocation
	sub word [relocateddata], ax
linkdatarelocation extdata_used
@@:
	retn


DATAOFFSET equ CODEOFFSET + code_size
	addsection DATA, follows=CODE vstart=_ELD_DATA_VSTART
%define DATAFIXUP - datastart + 0
datastart:
PUT_ELD_DATETIME

msg:
.history:		asciz "HISTORY"
.clear:			asciz "CLEAR"
.show:			asciz "SHOW"
.uninstall_done:	db "HISTORY utility uninstalled."
%if _ELD_RECLAIM_HINT
			db " (Don't forget to use reclaim.eld)"
%endif
			asciz 13,10
.uninstall_error:	asciz "HISTORY utility unable to uninstall!",13,10
.history_none:		asciz 13,10,"No history format link found.",13,10
.history_unknown:	asciz 13,10,"Unknown history format.",13,10
.history_missing:	asciz "History last or first displacement or segment missing.",13,10

uinit_data: equ $

.help:		db "HISTORY utility. Available commands:",13,10
		db 13,10
		db "SHOW",9,9,"Show all of history",13,10
		db "CLEAR",9,9,"Clear history",13,10
		db "INSTALL",9,9,"(ELD load only) Install utility resident",13,10
		db "UNINSTALL",9,"(Resident only) Uninstall resident utility",13,10
		asciz
.installed:		asciz "HISTORY utility installed.",13,10

	align 16, db 0
init_data_end:
data_size equ $ - datastart
transient_data_size equ data_size

	absolute uinit_data
history_data_entry:
	resb 1

	alignb 16
uinit_data_end:
resident_data_end:
resident_data_size equ resident_data_end - datastart

%if uinit_data_end >= init_data_end
 total_data_size equ $ - datastart
%else
 total_data_size equ init_data_end - datastart
%endif
%assign _DATA_SIZE total_data_size


	usesection CODE

install:
	lodsb
	extcall chkeol

	houdini
	mov es, bx		; => ext seg (writable)

	mov ax, endresident - endinstalled
	sub word [es:code + eldiEndCode], ax
internalcoderelocation		; adjust size
	sub word [relocateddata], ax
linkdatarelocation extseg_used	; adjust size

%if (transient_data_size - resident_data_size) > 0
	mov ax, transient_data_size - resident_data_size
	sub word [es:code + eldiEndData], ax
internalcoderelocation		; adjust size
	sub word [relocateddata], ax
linkdatarelocation extdata_used	; adjust size
%endif

	mov bx, word [relocateddata]
linkdatarelocation ext_command_handler
				; -> prior
	mov di, command		; -> our handler
internalcoderelocation
	test bx, bx		; installing as first ?
	jz .only_first		; yes, simple --> (leave as extcall cmd3_not_ext)
	scasw			; skip entrypoint jmp strict short
	mov al, 0E9h		; = jmp near opcode
	stosb			; store
	xchg ax, bx		; ax -> next handler
	sub ax, di
	dec ax
	dec ax			; ax = ax - (di + 2)
	stosw			; store our downlink as rel16 displacement

.only_first:
	setopt [es:code + eldiFlags], eldifResident
internalcoderelocation -3	; mark block as resident
	mov word [relocateddata], command
linkdatarelocation ext_command_handler, -4
internalcoderelocation		; -> our entrypoint

	testopt [relocateddata], 4
linkdatarelocation options7, -3
	jnz @F
	mov dx, msg.installed
internaldatarelocation
	call putsz
@@:
	xor ax, ax
	retf


%include "eldlink.asm"

	align 16
code_size equ $ - code
