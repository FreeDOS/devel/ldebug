
%if 0

lDebug string keyword comparison

Copyright (C) 1995-2003 Paul Vojta
Copyright (C) 2008-2023 E. C. Masloch

Usage of the works is permitted provided that this
instrument is retained with the works, so that any entity
that uses the works is notified of this instrument.

DISCLAIMER: THE WORKS ARE WITHOUT WARRANTY.

%endif


		; Check for given string (cap-insensitive)
		;
		; INP:	ds:si -> input string to check (either cap),
		;	 terminated by CR (13), NUL, semicolon, space,
		;	 tab, dot, comma, equals, colon, [, ], (, or )
		;	es:dx -> ASCIZ string to check (all-caps)
		; OUT:	Iff string matches,
		;	 ZR, NC
		;	 si -> at separator that terminates the keyword
		;	else,
		;	 NZ, CY
		;	 si = input si
		; STT:	ds = es = ss
		; CHG:	dx, al
%[PREFIX]isstring?:
	push si
	xchg dx, di
.loop:
	lodsb
	call %[PREFIX]uppercase
	scasb
	jne .mismatch
	test al, al		; NC
	jne .loop
				; ZR
	jmp .matched_zr_nc

.mismatch:
	call %[PREFIX]iseol?
	je .checkend
	cmp al, 32
	je .checkend
	cmp al, 9
	je .checkend
	cmp al, '.'
	je .checkend
	cmp al, ','
	je .checkend
	cmp al, '='
	je .checkend
	cmp al, ':'
	je .checkend
	cmp al, '['
	je .checkend
	cmp al, ']'
	je .checkend
	cmp al, '('
	je .checkend
	cmp al, ')'
	je .checkend
%ifidni PREFIX, init_
 %if ELD
	cmp al, byte [ss:relocateddata]
linkdatarelocation swch1
 %else
	cmp al, byte [ss:swch1]
 %endif
	je .checkend
%endif
.ret_nz:	; NZ
	pop si
	stc			; CY
.ret:
	xchg dx, di
	retn

.checkend:
	cmp byte [es:di - 1], 0
	jne .ret_nz
				; ZR, NC
.matched_zr_nc:	; ZR, NC
	pop di			; (discard)
	lea si, [si - 1]	; -> separator (preserve ZR)
	jmp .ret


%[PREFIX]uppercase: section_of_function
	cmp al, 'a'
	jb .ret
	cmp al, 'z'
	ja .ret
	and al, TOUPPER
.ret:
	retn

%ifempty PREFIX
iseol?_or_then:
 %if _INPUT_FILE_HANDLES || _INPUT_FILE_BOOT
	testopt [internalflags3], dif3_in_if
	jz iseol?
	mov dx, msg.then
	dec si
	call isstring?
	je iseol?.ret
	lodsb
 %endif
%endif

%[PREFIX]iseol?:
	cmp al, ';'
	je .ret
.notsemicolon:
	cmp al, 13		; this *IS* iseol?
	je .ret
	cmp al, 0
.ret:
	retn


		; Skip blanks, then an optional comma, and then more blanks
		;
		; INP:	ds:si -> first character
		; OUT:	al = first non-blank character behind
		;	ds:si -> character behind the first non-blank behind
		;	NC
		; STK:	3 word
%[PREFIX]skipcomma: section_of_function
	lodsb

		; Same as above but we already have the first character in al
%[PREFIX]skipcomm0: section_of_function
	call %[PREFIX]skipwh0
	cmp al, ','
	jne .return		; if no comma
	push si
	call %[PREFIX]skipwhite
	call %[PREFIX]iseol?
	jne .noteol		; if not end of line
	pop si
	mov al, ','
	retn
.noteol:
	add sp, byte 2		; pop si into nowhere
.return:
	retn


		; Skip blanks, then an optional equals sign, then more blanks
%[PREFIX]skipequals: section_of_function
	lodsb
%[PREFIX]skipequ0:
	call %[PREFIX]skipwh0
	cmp al, '='
	jne .return
	call %[PREFIX]skipwhite
.return:
	retn


		; Skip alphabetic characters, and then white space
		;
		; INP:	ds:si-> first character
		; OUT:	al = first non-blank character behind alphabetic characters
		;	ds:si-> character behind the first non-blank behind alpha.
		;	NC
%[PREFIX]skipalpha:
.:
	lodsb
	and al, TOUPPER
	sub al, 'A'
	cmp al, 'Z'-'A'
	jbe .
	dec si

		; Skip blanks and tabs
		;
		; INP:	ds:si-> first character
		; OUT:	al = first non-blank character
		;	ds:si-> character behind the first non-blank
		;	NC
		; CHG:	-
		; STK:	1 word
%[PREFIX]skipwhite: section_of_function
	lodsb

		; Same as above, but first character in al
		;
		; INP:	al = first character
		;	ds:si-> next character
		; OUT:	al = first non-blank character
		;	ds:si-> character behind the first non-blank
		;	NC
		; CHG:	-
		; STK:	1 word
%[PREFIX]skipwh0: section_of_function
	cmp al, 32
	je %[PREFIX]skipwhite
	cmp al, 9
	je %[PREFIX]skipwhite
	clc
	retn
