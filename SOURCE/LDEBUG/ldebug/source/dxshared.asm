
%if 0

lDebug DX command - Dump extended memory

Copyright (C) 2008-2023 E. C. Masloch

Usage of the works is permitted provided that this
instrument is retained with the works, so that any entity
that uses the works is notified of this instrument.

DISCLAIMER: THE WORKS ARE WITHOUT WARRANTY.

%endif

		; DX command
[cpu 386]
extmem:
%if _DTOP
	call ddd.handletop
%else
	extcallcall skipwhite
%endif
	mov dx, word [x_addr+0]
internaldatarelocation
	mov bx, word [x_addr+2]
internaldatarelocation
	extcallcall iseol?
	je extmem_1
	extcallcall getdword	; get linear address into bx:dx
	extcallcall chkeol		; expect end of line here
extmem_1:
%if ELD
	mov word [relocateddata], extmem
linkdatarelocation lastcmd_transfer_ext_address, -4
internalcoderelocation
	mov word [relocateddata], relocateddata
linkdatarelocation lastcmd, -4
linkdatarelocation lastcmd_transfer_ext_entry
%else
	mov word [lastcmd], extmem
%endif
	push bx
	push dx
	pop ebp

	mov di, relocated(stack); create a GDT for Int15.87
linkdatarelocation stack
	mov si, di		; es:si -> GDT

		; null descriptor, one descriptor for use by handler
	xor ax, ax
	mov cx, 8
	rep stosw

		; build a descriptor for source
	mov ax, 007Fh
	stosw			; 127 bytes limit
	mov ax, dx
	stosw
	mov al, bl
	stosb			; 24 bits of base
	mov ax, 0093h
	stosw			; descriptor type
	mov al, bh
	stosb			; high 8 bits of base

		; build a descriptor for destination
	mov ax, 007Fh
	stosw			; 127 bytes limit
	mov eax, relocated(line_out) + 128
linkdatarelocation line_out, -4
	movzx ebx, word [relocated(pspdbg)]
linkdatarelocation pspdbg
	shl ebx, 4
	add eax, ebx		; eax = flat address of line_out + 128
	stosw
	shr eax, 16
	stosb			; 24 bits of base
	mov bl, ah		; bl = high 8 bits of base
	mov ax, 0093h
	stosw			; descriptor type
	xchg ax, bx		; al = bl, clobber bx
	stosb			; high 8 bits of base

		; two more descriptors for use by handler
	mov cl, 8		; cx = 8
	xor ax, ax
	rep stosw

%if _PM
	extcallcall ispm
%endif
				; es:si -> GDT
	mov cl, 40h		; cx = 128 bytes (64 words)
	mov ah, 87h
%if _PM
	jnz extmem_rm
	push word [relocated(pspdbg)]
linkdatarelocation pspdbg
	push 15h
	stc
 %if ELD
	extcall intcall_ext_return_es, PM required	; must NOT be extcallcall
	pop si
	pop si			; (discard parameters)
 %else
	call intcall
 %endif
	jmp short i15ok
%endif
extmem_rm:
	int 15h
i15ok:
	jc extmem_exit
	mov si, relocated(line_out) + 128
linkdatarelocation line_out
	mov ch, 8h
nexti15l:
	extcallcall handle_serial_flags_ctrl_c
	mov di, relocated(line_out)
linkdatarelocation line_out
	mov eax, ebp
	extcallcall hexword_high
	extcallcall hexword
	mov ax, 32<<8|32
	stosw
	mov bx, relocated(line_out) + 10 + 3*16
linkdatarelocation line_out
	mov cl, 10h
nexti15b:
	lodsb
	extcallcall dd_store
	mov al, 32
	stosb
	dec cl
	jnz nexti15b
	mov byte [di-(8*3+1)], '-'	; display a '-' after 8 bytes
	add di, 16
	push cx
	extcallcall putsline_crlf
	pop cx
	add ebp, byte 10h
	dec ch
	jnz nexti15l
	mov dword [x_addr], ebp
internaldatarelocation
extmem_exit:
	retn
__CPU__
