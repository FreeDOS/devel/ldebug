
%if 0

lDebug RM command - MMX Register dump

Copyright (C) 2008-2023 E. C. Masloch

Usage of the works is permitted provided that this
instrument is retained with the works, so that any entity
that uses the works is notified of this instrument.

DISCLAIMER: THE WORKS ARE WITHOUT WARRANTY.

%endif


subcpu 586
dumpregsMMX:
	dec si
	extcallcall get_length_keyword
	lodsb
	cmp cl, 4			; paras, pages, KiB/MiB/GiB ?
	jae error			; all invalid, only up to qwords -->
	mov dh, 1
	shl dh, cl
	extcallcall chkeol

%if _RM_AUX
	extcallcall guard_auxbuff
	mov ds, word [relocated(auxbuff_segorsel)]
linkdatarelocation auxbuff_segorsel	; => auxbuff
	o32
	fnsave [0]
	mov si, 7*4
%else
	o32
	fnsave [rn_rm_buffer]
internaldatarelocation
	mov si, 7 * 4 + rn_rm_buffer
internaldatarelocation
%endif
	mov cl, '0'
	mov di, relocated(line_out)
linkdatarelocation line_out
.nextreg:
%if _RM_AUX
	mov ds, word [ss:relocated(auxbuff_segorsel)]
linkdatarelocation auxbuff_segorsel	; => auxbuff
%endif
	mov ax, "MM"
	stosw
	mov al, cl
	mov ah, '='
	stosw
	push cx
	mov dl, 8
	mov bh, 0
	mov bl, dh		; = how many bytes per item
.nextitem:
	add si, bx		; -> behind item of data
	db __TEST_IMM8		; (skip dec dx)
.nextbyte:
	dec dx			; (if branched here) dl always nonzero after this
	dec si			; -> next byte (less significant than prior)
	mov al, byte [si]
	extcallcall hexbyte		; write this byte
	cmp bl, 5		; wrote 4 bytes of qword yet ?
	mov al, ':'
	je .oddbyte		; yes, add a colon
	cmp bl, 1		; within item ?
	ja .initem		; yes, no more separators -->
	mov al, 32
	test dl, 1
	jz .oddbyte
	mov al, '-'
.oddbyte:
	stosb
.initem:
	dec bx			; count down in item index
	jnz .nextbyte		; not yet done with item -->
	mov bl, dh		; reset bx = number of bytes per item
	add si, bx		; -> behind item
	dec dl			; qword done ?
	jnz .nextitem		; not yet -->

	dec di			; -> at last separator
	mov ax, 32<<8|32
	stosw
	add si, byte 2
	pop cx
	test cl, 1
	jz .oddreg
	push cx
	push dx
%if _RM_AUX
	 push ss
	 pop ds				; ds = es = ss
%endif
	extcallcall trimputs
	pop dx
	pop cx
	mov di, relocated(line_out)
linkdatarelocation line_out
.oddreg:
	inc cl
	cmp cl, '8'
	jne .nextreg
%if _RM_AUX
	mov ds, word [ss:relocated(auxbuff_segorsel)]
linkdatarelocation auxbuff_segorsel	; => auxbuff
	o32
	fldenv [0]
	 push ss
	 pop ds				; ds = es = ss
%else
	o32
	fldenv [rn_rm_buffer]
internaldatarelocation
%endif
	retn
subcpureset
