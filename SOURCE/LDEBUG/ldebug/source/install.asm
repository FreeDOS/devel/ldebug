
%if 0

lDebug INSTALL commands

Copyright (C) 2008-2022 E. C. Masloch

Usage of the works is permitted provided that this
instrument is retained with the works, so that any entity
that uses the works is notified of this instrument.

DISCLAIMER: THE WORKS ARE WITHOUT WARRANTY.

%endif


uninstall:
	mov cx, install.clear
	jmp install.common

install:
	mov cx, .toggle
	call skipcomm0
	dec si
	mov dx, msg.toggle
	call isstring?
	lodsb
	je .common
	mov cx, .set

.common:
	call skipcomm0
	push si
.loopcheck:
	dec si
	call checkinstallflag	; valid command ? (errors out if no)
	call skipcomma
	call iseol?
	jne .loopcheck

	pop si
.loopdo:
	dec si
	call checkinstallflag	; re-detect which it is, cannot fail
	mov dx, word [bx + ifDescription]
	mov di, word [bx + ifOptions]
	mov ax, word [bx + ifValue]
	test di, di		; normal with options pointer ?
	jnz @F			; yes -->
	call ax			; call ifValue function pointer
	db __TEST_IMM16		; (skip call cx)
@@:
	call cx			; call install.set or install.clear
	call skipcomma
	call iseol?
	jne .loopdo
	retn


		; OUT:	ZR if not reverse
.isreverse:
		; (n & (n - 1)) != 0
	push bx
	mov bx, ax
	dec bx
	and bx, ax
	pop bx
	retn


.toggle:
	call .isreverse
	jz .togglenormal

.togglereverse:
	not ax
	xor word [di], ax
	test word [di], ax
	jz .settry
	jmp @F

.togglenormal:
	xor word [di], ax
	test word [di], ax
	jnz .settry
@@:
	jmp .cleartry


.set:
	call .isreverse
	jz .setnormal

.setreverse:
	not ax
	test word [di], ax	; already clear ?
	jz @F			; yes -->
	not ax
	and word [di], ax	; clear flag
	jmp .settry


.setnormal:
	test word [di], ax	; already set ?
	jnz @F			; yes -->
	or word [di], ax	; set flag
.settry:
	clc			; NC = setting
	jmp near [bx + ifTrying]; call try handler

@@:
	call putsz
	mov dx, msg.alreadyenabled
.putsz:
	jmp putsz


.clear:
	call .isreverse
	jz .clearnormal

.clearreverse:
	not ax
	test word [di], ax	; already set ?
	jnz @F
	or word [di], ax	; set flag
	jmp .cleartry

.clearnormal:
	test word [di], ax	; already clear ?
	jz @F			; yes -->
	not ax			; get mask value
	and word [di], ax	; clear flag
.cleartry:
	stc			; CY = clearing
	jmp near [bx + ifTrying]; call try handler

@@:
	call putsz
	mov dx, msg.alreadydisabled
	jmp .putsz


install_trying:
	jc .clear
.set:
	call putsz
	mov dx, msg.tryenable
	jmp .putsz

.clear:
	call putsz
	mov dx, msg.trydisable
.putsz:
	jmp install.putsz

checkinstallflag:
	mov bx, installflags - INSTALLFLAG_size
@@:
	add bx, INSTALLFLAG_size
	mov dx, word [bx + ifKeyword]
	test dx, dx
	jz .error
	call isstring?
	jne @B
	retn

.error:
	jmp error


%if _AREAS && _AREAS_HOOK_CLIENT
install_areas:
	cmp cx, install.clear
	je .uninstall
	cmp cx, install.toggle
	jne .install

.toggle:
	cmp word [areas_struc + areastrucEntry], 0CBF9h
	je .install
.uninstall:
	jmp uninstall_areas

.install:
	push si
	push cx
	cmp word [areas_struc + areastrucEntry], 0CBF9h
	je @F
	mov dx, msg.areasalreadyinstalled
	mov ax, 0703h
.setrc_putsz_ret:
	call setrc
.putsz_ret:
	call putsz
	pop cx
	pop si
	retn

@@:
	call findinstalleddebugger
				; CHG: si, di, es, ax, cx, dx
	push ss
	pop es
	jnc @F

	mov dx, msg.areasnodebuggerfound
	mov ax, 0704h
	jmp .setrc_putsz_ret

@@:
	mov al, 33h
	mov bx, areas_struc
%if _PM
	mov dx, word [pspdbg]
	call call_int2D
%else
	mov dx, ss
	int 2Dh			; install areas
%endif
	push ss
	pop ds
	push ss
	pop es

	test al, al
	jz .not_supported
	cmp al, -1
	je .installed
	mov di, msg.areasnotinstalled.code
	call hexbyte
	mov dx, msg.areasnotinstalled
	mov ax, 0702h
	jmp .setrc_putsz_ret

.not_supported:
	mov dx, msg.areasnotsupported
	mov ax, 0701h
	jmp .setrc_putsz_ret

.installed:
	mov dx, msg.areasinstalled
	jmp .putsz_ret


uninstall_areas:
	push si
		; cx != 0
	push cx
	cmp word [areas_struc + areastrucEntry], 0CBF9h
	jne .is_installed
	mov dx, msg.areasalreadyuninstalled
	mov ax, 0705h
.setrc_putsz_ret:
	jmp install_areas.setrc_putsz_ret
.putsz_ret:
	jmp install_areas.putsz_ret


.qq_entry:
	push si
	xor cx, cx
	push cx
	cmp word [areas_struc + areastrucEntry], 0CBF9h
	jne .is_installed
				; (NC)
.ret:
	pop cx
	pop si
	retn

.ret_qq:
	cmp al, 0FFh
	jb .ret			; --> (CY)
	cmp word [areas_struc + areastrucEntry], 0CBF9h
	je .ret			; --> (NC)
	stc
	jmp .ret


.is_installed:
	mov al, 0
%if _PM
	call ispm
	jnz .86m
subcpu 286
.pm:
	lframe none
	lvar 32h, 86m_call_struc
	lenter
	mov word [bp + ?86m_call_struc +1Ch], ax	; eax
	xor ax, ax
	mov word [bp + ?86m_call_struc +20h], ax	; flags
	mov word [bp + ?86m_call_struc +0Ch + 2], ax
	mov word [bp + ?86m_call_struc +0Ch], ax
	mov word [bp + ?86m_call_struc +2Eh], ax	; sp
	mov word [bp + ?86m_call_struc +30h], ax	; ss
	mov word [bp + ?86m_call_struc +22h], ax	; es
	mov word [bp + ?86m_call_struc +24h], ax	; ds
	mov ax, word [pspdbg]
	mov word [bp + ?86m_call_struc +2Ah], areas_struc + areastrucEntry
							; ip
	mov word [bp + ?86m_call_struc +2Ch], ax	; cs
	; push ss
	; pop es			; => stack
	lea di, [bp + ?86m_call_struc]	; -> 86-Mode call structure
_386	movzx edi, di			; (previously checked b[dpmi32] here)
	xor bx, bx			; flags/reserved
	xor cx, cx			; do not copy from PM stack
	mov ax, 0301h
	int 31h				; call 86 mode function with far return
	mov ah, byte [bp + ?86m_call_struc +20h]	; flags
	sahf
	mov ax, word [bp + ?86m_call_struc +1Ch]	; eax
	lleave
subcpureset
	jmp .common
%endif
.86m:
	push cs
	call .86m_to_entry_areas_struc
.common:
	 pop cx
	 push cx
	push ss
	pop ds
	push ss
	pop es
	jcxz .ret_qq
	mov di, msg.areasuninstalled.code
	call hexbyte
	mov dx, msg.areasuninstalled
	jmp .putsz_ret

.86m_to_entry_areas_struc:
	mov bx, areas_struc + areastrucEntry
	push ss
	push bx
	retf
%endif
