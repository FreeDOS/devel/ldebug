
%if 0

Extensions for lDebug link info

Copyright (C) 2008-2024 E. C. Masloch

Usage of the works is permitted provided that this
instrument is retained with the works, so that any entity
that uses the works is notified of this instrument.

DISCLAIMER: THE WORKS ARE WITHOUT WARRANTY.

%endif


%if _LINKHASH
 %include "nasmhash.mac"
%endif

%define LINKINFO_DATA_PREFIXES ""
%define LINKINFO_DATA_ENTRIES ""
%define LINKINFO_DATA_ADDRESSES ""
%define LINKINFO_DATA_MESSAGES db ""
%assign LINKINFO_DATA_AMOUNT 0

%imacro linkinfo_data 1-*.nolist
%rep %0
	linkinfo_data_as %1, %1
  %rotate 1
%endrep
%endmacro

%imacro linkinfo_data_as 2.nolist
 %push
  %defstr %$string %1
  %ifn _LINKHASH
   %substr %$prefix %$string 1, 2
   %substr %$suffix %$string 3, -1
   %strlen %$length %$string
   %if %$length < 2
    %fatal Too short link info data name
   %endif
   %if %$length > 63 + 2
    %fatal Too long link info data name
   %endif
   %xdefine LINKINFO_DATA_PREFIXES LINKINFO_DATA_PREFIXES, %$prefix
   %xdefine LINKINFO_DATA_ENTRIES LINKINFO_DATA_ENTRIES, %%message_%1
   %xdefine LINKINFO_DATA_ADDRESSES LINKINFO_DATA_ADDRESSES, %2
   %xdefine LINKINFO_DATA_MESSAGES LINKINFO_DATA_MESSAGES, %%message_%1:, {counted %$suffix}
  %else
   %xdefine %$suffix %$string
hashdef HASH, %$string
   %strlen %$length %$string
   %if %$length > 63 + 2
    %fatal Too long link info data name
   %endif
   %xdefine LINKINFO_DATA_PREFIXES LINKINFO_DATA_PREFIXES, _HASH
   %xdefine LINKINFO_DATA_ENTRIES LINKINFO_DATA_ENTRIES, %%message_%1
   %xdefine LINKINFO_DATA_ADDRESSES LINKINFO_DATA_ADDRESSES, %2
   %xdefine LINKINFO_DATA_MESSAGES LINKINFO_DATA_MESSAGES, %%message_%1:, {counted %$suffix}
  %endif
  %assign LINKINFO_DATA_AMOUNT LINKINFO_DATA_AMOUNT + 1
  %rotate 1
 %pop
%endmacro


%define LINKINFO_CODE_PREFIXES ""
%define LINKINFO_CODE_ENTRIES ""
%define LINKINFO_CODE_ADDRESSES ""
%define LINKINFO_CODE_MESSAGES db ""
%assign LINKINFO_CODE_AMOUNT 0

%imacro linkinfo_code 1-*.nolist
%rep %0
	linkinfo_code_as %1, %1
  %rotate 1
%endrep
%endmacro

%imacro linkinfo_code_as 2.nolist
 %push
  %if LINKINFO_CODE_SEGMENT == 0
   %define %$section lDEBUG_CODE
  %elif LINKINFO_CODE_SEGMENT == 1
   %define %$section lDEBUG_CODE2
  %elif LINKINFO_CODE_SEGMENT == 2
   %define %$section lDEBUG_DATA_ENTRY
  %else
   %fatal Unknown link info code section
  %endif
  %ifndef SECTION_OF_%2
   %error No section specified for link symbol %1
  %elifnidni SECTION_OF_%2, %$section
   %error Wrong section for link symbol %1: section_of=SECTION_OF_%1, current=%$section
  %endif
  %defstr %$string %1
  %ifn _LINKHASH
   %substr %$prefix %$string 1, 2
   %substr %$suffix %$string 3, -1
   %strlen %$length %$string
   %if %$length < 2
    %fatal Too short link info code name
   %endif
   %if %$length > 63 + 2
    %fatal Too long link info code name
   %endif
   %xdefine LINKINFO_CODE_PREFIXES LINKINFO_CODE_PREFIXES, %$prefix
   %xdefine LINKINFO_CODE_ENTRIES LINKINFO_CODE_ENTRIES, %%message_%1
   %xdefine LINKINFO_CODE_ADDRESSES LINKINFO_CODE_ADDRESSES, %2, LINKINFO_CODE_SEGMENT
   %xdefine LINKINFO_CODE_MESSAGES LINKINFO_CODE_MESSAGES, %%message_%1:, {counted %$suffix}
  %else
   %xdefine %$suffix %$string
hashdef HASH, %$string
   %strlen %$length %$string
   %if %$length > 63 + 2
    %fatal Too long link info code name
   %endif
   %xdefine LINKINFO_CODE_PREFIXES LINKINFO_CODE_PREFIXES, _HASH
   %xdefine LINKINFO_CODE_ENTRIES LINKINFO_CODE_ENTRIES, %%message_%1
   %xdefine LINKINFO_CODE_ADDRESSES LINKINFO_CODE_ADDRESSES, %2, LINKINFO_CODE_SEGMENT
   %xdefine LINKINFO_CODE_MESSAGES LINKINFO_CODE_MESSAGES, %%message_%1:, {counted %$suffix}
  %endif
  %assign LINKINFO_CODE_AMOUNT LINKINFO_CODE_AMOUNT + 1
 %pop
%endmacro


%imacro dump_linkinfo_messages 1-*.nolist
 %rep %0
	%1
  %rotate 1
 %endrep
%endmacro


%if _MESSAGESEGMENT
 %if _PM
linksel equ messagesel
 %else
linksel equ linkseg
 %endif
linkseg equ messageseg
%else
 %if _PM
linksel equ dssel
 %else
linksel equ linkseg
 %endif
linkseg equ pspdbg
%endif

linkinfo_data linksel
linkinfo_data linkseg
linkinfo_data linkinfoaddress
linkinfo_data linkcall_table
linkinfo_data extseg, extcssel, extdssel, extseg_size, extseg_used
linkinfo_data extdata, extdata_size, extdata_used
linkinfo_data pspdbg, pspdbe
 %ifn _PM
dssel equ pspdbg
 %endif
linkinfo_data dssel
%if _PM
linkinfo_data cssel, code_sel
 %if _DUALCODE
linkinfo_data code2_sel
 %endif
 %if _MESSAGESEGMENT
linkinfo_data messagesel
 %endif
%endif
linkinfo_data internalflags
linkinfo_data internalflags2
linkinfo_data internalflags3
linkinfo_data internalflags4
linkinfo_data internalflags5
linkinfo_data internalflags6
linkinfo_data internalflags7
linkinfo_data options
linkinfo_data options2
linkinfo_data options3
linkinfo_data options4
linkinfo_data options5
linkinfo_data options6
linkinfo_data options7
linkinfo_data lastcmd
linkinfo_data line_in, line_in.end, line_out, line_out_end
linkinfo_data auxbuff_segorsel
linkinfo_data auxbuff_current_size, auxbuff_current_size_minus_24
linkinfo_data firstmcb, firstumcb
linkinfo_data msg.install, msg.uninstall, msg.run
linkinfo_data entryseg_size, messageseg, messageseg_size
linkinfo_data code_seg, code_size
%if _DUALCODE
linkinfo_data code2_seg, code2_size
%endif
linkinfo_data auxbuff_segment, auxbuff_current_size
 %if _HISTORY_SEPARATE_FIXED && _HISTORY
linkinfo_data history_segment, historyseg_size
 %endif
%if _PM
auxbuff_segment equ auxbuff_segorsel + soaSegment
 %if _HISTORY_SEPARATE_FIXED && _HISTORY
history_segment equ history.segorsel + soaSegment
 %endif
%else
auxbuff_segment equ auxbuff_segorsel
 %if _HISTORY_SEPARATE_FIXED && _HISTORY
history_segment equ history.segorsel
 %endif
%endif
%if _APP_ENV_SIZE || _DEV_ENV_SIZE || _BOOT_ENV_SIZE
linkinfo_data envseg, env_size
%endif
linkinfo_data alloc_seg, alloc_size
linkinfo_data next_reserved_segment, alloc_size_not_reserved, reserved_address
%if _HISTORY
 %if _HISTORY_SEPARATE_FIXED
historyformat equ 2
linkinfo_data history.segorsel
 %else
historyformat equ 1
linkinfo_data historybuffer
linkinfo_data historybuffer.end
 %endif
linkinfo_data history.first
linkinfo_data history.last
%else
historyformat equ 0
%endif
linkinfo_data historyformat
linkinfo_data stack, stack_end
linkinfo_data ext_command_handler, ext_inject_handler, savesp, throwsp
linkinfo_data ext_preprocess_handler, ext_puts_handler
linkinfo_data ext_puts_copyoutput_handler
%if _EXT_PUTS_GETLINE
linkinfo_data ext_puts_getline_handler
%endif
linkinfo_data ext_aa_before_getline_handler
linkinfo_data ext_aa_inject_handler
linkinfo_data ext_aa_after_getline_handler
%if _CATCHINT2D
linkinfo_data ext_amis_handler
linkinfo_data amis_multiplex_number
%endif
linkinfo_data try_debugger_amis_multiplex_number
linkinfo_data msg.in, lastcmd, firstmcb, firstumcb, dpmi32
linkinfo_data lastcmd_transfer_ext_address
linkinfo_data lastcmd_transfer_ext_entry	; (not actually data but need it)
linkinfo_data dmycmd				; (not actually data but need it)
linkinfo_data has_87
%if _MMXSUPP
linkinfo_data has_mmx
%endif
linkinfo_data installflags, installformat
linkinfo_data reg_eax, reg_ecx, reg_edx, reg_ebx, reg_esp, reg_ebp
linkinfo_data reg_esi, reg_edi, reg_efl, reg_eip, reg_cs, reg_ds
linkinfo_data reg_ss, reg_es, reg_fs, reg_gs
linkinfo_data msg.not, msg.then
%if _EXTENSIONS && _EXT_VARIABLES
linkinfo_data ext_var, ext_var_amount, ext_var_format, ext_var_size
linkinfo_data isvariable_morebyte_nameheaders.ext
linkinfo_data var_ext_setup			; (not actually data)
%endif
linkinfo_data ext_handle
linkinfo_data near_transfer_ext_entry		; not actually data
linkinfo_data near_transfer_ext_address
%if _BOOTLDR
linkinfo_data load_data_lowest, load_data
linkinfo_data load_yyname_input
linkinfo_data handle_scan_dir_entry, handle_scan_dir_not_found
linkinfo_data ..@boot_scan_dir_return_filenotfound		; not actually data
linkinfo_data ..@boot_scan_dir_return_subdir_or_fat32_entry	; not actually data
linkinfo_data ..@boot_scan_dir_return_fat16_root_entry		; not actually data
linkinfo_data scan_dir_entry			; not actually data
linkinfo_data load_check_dir_attr
linkinfo_data load_yyname_next
linkinfo_data load_yy_direntry
linkinfo_data load_kernel_name
linkinfo_data load_adr_dirbuf_segment
%endif
linkinfo_data execblk.cmdline
%if _CONFIG
linkinfo_data configpath, configpath.dir_end
linkinfo_data scriptspath, scriptspath.dir_end
%endif
build_option_PM equ !!_PM
linkinfo_data build_option_PM
linkinfo_data cmd3, cmd3_not_ext, puts_ext_done, puts_ext_next
linkinfo_data puts_copyoutput_ext_done
%if _EXT_PUTS_GETLINE
linkinfo_data puts_getline_ext_done
linkinfo_data in_getinput
%endif
linkinfo_data cmd3_preprocessed, cmd3_not_inject, cmd3_injected
linkinfo_data aa_not_inject, aa_injected
linkinfo_data aa_before_getline, aa_after_getline
linkinfo_data while_buffer, while_buffer.end, swch1
linkinfo_data_as internalflags_with_tt_while, internalflags
linkinfo_data yy_is_script
linkinfo_data terminator_in_line_in.offset
linkinfo_data terminator_in_line_in.value
linkinfo_data ext_finish.bp_is_set
linkinfo_data vregs, vregs.amount
%if _DT
linkinfo_data asciitablenames
%endif
linkinfo_data pm_2_86m_0
%if _BOOTLDR
linkinfo_data load_unit_flags
%endif
linkinfo_data sss_silent_count_used, sss_silent_count, search_results, sscounter
linkinfo_data search_results_amount
linkinfo_data patcharea_run, patcharea_run.size, patcharea_run.segment
linkinfo_data patcharea_intrtn, patcharea_intrtn.size, patcharea_intrtn.segment
%if _PM
linkinfo_data patcharea_pm_exc, patcharea_pm_exc.size, patcharea_pm_exc.segment
%endif
;linkinfo_data 


%assign LINKINFO_CODE_SEGMENT 0		; CODE1
linkinfo_code puts, putsz, putc, putsline, putsline_crlf, trimputs
linkinfo_code skipcomma, skipcomm0, skipwhite, skipwh0
linkinfo_code isstring?, uppercase, chkeol, iseol?, iseol?.notsemicolon
linkinfo_code guard_auxbuff, guard_re, prephack, dohack, unhack
linkinfo_code handle_serial_flags_ctrl_c
linkinfo_code hexnyb, hexbyte, hexword, hexword_high, decword, decdword
linkinfo_code dec_dword_minwidth
linkinfo_code ispm, setes2dx, call_int2D
linkinfo_code copy_single_counted_string
linkinfo_code intchk
linkinfo_code IsIISPEntry?
linkinfo_code disp_dxax_times_cx_width_bx_size.store
linkinfo_code cmd3, cmd3_not_ext, puts_ext_done, puts_ext_next
linkinfo_code puts_copyoutput_ext_done
%if _EXT_PUTS_GETLINE
linkinfo_code puts_getline_ext_done
%endif
linkinfo_code cmd3_preprocessed, cmd3_not_inject, cmd3_injected
linkinfo_code error
linkinfo_code setds2si, setds2bx
%if _PM
linkinfo_code intcall_ext_return_es
%endif
linkinfo_code dd_store
linkinfo_code InDOS, _doscall, yy_dos_parse_name, yy_open_file, yy_check_lfn
linkinfo_code yy_common_parse_name
linkinfo_code yy_common_parse_name_bx_buffer
linkinfo_code retry_open_scriptspath
linkinfo_code near_transfer_ext_return
%if _BOOTLDR
linkinfo_code yy_boot_read.bx, yy_boot_seek_start.bx, yy_boot_seek_current.bx
linkinfo_code_as yy_boot_parse_name2, yy_boot_parse_name
linkinfo_code yy_boot_parse_name_bx_buffer
linkinfo_code_as yy_boot_open_file2, yy_boot_open_file
linkinfo_code scan_dir_aux, read_sector
linkinfo_code_as yy_boot_init_dir2, yy_boot_init_dir
linkinfo_code got_yyentry, scan_dir_entry
linkinfo_code ..@yy_filename_empty
linkinfo_code boot_parse_fn
%endif
linkinfo_code close_ext
linkinfo_code yy_reset_buf
linkinfo_code aa_not_inject, aa_injected
linkinfo_code aa_before_getline, aa_after_getline
linkinfo_code doscall_extseg
linkinfo_code test_d_b_bit, test_high_limit
linkinfo_code setrc
linkinfo_code skipequals
%if _COUNT || _SCOUNT
linkinfo_code count_store
%endif
linkinfo_code hexdword
;linkinfo_code 

%if _DUALCODE && _EXPRDUALCODE
 %assign LINKINFO_CODE_SEGMENT 1	; CODE2
%else
 %assign LINKINFO_CODE_SEGMENT 0	; CODE1
%endif
linkinfo_code isvariable?, isseparator?, get_value_range
linkinfo_code getexpression, getdword, getword, getbyte
linkinfo_code getaddr, getaddrX, getrange, getrangeX
linkinfo_code get_length_keyword, getnyb
linkinfo_code isoperator?, ..@call_operator_dispatchers
%if _EXTENSIONS && _EXT_VARIABLES
linkinfo_code var_ext_setup_done
%endif
linkinfo_code getstr
;linkinfo_code 


 %assign LINKINFO_CODE_SEGMENT 2	; ENTRY
linkinfo_code entry_retn
;linkinfo_code 


	align 4, db 0
linkinfo:
	istruc ELD_LINKINFO
at eldlSignature,	dw 0E1D1h
at eldlReserved,	dw 0, 0
at eldlUseLinkHash,	dw !!_LINKHASH
at eldlDataAmount,	dw LINKINFO_DATA_AMOUNT
at eldlDataPrefixes,	dw linkinfo_data_table_prefixes
at eldlDataEntries,	dw linkinfo_data_table_entries
at eldlDataAddresses,	dw linkinfo_data_table_addresses
at eldlCodeAmount,	dw LINKINFO_CODE_AMOUNT
at eldlCodePrefixes,	dw linkinfo_code_table_prefixes
at eldlCodeEntries,	dw linkinfo_code_table_entries
at eldlCodeAddresses,	dw linkinfo_code_table_addresses
	iend


	align 2, db 0
linkinfo_data_table_prefixes:
.:	dw LINKINFO_DATA_PREFIXES
.amount equ ($ - .) / 2
%if .amount != LINKINFO_DATA_AMOUNT
 %error Unexpected table size
%endif

	align 2, db 0
linkinfo_data_table_entries:
.:	dw LINKINFO_DATA_ENTRIES
.amount equ ($ - .) / 2
%if .amount != LINKINFO_DATA_AMOUNT
 %error Unexpected table size
%endif

	align 2, db 0
linkinfo_data_table_addresses:
.:	dw LINKINFO_DATA_ADDRESSES
.amount equ ($ - .) / 2
%if .amount != LINKINFO_DATA_AMOUNT
 %error Unexpected table size
%endif


	align 2, db 0
linkinfo_code_table_prefixes:
.:	dw LINKINFO_CODE_PREFIXES
.amount equ ($ - .) / 2
%if .amount != LINKINFO_CODE_AMOUNT
 %error Unexpected table size
%endif

	align 2, db 0
linkinfo_code_table_entries:
.:	dw LINKINFO_CODE_ENTRIES
.amount equ ($ - .) / 2
%if .amount != LINKINFO_CODE_AMOUNT
 %error Unexpected table size
%endif

	align 2, db 0
linkinfo_code_table_addresses:
.:	dw LINKINFO_CODE_ADDRESSES
.amount equ ($ - .) / 4
%if .amount != LINKINFO_CODE_AMOUNT
 %error Unexpected table size
%endif


	dump_linkinfo_messages LINKINFO_DATA_MESSAGES, LINKINFO_CODE_MESSAGES
