
%if 0

lDebug S commands (search, sleep)

Copyright (C) 1995-2003 Paul Vojta
Copyright (C) 2008-2024 E. C. Masloch

Usage of the works is permitted provided that this
instrument is retained with the works, so that any entity
that uses the works is notified of this instrument.

DISCLAIMER: THE WORKS ARE WITHOUT WARRANTY.

%endif


	usesection lDEBUG_CODE

sleepcmd:
	call skipcomma
	nearcall getdword
	push bx
	push dx
	call skipwh0
	call iseol?
	je .seconds
	dec si
	mov dx, msg.seconds
	call isstring?
	je .seconds_check_eol
	mov dx, msg.ticks
	call isstring?
.error_NZ:
	jne error
.ticks_check_eol:
	lodsb
	call chkeol

	mov ax, 1
	jmp .common

.seconds_check_eol:
	lodsb
	call chkeol
.seconds:
	mov ax, 18
.common:
	pop dx
	push ax
	mul dx			; dx:ax = low word times multiplier
	mov bx, dx
	mov cx, ax		; bx:cx = low word times multiplier
	pop ax
	pop dx
	mul dx			; dx:ax = high word times multiplier
	add bx, ax
	adc dx, 0		; dx:bx:cx = entire result
	jnz .error_NZ

	test cx, cx
	jnz @F
	test bx, bx
	jz .end
@@:

	mov ax, 40h		; bimodal segment/selector
	mov es, ax
.loop_reset:
	mov dx, word [es:6Ch]
.loop:
	cmp dx, word [es:6Ch]
	jne .next
	call handle_serial_flags_ctrl_c
	testopt [options3], opt3_check_ctrlc_0bh
	jnz @F			; already called function 0Bh -->
	call InDOS
	jnz @F
	mov ah, 0Bh
	doscall			; allow to break with Ctrl-C
@@:
%if _SLEEP_NEW
	mov di, word [sleep_repeat_idle]
	inc di
@@:
	call idle
	dec di
	jnz @B
%else
	call idle
%endif
	jmp .loop

.next:
%if _SLEEP_NEW
	neg dx			; minus prior tick
	add dx, word [es:6Ch]	; new tick - prior tick

	cmp dx, word [sleep_delta_limit]
	jbe @F
	mov dx, word [sleep_delta_limit]
	test dx, dx
	jnz @F
	inc dx			; limit 0 would lead to stagnant sleep
@@:
	cmp dx, word [sleep_highest_delta]
	jbe @F
	mov word [sleep_highest_delta], dx
@@:
	sub cx, dx
	sbb bx, 0
	jc .end
%else
	sub cx, 1
	sbb bx, 0
%endif
	jnz .loop_reset
	jcxz .end
	jmp .loop_reset

.end:
	retn


 %imacro extcall 1-2.nolist
	nearcall %1
 %endmacro
 %define extcallcall nearcall
 %imacro internalcoderelocation 0-*.nolist
 %endmacro
 %imacro internaldatarelocation 0-*.nolist
 %endmacro
 %imacro linkdatarelocation 0-*.nolist
 %endmacro
 %define relocated(address) address
 %assign ELD 0

%include "ssshared.asm"

 %unimacro extcall 1-2.nolist
 %undef extcall
 %undef extcallcall
 %unimacro internalcoderelocation 0-*.nolist
 %unimacro internaldatarelocation 0-*.nolist
 %unimacro linkdatarelocation 0-*.nolist
 %undef relocated
