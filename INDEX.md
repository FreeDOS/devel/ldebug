# lDebug

lDebug is a 86-DOS debugger similar to the MS-DOS Debug tool. lDebug
is based on DEBUG/X 1.13 to 1.18 as released by Japheth, whose work
was released as Public Domain. It features DPMI client support for
32-bit and 16-bit segments, a 686-level assembler and disassembler, an
expression evaluator, an InDOS and a bootloaded mode, script file
reading, serial port I/O, permanent breakpoints, conditional tracing,
buffered tracing, and auto-repetition of some commands. There is also
a symbolic debugging branch being developed. The lDebug manual in
html, plain text and PDF is available online. The main website is at
ecm's webbed site with links to the source code and file releases.

main page:
https://pushbx.org/ecm/web/#projects-ldebug

manual:
https://pushbx.org/ecm/doc/ldebug.htm

source code:
https://hg.pushbx.org/ecm/ldebug/

file releases:
https://pushbx.org/ecm/download/ldebug/

# Contributing

LDEBUG is maintained at https://pushbx.org/ecm/web/#projects-ldebug
## LDEBUG.LSM

<table>
<tr><td>title</td><td>lDebug</td></tr>
<tr><td>version</td><td>release 9</td></tr>
<tr><td>entered&nbsp;date</td><td>2024-12-21</td></tr>
<tr><td>description</td><td>advanced debugger based on FreeDOS Debug</td></tr>
<tr><td>keywords</td><td>debug, debugger, DPMI debugger, bootable debugger</td></tr>
<tr><td>author</td><td>Paul Vojta &lt;vojta@math.berkeley.edu&gt; Andreas "Japheth" Grech &lt;mail -at- japheth.de&gt;</td></tr>
<tr><td>maintained&nbsp;by</td><td>E. C. Masloch &lt;pushbx -AT- ulukai.org&gt;</td></tr>
<tr><td>primary&nbsp;site</td><td>https://pushbx.org/ecm/web/#projects-ldebug</td></tr>
<tr><td>original&nbsp;site</td><td>http://www.ibiblio.org/pub/micro/pc-stuff/freedos/files/dos/debug/old/debug113.zip</td></tr>
<tr><td>platforms</td><td>DOS (crosscompile from Linux with NASM and C compiler)</td></tr>
<tr><td>copying&nbsp;policy</td><td>[Fair License](LICENSE)</td></tr>
</table>
