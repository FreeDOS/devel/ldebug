lDebug is a 86-DOS debugger similar to the MS-DOS Debug tool. lDebug
is based on DEBUG/X 1.13 to 1.18 as released by Japheth, whose work
was released as Public Domain. It features DPMI client support for
32-bit and 16-bit segments, a 686-level assembler and disassembler, an
expression evaluator, an InDOS and a bootloaded mode, script file
reading, serial port I/O, permanent breakpoints, conditional tracing,
buffered tracing, and auto-repetition of some commands. There is also
a symbolic debugging branch being developed. The lDebug manual in
html, plain text and PDF is available online. The main website is at
ecm's webbed site with links to the source code and file releases.

main page:
https://pushbx.org/ecm/web/#projects-ldebug

manual:
https://pushbx.org/ecm/doc/ldebug.htm

source code:
https://hg.pushbx.org/ecm/ldebug/

file releases:
https://pushbx.org/ecm/download/ldebug/
